//
//  EducationIndexManager.h
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^IMSuccessBlock)(id objects);
typedef void (^IMFailureBlock)(NSError *error);

@interface EducationIndexManager : UIViewController

SINGLETON_INTERFACE(EducationIndexManager)

//取得奶瓶志首页信息
- (void)getFeederIndexList:(NSDictionary *)parameters
                      success:(IMSuccessBlock)success
                      failure:(IMFailureBlock)failure;

//取得奶瓶志知识包详细内容
- (void)getFeederDetailListById:(NSString *)objectId
                              success:(IMSuccessBlock)success
                              failure:(IMFailureBlock)failure;

//取得活动首页信息
- (void)getActivityIndexList:(NSDictionary *)parameters
                   success:(IMSuccessBlock)success
                   failure:(IMFailureBlock)failure;

//取得活动内容详细信息列表
- (void)getActivityContentList:(NSDictionary *)parameters
                     success:(IMSuccessBlock)success
                     failure:(IMFailureBlock)failure;

//取得奶瓶志喜欢的前几个信息
- (void)getFeederLikeIndex:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure;

//取得活动喜欢的前几个信息
- (void)getActivityLikeIndex:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure;

//添加奶瓶志喜欢
- (void)addFeederLike:(NSDictionary *)parameters
                 success:(IMSuccessBlock)success
                 failure:(IMFailureBlock)failure;

//取消奶瓶志喜欢
- (void)removeFeederLike:(NSDictionary *)parameters
                    success:(IMSuccessBlock)success
                    failure:(IMFailureBlock)failure;

//添加活动喜欢
- (void)addActivityLike:(NSDictionary *)parameters
              success:(IMSuccessBlock)success
              failure:(IMFailureBlock)failure;

//取消活动喜欢
- (void)removeActivityLike:(NSDictionary *)parameters
                 success:(IMSuccessBlock)success
                 failure:(IMFailureBlock)failure;

@end
