//
//  CommunityIndexManager.h
//  BabyRun
//
//  Created by fengqiang on 14-12-15.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^IMSuccessBlock)(id objects);
typedef void (^IMFailureBlock)(NSError *error);

@interface CommunityIndexManager : UIViewController

SINGLETON_INTERFACE(CommunityIndexManager)

//取得文字帖首页分类信息
- (void)getTextGroupList:(NSDictionary *)parameters
                success:(IMSuccessBlock)success
                failure:(IMFailureBlock)failure;

//取得文字贴首页信息
- (void)getCommunityIndexList:(NSDictionary *)parameters
                success:(IMSuccessBlock)success
                failure:(IMFailureBlock)failure;

//取得文字贴关注用户的首页信息
- (void)getCommunityIndexForUserList:(NSDictionary *)parameters
                      success:(IMSuccessBlock)success
                      failure:(IMFailureBlock)failure;

//取得文字帖对应的评论信息
- (void)getCommunityCommentList:(NSDictionary *)parameters
                  success:(IMSuccessBlock)success
                  failure:(IMFailureBlock)failure;
//取得文字帖评论数量
- (void)getTextSharedCommentCount:(NSDictionary *)parameters
                         success:(IMSuccessBlock)success
                         failure:(IMFailureBlock)failure;

//取得文字帖详情页对应的喜欢信息最新6个数据
- (void)getCommunityLikeIndex:(NSDictionary *)parameters
                success:(IMSuccessBlock)success
                failure:(IMFailureBlock)failure;

//取得文字帖详情页对应的喜欢列表信息
- (void)getCommunityLikeList:(NSDictionary *)parameters
               success:(IMSuccessBlock)success
               failure:(IMFailureBlock)failure;

//添加文字帖评论
- (void)addTextSharedComment:(NSDictionary *)parameters
                    success:(IMSuccessBlock)success
                    failure:(IMFailureBlock)failure;

//删除文字帖评论
- (void)removeTextSharedComment:(NSDictionary *)parameters
                 success:(IMSuccessBlock)success
                 failure:(IMFailureBlock)failure;

//添加文字帖喜欢
- (void)addTextSharedLike:(NSDictionary *)parameters
                 success:(IMSuccessBlock)success
                 failure:(IMFailureBlock)failure;

//取消文字帖喜欢
- (void)removeTextSharedLike:(NSDictionary *)parameters
                    success:(IMSuccessBlock)success
                    failure:(IMFailureBlock)failure;

//取消文字帖
- (void)removeTextShared:(NSString *)objectId
                     success:(IMSuccessBlock)success
                     failure:(IMFailureBlock)failure;

@end
