//
//  PublicManager.h
//  BabyRun
//
//  Created by fengqiang on 15-1-2.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

typedef void (^IMSuccessBlock)(id objects);
typedef void (^IMFailureBlock)(NSError *error);

@interface PublicManager : UIViewController

@property (nonatomic,strong) UIViewController *rootViewController;

SINGLETON_INTERFACE(PublicManager)

//取得推荐关注用户列表
- (void)getRecommendUserList:(NSDictionary *)parameters
                success:(IMSuccessBlock)success
                failure:(IMFailureBlock)failure;

//取得推荐关注用户列表提示信息
- (void)getRecommendUserInfo:(NSDictionary *)parameters
                     success:(IMSuccessBlock)success
                     failure:(IMFailureBlock)failure;

//生成主要底部按钮
- (UIViewController *)setupViewControllers;

/*处理返回应该显示的时间*/
- (NSString *)returnUploadTime:(NSDate *)dic;

- (NSString *)getTimeDiffString:(NSTimeInterval)timestamp;

- (void)shareData:(id)data type:(int)shareType;

//取得喜欢的列表信息数据
- (void)getLikeAllList:(NSDictionary *)parameters
               success:(IMSuccessBlock)success
               failure:(IMFailureBlock)failure;

//举报信息
- (void)addReport:(NSDictionary *)parameters
       success:(IMSuccessBlock)success
       failure:(IMFailureBlock)failure;

//判断是否允许使用相机
-(BOOL)isOpenCarma;

@end
