//
//  PostManager.h
//  BabyRun
//
//  Created by fengqiang on 15-1-3.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//
#import "ImgTag.h"

typedef void (^IMSuccessBlock)(id objects);
typedef void (^IMFailureBlock)(NSError *error);

@interface PostManager : UIViewController

SINGLETON_INTERFACE(PostManager)

//添加图片帖
- (void)addImgShared:(NSDictionary *)parameters
             success:(IMSuccessBlock)success
             failure:(IMFailureBlock)failure;

//添加文字帖
- (void)addTextShared:(NSDictionary *)parameters
             success:(IMSuccessBlock)success
             failure:(IMFailureBlock)failure;

//查找标签列表
- (void)findTagList:(NSDictionary *)parameters
             success:(IMSuccessBlock)success
             failure:(IMFailureBlock)failure;

//添加标签
- (void)addTag:(ImgTag *)tag
            success:(IMSuccessBlock)success
            failure:(IMFailureBlock)failure;

@end
