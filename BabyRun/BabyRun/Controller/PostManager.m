//
//  PostManager.m
//  BabyRun
//
//  Created by fengqiang on 15-1-3.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "PostManager.h"
#import "BREditTagButton.h"
#import "ImgTag.h"

@interface PostManager ()

@end

@implementation PostManager

SINGLETON_IMPLEMENTATION(PostManager)

- (void)addImgShared:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVObject *object = [AVObject objectWithClassName:@"imgShared"];
    [object setObject:parameters[@"sharedTitle"] forKey:@"sharedTitle"];
    [object setObject:parameters[@"userId"] forKey:@"userid"];
    if (parameters[@"taskId"]) {
        [object setObject:parameters[@"taskId"] forKey:@"taskId"];
    }
    [object setObject:parameters[@"imgUrl"] forKey:@"imgUrl"];
    [object setObject:[NSNumber numberWithInt:0] forKey:@"deleteFlag"];
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            NSMutableArray *tagArray = [[NSMutableArray alloc] init];
            //添加图片中的标签
            for (BREditTagButton *btn in parameters[@"tagArray"]) {
                AVObject *objectTag = [AVObject objectWithClassName:@"imgTag"];
                [objectTag setObject:[NSNumber numberWithInt:btn.imgTag.tagOrientation] forKey:@"orientation"];
                [objectTag setObject:object.objectId forKey:@"sharedId"];
                [objectTag setObject:btn.imgTag.tagText forKey:@"tagContent"];
                [objectTag setObject:btn.imgTag.tagGroup forKey:@"tagGroup"];
                
                //进行坐标转换
                
                [objectTag setObject:@[
                                       [NSString stringWithFormat:@"%f",btn.imgTag.tagPosition.x/SCREEN_WIDTH]
                                       ,[NSString stringWithFormat:@"%f",btn.imgTag.tagPosition.y/SCREEN_HEIGHT]
                                       ] forKey:@"tagPosition"];
                if([objectTag save]){
                    [tagArray addObject:objectTag.objectId];
                }
            }
            if (tagArray.count>0) {
                [object setObject:tagArray forKey:@"tagArray"];
                [object saveEventually];
            }
            success(object);
        } else {
            failure(error);
        }
    }];
}

- (void)addTextShared:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVObject *object = [AVObject objectWithClassName:@"textShared"];
    [object setObject:parameters[@"text"] forKey:@"sharedTitle"];
    [object setObject:parameters[@"userid"] forKey:@"userid"];
    [object setObject:parameters[@"textBackground"]forKey:@"textBackground"];
    [object setObject:[NSNumber numberWithInteger:[parameters[@"groupId"] integerValue]] forKey:@"sharedGroup"];
    [object setObject:[NSNumber numberWithInt:0] forKey:@"deleteFlag"];
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            success(object);
        } else {
            failure(error);
        }
    }];
}

-(void)findTagList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    if ([parameters[@"loadMore"] isEqualToString:@"YES"]) {
        NSMutableArray *resultArray = [[NSMutableArray alloc] init];
        AVQuery *query1 = [AVQuery queryWithClassName:@"tag"];
        query1.limit = 20;
        if(parameters){
            if (parameters[@"skip"]) {
                query1.skip = [parameters[@"skip"] integerValue];
            }
        }
        [query1 orderByDescending:@"createdAt"];
        if(parameters[@"tagName"])
            [query1 whereKey:@"tagContent" hasPrefix:parameters[@"tagName"]];
        [query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            for (AVObject *object in objects) {
                
                ImgTag *imgTag = [[ImgTag alloc] init];
                imgTag.tagText = object[@"tagContent"];
                imgTag.tagId = object.objectId;
                imgTag.tagGroup = object[@"tagGroup"];
                [resultArray addObject:imgTag];
            }
            success(resultArray);
        }];
    }
    else{
        AVQuery *query2 = [AVQuery queryWithClassName:@"tag"];
        if(parameters[@"tagName"])
            [query2 whereKey:@"tagContent" equalTo:parameters[@"tagName"]];
        [query2 getFirstObjectInBackgroundWithBlock:^(AVObject *object, NSError *error) {
            NSMutableArray *resultArray = [[NSMutableArray alloc] init];
            if (object) {
                ImgTag *imgTag = [[ImgTag alloc] init];
                imgTag.tagText = object[@"tagContent"];
                imgTag.tagId = object.objectId;
                imgTag.tagGroup = object[@"tagGroup"];
                [resultArray addObject:imgTag];
            }
            else{
                ImgTag *imgTag = [[ImgTag alloc] init];
                imgTag.tagText = parameters[@"tagName"];
                imgTag.tagGroup = [NSNumber numberWithInt:-1];
                [resultArray addObject:imgTag];
            }
            //取得主要类型
            AVQuery *query = [AVQuery queryWithClassName:@"tag"];
            [query orderByDescending:@"createdAt"];
            [query whereKey:@"tagGroup" equalTo:parameters[@"tagGroup"]];
            //生成展示Object
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                for (AVObject *object in objects) {
                    ImgTag *imgTag = [[ImgTag alloc] init];
                    imgTag.tagText = object[@"tagContent"];
                    imgTag.tagId = object.objectId;
                    imgTag.tagGroup = object[@"tagGroup"];
                    [resultArray addObject:imgTag];
                }
                AVQuery *query1 = [AVQuery queryWithClassName:@"tag"];
                query1.limit = 20;
                if(parameters){
                    if (parameters[@"skip"]) {
                        query1.skip = [parameters[@"skip"] integerValue];
                    }
                }
                [query1 orderByDescending:@"createdAt"];
                if(parameters[@"tagName"])
                    [query1 whereKey:@"tagContent" hasPrefix:parameters[@"tagName"]];
                [query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                    for (AVObject *object in objects) {
                        
                        ImgTag *imgTag = [[ImgTag alloc] init];
                        imgTag.tagText = object[@"tagContent"];
                        imgTag.tagId = object.objectId;
                        imgTag.tagGroup = object[@"tagGroup"];
                        [resultArray addObject:imgTag];
                    }
                    success(resultArray);
                }];
            }];
            
        }];
    }
}

- (void)addTag:(ImgTag *)tag success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    AVObject *object = [AVObject objectWithClassName:@"tag"];
    [object setObject:tag.tagText forKey:@"tagContent"];
    [object setObject:[NSNumber numberWithInt:999] forKey:@"tagGroup"];
    [object saveEventually];
}

@end
