//
//  EducationIndexManager.m
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "EducationIndexManager.h"
#import "Activity.h"
#import "Feeder.h"
#import "FeederDetail.h"
#import "ImgShared.h"

@implementation EducationIndexManager

SINGLETON_IMPLEMENTATION(EducationIndexManager)

- (void)getFeederIndexList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"knowledge"];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    //每页取得的记录数,并做分页处理
    query.limit = 20;
    if(parameters){
        if (parameters[@"skip"]) {
            query.skip = [parameters[@"skip"] integerValue];
        }
    }
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *feederArray = [[NSMutableArray alloc] init];
        if (!error) {
            for (AVObject *object in objects) {
                Feeder *feeder = [[Feeder alloc] init];
                feeder.image = object[@"image"];
                feeder.title = object[@"title"];
                feeder.updateTime = object.createdAt;
                feeder.objectId = object.objectId;
                [feederArray addObject:feeder];
            }
            success(feederArray);
        } else {
            failure(error);
        }
    }];
}

- (void)getFeederDetailListById:(NSString *)objectId success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"article"];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    [query whereKey:@"knowledgeId" equalTo:objectId];

    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *feederArray = [[NSMutableArray alloc] init];
        if (!error) {
            for (AVObject *object in objects) {
                FeederDetail *feederDetail = [[FeederDetail alloc] init];
                feederDetail.image = object[@"img"];
                feederDetail.title = object[@"title"];
                feederDetail.subTitle = object[@"subTitle"];
                feederDetail.content = object[@"content"];
                feederDetail.likeCount = object[@"likeCount"];
                feederDetail.readCount = object[@"readCount"];
                feederDetail.updateTime = object.createdAt;
                feederDetail.objectId = object.objectId;
                
                if([AVUser currentUser]!=nil){
                    //查询是否是喜欢
                    AVQuery *query1 = [AVQuery queryWithClassName:@"articlePraise"];
                    [query1 whereKey:@"articleId" equalTo:object.objectId];
                    [query1 whereKey:@"userId" equalTo:[AVUser currentUser].objectId];
                    [query1 whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
                    AVObject *object1 = [query1 getFirstObject];
                    if (object1 != nil) {
                        feederDetail.isLike = YES;
                    }
                    else{
                        feederDetail.isLike = NO;
                    }
                }
                
                [feederArray addObject:feederDetail];
            }
            success(feederArray);
        } else {
            failure(error);
        }
    }];
}

- (void)getActivityIndexList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"task"];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    //每页取得的记录数,并做分页处理
    query.limit = 20;
    if(parameters){
        if (parameters[@"skip"]) {
            query.skip = [parameters[@"skip"] integerValue];
        }
    }
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *ActivityArray = [[NSMutableArray alloc] init];
        if (!error) {
            for (AVObject *object in objects) {
                Activity *activity = [[Activity alloc] init];
                activity.image = object[@"image"];
                activity.title = object[@"title"];
                activity.content = object[@"content"];
                activity.joinCount = object[@"joinCount"];
                activity.likeCount = object[@"likeCount"];
                activity.updateTime = object.createdAt;
                activity.objectId = object.objectId;
                
                [ActivityArray addObject:activity];
            }
            success(ActivityArray);
        } else {
            failure(error);
        }
    }];
}

- (void)getActivityContentList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"imgShared"];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    //每页取得的记录数,并做分页处理
    query.limit = 12;
    if(parameters){
        if (parameters[@"skip"]) {
            query.skip = [parameters[@"skip"] integerValue];
        }
        if (parameters[@"taskId"]) {
            [query whereKey:@"taskId" equalTo:parameters[@"taskId"]];
        }
    }
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *resultArray = [[NSMutableArray alloc] init];
        for (AVObject *object in objects) {
            AVQuery *query = [AVUser query];
            AVUser *user = (AVUser *)[query getObjectWithId:object[@"userid"]];
            if(user){
                ImgShared *shared = [[ImgShared alloc] init];
                shared.user = user;
                shared.imgText = object[@"sharedTitle"];
                shared.imgUrl = object[@"imgUrl"];
                shared.readCount = object[@"readCount"];
                shared.commentCount = object[@"commentCount"];
                shared.likeCount = object[@"praiseCount"];
                shared.tags = object[@"tagArray"];
                shared.updateTime = object.createdAt;
                shared.objectId = object.objectId;
                
                [resultArray addObject:shared];
                [object incrementKey:@"readCount"];
                [object saveInBackground];
            }
        }
        
        if (!error) {
            success(resultArray);
        } else {
            failure(error);
        }
    }];
}

- (void)getFeederLikeIndex:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"articlePraise"];
    [query whereKey:@"articleId" equalTo:parameters[@"articleId"]];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    [query countObjectsInBackgroundWithBlock:^(NSInteger number, NSError *error) {
        NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] init];
        [resultDic setObject:[NSNumber numberWithInteger:number] forKey:@"likeCount"];
        if (number > 0) {
            query.limit = 5;
            [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
                if (!error) {
                    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
                    for (AVObject *object in results) {
                        AVQuery *query = [AVUser query];
                        AVUser *user = (AVUser *)[query getObjectWithId:object[@"userId"]];
                        if(user){
                            [resultArray addObject:user];
                        }
                    }
                    [resultDic setObject:resultArray forKey:@"likeList"];
                    success(resultDic);
                } else {
                    success(resultDic);
                }
            }];
        }else{
            success(resultDic);
        }
    }];
}

- (void)getActivityLikeIndex:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"taskPraise"];
    [query whereKey:@"taskId" equalTo:parameters[@"taskId"]];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    [query countObjectsInBackgroundWithBlock:^(NSInteger number, NSError *error) {
        NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] init];
        [resultDic setObject:[NSNumber numberWithInteger:number] forKey:@"likeCount"];
        if (number > 0) {
            query.limit = 5;
            [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
                if (!error) {
                    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
                    for (AVObject *object in results) {
                        AVQuery *query = [AVUser query];
                        AVUser *user = (AVUser *)[query getObjectWithId:object[@"userId"]];
                        if(user){
                            [resultArray addObject:user];
                        }
                    }
                    [resultDic setObject:resultArray forKey:@"likeList"];
                    success(resultDic);
                } else {
                    success(resultDic);
                }
            }];
        }else{
            success(resultDic);
        }
    }];
}

- (void)getActivityLikeCount:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"taskPraise"];
    [query whereKey:@"taskId" equalTo:parameters[@"taskId"]];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    [query countObjectsInBackgroundWithBlock:^(NSInteger number, NSError *error) {
        
    }];
}

- (void)addFeederLike:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVObject *object = [AVObject objectWithClassName:@"articlePraise"];
    [object setObject:parameters[@"articleId"] forKey:@"articleId"];
    [object setObject:[AVUser currentUser].objectId forKey:@"userId"];
    [object setObject:[NSNumber numberWithInt:0] forKey:@"deleteFlag"];
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            AVQuery *query = [AVQuery queryWithClassName:@"article"];
            AVObject *object = [query getObjectWithId:parameters[@"articleId"]];
            [object incrementKey:@"likeCount"];
            [object saveInBackground];
            success(object);
        } else {
            failure(error);
        }
    }];
}

- (void)removeFeederLike:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"articlePraise"];
    [query whereKey:@"articleId" equalTo:parameters[@"articleId"]];
    [query whereKey:@"userId" equalTo:[AVUser currentUser].objectId];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            AVObject *object = objects[0];
            [object setObject:[NSNumber numberWithInt:1] forKey:@"deleteFlag"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    AVQuery *query = [AVQuery queryWithClassName:@"article"];
                    AVObject *imgShared = [query getObjectWithId:parameters[@"articleId"]];
                    [imgShared setObject:[NSNumber numberWithInt:[imgShared[@"likeCount"] intValue]-1] forKey:@"likeCount"];
                    [imgShared saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (succeeded) {
                            success(object);
                        } else {
                            failure(error);
                        }
                    }];
                } else {
                    failure(error);
                }
            }];
        } else {
            failure(error);
        }
    }];
}

- (void)addActivityLike:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVObject *object = [AVObject objectWithClassName:@"taskPraise"];
    [object setObject:parameters[@"taskId"] forKey:@"taskId"];
    [object setObject:[AVUser currentUser].objectId forKey:@"userId"];
    [object setObject:[NSNumber numberWithInt:0] forKey:@"deleteFlag"];
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            AVQuery *query = [AVQuery queryWithClassName:@"task"];
            AVObject *object = [query getObjectWithId:parameters[@"taskId"]];
            [object incrementKey:@"likeCount"];
            [object saveInBackground];
            success(object);
        } else {
            failure(error);
        }
    }];
}

- (void)removeActivityLike:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"taskPraise"];
    [query whereKey:@"taskId" equalTo:parameters[@"taskId"]];
    [query whereKey:@"userId" equalTo:[AVUser currentUser].objectId];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            AVObject *object = objects[0];
            [object setObject:[NSNumber numberWithInt:1] forKey:@"deleteFlag"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    AVQuery *query = [AVQuery queryWithClassName:@"task"];
                    AVObject *imgShared = [query getObjectWithId:parameters[@"taskId"]];
                    [imgShared setObject:[NSNumber numberWithInt:[imgShared[@"likeCount"] intValue]-1] forKey:@"likeCount"];
                    [imgShared saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (succeeded) {
                            success(object);
                        } else {
                            failure(error);
                        }
                    }];
                } else {
                    failure(error);
                }
            }];
        } else {
            failure(error);
        }
    }];
}

@end
