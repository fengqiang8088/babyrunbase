//
//  PicIndexManager.h
//  BabyRun
//
//  Created by fengqiang on 14-11-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

@class ImgTag;

typedef void (^IMSuccessBlock)(id objects);
typedef void (^IMFailureBlock)(NSError *error);

@interface PicIndexManager : UIViewController

SINGLETON_INTERFACE(PicIndexManager)

//取得图片首页分类信息
- (void)getPicGroupList:(NSDictionary *)parameters
                success:(IMSuccessBlock)success
                failure:(IMFailureBlock)failure;

//取得图片首页信息
- (void)getPicIndexList:(NSDictionary *)parameters
                success:(IMSuccessBlock)success
                failure:(IMFailureBlock)failure;

//取得图片关注首页信息
- (void)getPicIndexForUserList:(NSDictionary *)parameters
                success:(IMSuccessBlock)success
                failure:(IMFailureBlock)failure;

//取得图片对应的标签信息
- (void)getPicTagList:(NSDictionary *)parameters
                success:(IMSuccessBlock)success
                failure:(IMFailureBlock)failure;

//取得标签对应的图片信息
- (void)getTagForPicList:(NSDictionary *)parameters
              success:(IMSuccessBlock)success
              failure:(IMFailureBlock)failure;

//取得图片对应的评论信息
- (void)getPicCommentList:(NSDictionary *)parameters
                  success:(IMSuccessBlock)success
                  failure:(IMFailureBlock)failure;

//取得图片详情页对应的喜欢信息最新6个数据
- (void)getPicLikeIndex:(NSDictionary *)parameters
                  success:(IMSuccessBlock)success
                  failure:(IMFailureBlock)failure;

//取得图片详情页对应的喜欢列表信息
- (void)getPicLikeList:(NSDictionary *)parameters
                success:(IMSuccessBlock)success
                failure:(IMFailureBlock)failure;

//取得标签的评论信息
- (void)getTagCommentList:(NSDictionary *)parameters
              success:(IMSuccessBlock)success
              failure:(IMFailureBlock)failure;

//添加图片帖评论
- (void)addImgSharedComment:(NSDictionary *)parameters
                    success:(IMSuccessBlock)success
                    failure:(IMFailureBlock)failure;

//删除图片帖评论
- (void)removeImgSharedComment:(NSDictionary *)parameters
                    success:(IMSuccessBlock)success
                    failure:(IMFailureBlock)failure;

//取得图片帖评论数量
- (void)getImgSharedCommentCount:(NSDictionary *)parameters
                       success:(IMSuccessBlock)success
                       failure:(IMFailureBlock)failure;

//添加图片帖喜欢
- (void)addImgSharedLike:(NSDictionary *)parameters
                    success:(IMSuccessBlock)success
                    failure:(IMFailureBlock)failure;

//取消图片帖喜欢
- (void)removeImgSharedLike:(NSDictionary *)parameters
                 success:(IMSuccessBlock)success
                 failure:(IMFailureBlock)failure;

//添加标签评论
- (void)addImgTagComment:(NSDictionary *)parameters
                 success:(IMSuccessBlock)success
                 failure:(IMFailureBlock)failure;

//删除标签评论
- (void)removeImgTagComment:(NSDictionary *)parameters
                       success:(IMSuccessBlock)success
                       failure:(IMFailureBlock)failure;

//删除图片帖
- (void)removeImgShared:(NSString *)objectId
                    success:(IMSuccessBlock)success
                    failure:(IMFailureBlock)failure;

//取得图片首页分类信息
- (void)getPicGroupListForCloud:(NSDictionary *)parameters
                success:(IMSuccessBlock)success
                failure:(IMFailureBlock)failure;
@end
