//
//  UserManager.m
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "UserManager.h"
#import "ImgShared.h"
#import "TextShared.h"
#import "Personnel.h"
#import "NotiMessage.h"

@implementation UserManager

SINGLETON_IMPLEMENTATION(UserManager)

-(void)getPostPicList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"imgShared"];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    query.limit = 10;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    AVUser *user = parameters[@"user"];
    if (user) {
        [query whereKey:@"userid" equalTo:user.objectId];
    }
    if(parameters){
        if (parameters[@"skip"]) {
            query.skip = [parameters[@"skip"] integerValue];
        }
    }
    
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *ImgSharedArray = [[NSMutableArray alloc] init];
        for (AVObject *object in objects) {
    
            ImgShared *shared = [[ImgShared alloc] init];
            shared.user = user;
            shared.imgUrl = object[@"imgUrl"];
            shared.readCount = object[@"readCount"];
            shared.commentCount = object[@"commentCount"];
            shared.likeCount = object[@"praiseCount"];
            shared.updateTime = object.createdAt;
            shared.objectId = object.objectId;
            
            [ImgSharedArray addObject:shared];
            [object incrementKey:@"readCount"];
            [object saveInBackground];
        }
        
        //查询用户之间的所属关系
        if (![[AVUser currentUser].objectId isEqualToString:user.objectId]) {
            int relation = 0;
            AVQuery *queryfe = [AVUser currentUser].followeeQuery;
            [queryfe includeKey:@"followee"];
            [queryfe whereKey:@"followee" equalTo:user];
            if ([queryfe getFirstObject]) {
                relation = 1;
                AVQuery *queryfr = [AVUser currentUser].followerQuery;
                [queryfr includeKey:@"follower"];
                [queryfr whereKey:@"follower" equalTo:user];
                if ([queryfr getFirstObject]) {
                    relation = 2;
                }
            }
            for (ImgShared *shared in ImgSharedArray) {
                shared.relation = relation;
            }
        }
        
        if (!error) {
            success(ImgSharedArray);
        } else {
            failure(error);
        }
    }];
}

-(void)getPostTextList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"textShared"];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    query.limit = 10;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    AVUser *user = parameters[@"user"];
    if (user) {
        [query whereKey:@"userid" equalTo:user.objectId];
    }
    if(parameters){
        if (parameters[@"skip"]) {
            query.skip = [parameters[@"skip"] integerValue];
        }
    }
    
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *ImgSharedArray = [[NSMutableArray alloc] init];
        for (AVObject *object in objects) {
            
            TextShared *shared = [[TextShared alloc] init];
            shared.user = user;
            shared.textBackground = [object[@"textBackground"] stringValue];
            shared.sharedTitle = object[@"sharedTitle"];
            shared.sharedGroup = [object[@"sharedGroup"] intValue];
            shared.readCount = object[@"readCount"];
            shared.commentCount = object[@"commentCount"];
            shared.likeCount = object[@"praiseCount"];
            shared.updateTime = object.createdAt;
            shared.objectId = object.objectId;
            
            [ImgSharedArray addObject:shared];
            [object incrementKey:@"readCount"];
            [object saveInBackground];
        }
        
        if (!error) {
            success(ImgSharedArray);
        } else {
            failure(error);
        }
    }];
}

-(void)findUserList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    //取得主要类型
    AVQuery *query = [AVUser query];
    //按生成时间排序
    [query whereKey:@"username" hasPrefix:parameters[@"username"]];
    [query whereKey:@"objectId" notEqualTo:[AVUser currentUser].objectId];
    
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *ResultArray = [[NSMutableArray alloc] init];
        for (AVUser *user in objects) {
            ViewUser *userObject = [ViewUser alloc];
            userObject.user = user;
            //查询用户之间的所属关系
            AVQuery *queryfe = [AVUser currentUser].followeeQuery;
            [queryfe includeKey:@"followee"];
            [queryfe whereKey:@"followee" equalTo:user];
            if ([queryfe getFirstObject]) {
                userObject.relation = 1;
                AVQuery *queryfr = [AVUser currentUser].followerQuery;
                [queryfr includeKey:@"follower"];
                [queryfr whereKey:@"follower" equalTo:user];
                if ([queryfr getFirstObject]) {
                    userObject.relation = 2;
                }
            }
            else{
                userObject.relation = 0;
            }
            
            [ResultArray addObject:userObject];
        }
        
        if (!error) {
            success(ResultArray);
        } else {
            failure(error);
        }
    }];
}

-(void)getFavList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    //取得主要类型
    AVQuery *query = [AVUser followeeQuery:parameters[@"objectId"]];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *ResultArray = [[NSMutableArray alloc] init];
        for (AVUser *user in objects) {
            ViewUser *userObject = [ViewUser alloc];
            userObject.user = user;
            //查询用户之间的所属关系
            AVQuery *queryfe = [AVUser currentUser].followeeQuery;
            [queryfe includeKey:@"followee"];
            [queryfe whereKey:@"followee" equalTo:user];
            if ([queryfe getFirstObject]) {
                userObject.relation = 1;
                AVQuery *queryfr = [AVUser currentUser].followerQuery;
                [queryfr includeKey:@"follower"];
                [queryfr whereKey:@"follower" equalTo:user];
                if ([queryfr getFirstObject]) {
                    userObject.relation = 2;
                }
            }
            else{
                userObject.relation = 0;
            }
            
            [ResultArray addObject:userObject];
        }
        
        if (!error) {
            success(ResultArray);
        } else {
            failure(error);
        }
    }];
}

-(void)getFansList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVUser followerQuery:parameters[@"objectId"]];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *ResultArray = [[NSMutableArray alloc] init];
        for (AVUser *user in objects) {
            ViewUser *userObject = [ViewUser alloc];
            userObject.user = user;
            //查询用户之间的所属关系
            AVQuery *queryfe = [AVUser currentUser].followeeQuery;
            [queryfe includeKey:@"followee"];
            [queryfe whereKey:@"followee" equalTo:user];
            if ([queryfe getFirstObject]) {
                userObject.relation = 1;
                AVQuery *queryfr = [AVUser currentUser].followerQuery;
                [queryfr includeKey:@"follower"];
                [queryfr whereKey:@"follower" equalTo:user];
                if ([queryfr getFirstObject]) {
                    userObject.relation = 2;
                }
            }
            else{
                userObject.relation = 0;
            }
            
            [ResultArray addObject:userObject];
        }
        
        if (!error) {
            success(ResultArray);
        } else {
            failure(error);
        }
    }];
}

-(void)getPersonList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"personnel"];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *resultArray = [[NSMutableArray alloc] init];
        for (AVObject *object in objects) {
            
            Personnel *person = [[Personnel alloc] init];
            person.imgUrl = object[@"imgUrl"];
            person.name = object[@"personName"];
            person.position = object[@"position"];
            person.updateTime = object.createdAt;
            person.objectId = object.objectId;
            
            [resultArray addObject:person];
        }
        
        if (!error) {
            success(resultArray);
        } else {
            failure(error);
        }
    }];
}

-(void)getUserMessageList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVStatusQuery *query = [AVStatus inboxQuery:@"BabyRunMessage"];
    //需要同时附带发送者的数据
    [query includeKey:@"source"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //获得AVStatus数组
        if (!error) {
            NSMutableArray *resultArray = [[NSMutableArray alloc] init];
            for (AVStatus *object in objects) {
                
                NotiMessage *message = [[NotiMessage alloc] init];
                message.user = (AVUser *)object.source;
                if(message.user){
                    //查询用户之间的所属关系
                    AVQuery *queryfe = [AVUser currentUser].followeeQuery;
                    [queryfe includeKey:@"followee"];
                    [queryfe whereKey:@"followee" equalTo:message.user];
                    if ([queryfe getFirstObject]) {
                        message.relation = 1;
                        AVQuery *queryfr = [AVUser currentUser].followerQuery;
                        [queryfr includeKey:@"follower"];
                        [queryfr whereKey:@"follower" equalTo:message.user];
                        if ([queryfr getFirstObject]) {
                            message.relation = 2;
                        }
                    }
                    else{
                        message.relation = 0;
                    }
                }
                message.content = object.data[@"content"];
                message.updateTime = object.createdAt;
                message.type = [object.data[@"messageType"] intValue];
                message.targetContent = object.data[@"targetContent"];
                message.messageId = object.messageId;
                message.targetId = object.data[@"targetId"];
                
                [resultArray addObject:message];
            }
            success(resultArray);
        } else {
            failure(error);
        }
    }];
}

-(void)getUserLikeList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    AVStatusQuery *query = [AVStatus inboxQuery:@"BabyRunLike"];
    //需要同时附带发送者的数据
    [query includeKey:@"source"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //获得AVStatus数组
        if (!error) {
            NSMutableArray *resultArray = [[NSMutableArray alloc] init];
            for (AVStatus *object in objects) {
                
                NotiMessage *message = [[NotiMessage alloc] init];
                message.user = (AVUser *)object.source;
                if(message.user){
                    //查询用户之间的所属关系
                    AVQuery *queryfe = [AVUser currentUser].followeeQuery;
                    [queryfe includeKey:@"followee"];
                    [queryfe whereKey:@"followee" equalTo:message.user];
                    if ([queryfe getFirstObject]) {
                        message.relation = 1;
                        AVQuery *queryfr = [AVUser currentUser].followerQuery;
                        [queryfr includeKey:@"follower"];
                        [queryfr whereKey:@"follower" equalTo:message.user];
                        if ([queryfr getFirstObject]) {
                            message.relation = 2;
                        }
                    }
                    else{
                        message.relation = 0;
                    }
                }
                message.content = object.data[@"content"];
                message.updateTime = object.createdAt;
                message.targetClass = object.data[@"targetClass"];
                message.targetId = object.data[@"targetId"];
                message.type = [object.data[@"messageType"] intValue];
                message.targetContent = object.data[@"targetContent"];
                message.targetId = object.data[@"targetId"];
                
                [resultArray addObject:message];
            }
            success(resultArray);
        } else {
            failure(error);
        }
    }];
}

-(void)getUserDynamicList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    AVStatusQuery *query = [AVStatus inboxQuery:@"BabyRunDynamic"];
    //需要同时附带发送者的数据
    [query includeKey:@"source"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //获得AVStatus数组
        if (!error) {
            NSMutableArray *resultArray = [[NSMutableArray alloc] init];
            for (AVStatus *object in objects) {
                
                NotiMessage *message = [[NotiMessage alloc] init];
                message.user = (AVUser *)object.source;
                if(message.user){
                    //查询用户之间的所属关系
                    AVQuery *queryfe = [AVUser currentUser].followeeQuery;
                    [queryfe includeKey:@"followee"];
                    [queryfe whereKey:@"followee" equalTo:message.user];
                    if ([queryfe getFirstObject]) {
                        message.relation = 1;
                        AVQuery *queryfr = [AVUser currentUser].followerQuery;
                        [queryfr includeKey:@"follower"];
                        [queryfr whereKey:@"follower" equalTo:message.user];
                        if ([queryfr getFirstObject]) {
                            message.relation = 2;
                        }
                    }
                    else{
                        message.relation = 0;
                    }
                }
                message.content = object.data[@"content"];
                message.updateTime = object.createdAt;
                message.targetClass = object.data[@"targetClass"];
                message.targetId = object.data[@"targetId"];
                message.type = [object.data[@"messageType"] intValue];
                message.targetContent = object.data[@"targetContent"];
                message.targetId = object.data[@"targetId"];
                
                [resultArray addObject:message];
            }
            success(resultArray);
        } else {
            failure(error);
        }
    }];
}

- (void)addUserMessage:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVStatus *status=[[AVStatus alloc] init];
    switch ([parameters[@"statusType"] intValue]) {
        case 0:
        {
            status.type = @"BabyRunMessage";
            [status setData:@{@"content":parameters[@"content"]
                              ,@"messageType":parameters[@"messageType"]
                              ,@"targetContent":parameters[@"targetContent"]?:@""
                              ,@"targetId":parameters[@"targetId"]?:@""}];
        }
            break;
        case 1:
        {
            status.type = @"BabyRunLike";
            [status setData:@{@"content":parameters[@"content"]
                              ,@"targetId":parameters[@"targetId"]
                              ,@"targetClass":parameters[@"targetClass"]
                              ,@"messageType":parameters[@"messageType"]
                              ,@"targetContent":parameters[@"targetContent"]}];
        }
            break;
        default:
            break;
    }
    
    
    AVQuery *query = [AVUser query];
    [query whereKey:@"objectId" equalTo:parameters[@"objectId"]];
    [status setQuery:query];
    [status sendInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] init];
            [resultDic setObject:@"1" forKey:@"success"];
            success(resultDic);
        } else {
            failure(error);
        }
    }];
}

- (void)pushMessageToFans:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVStatus *status=[[AVStatus alloc] init];
    status.type = @"BabyRunDynamic";
    
    [status setData:@{@"content":parameters[@"content"]
                      ,@"targetId":parameters[@"targetId"]
                      ,@"targetClass":parameters[@"targetClass"]
                      ,@"messageType":parameters[@"messageType"]
                      ,@"targetContent":parameters[@"targetContent"]}];
    
    
    [AVStatus sendStatusToFollowers:status andCallback:^(BOOL succeeded, NSError *error) {
        if (!error) {
            NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] init];
            [resultDic setObject:@"1" forKey:@"success"];
            success(resultDic);
        } else {
            failure(error);
        }
    }];
}

//查找用户名是否存在
- (bool)isHaveUserName:(NSString *)userName{
    AVQuery *query = [AVUser query];
    [query whereKey:@"username" equalTo:userName];
    if ([AVUser currentUser]!=nil) {
        [query whereKey:@"objectId" notEqualTo:[AVUser currentUser].objectId];
    }
    if ([query findObjects].count > 0) {
        return YES;
    }
    else {
        return NO;
    }
}

-(void)getUserNotReadMessageCount:(IMSuccessInteger)success failure:(IMFailureBlock)failure{
    [AVStatus getUnreadStatusesCountWithType:@"BabyRunMessage" andCallback:^(NSInteger number1, NSError *error) {
        if (!error) {
            [AVStatus getUnreadStatusesCountWithType:@"BabyRunLike" andCallback:^(NSInteger number2, NSError *error) {
                if (!error) {
                    success(number1+number2);
                } else {
                    failure(error);
                }
            }];
        } else {
            failure(error);
        }
    }];
}

- (ViewUser *)getUserRelation:(AVUser *)user{
    
    ViewUser *relationUser = [[ViewUser alloc] init];
    relationUser.user = user;
    
    //查询用户之间的所属关系
    AVQuery *query = [AVUser currentUser].followeeQuery;
    [query includeKey:@"followee"];
    [query whereKey:@"followee" equalTo:user];
    if ([query getFirstObject]) {
        relationUser.relation = 1;
        AVQuery *queryfr = [AVUser currentUser].followerQuery;
        [queryfr includeKey:@"follower"];
        [queryfr whereKey:@"follower" equalTo:user];
        if ([queryfr getFirstObject]) {
            relationUser.relation = 2;
        }
    }
    else{
        relationUser.relation = 0;
    }
    return relationUser;
}

-(void)getFansForTaskList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVUser followerQuery:[AVUser currentUser].objectId];
    //设置缓存策略
    //query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *ResultArray = [[NSMutableArray alloc] init];
        for (AVUser *user in objects) {
            ViewUser *userObject = [ViewUser alloc];
            userObject.user = user;
            //查询用户之间的所属关系
            AVQuery *queryfe = [AVQuery queryWithClassName:@"taskInvite"];
            [queryfe whereKey:@"inviteId" equalTo:user.objectId];
            [queryfe whereKey:@"taskId" equalTo:parameters[@"taskId"]];
            if ([queryfe getFirstObject]) {
                userObject.relation = 1;
            }
            else{
                userObject.relation = 0;
            }
            
            [ResultArray addObject:userObject];
        }
        
        if (!error) {
            success(ResultArray);
        } else {
            failure(error);
        }
    }];
}

- (void)addInviteToUser:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVObject *object = [AVObject objectWithClassName:@"taskInvite"];
    [object setObject:parameters[@"inviteId"] forKey:@"inviteId"];
    [object setObject:parameters[@"taskId"] forKey:@"taskId"];
    [object setObject:[AVUser currentUser].objectId forKey:@"userId"];
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            success(object);
        } else {
            failure(error);
        }
    }];
}

@end
