//
//  UserManager.h
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//
#import "ViewUser.h"

typedef void (^IMSuccessBlock)(id objects);
typedef void (^IMSuccessInteger)(NSInteger objects);
typedef void (^IMFailureBlock)(NSError *error);

@interface UserManager : NSObject

SINGLETON_INTERFACE(UserManager)

//取得用户发布图片信息
- (void)getPostPicList:(NSDictionary *)parameters
                   success:(IMSuccessBlock)success
                   failure:(IMFailureBlock)failure;

//取得用户发布文字信息
- (void)getPostTextList:(NSDictionary *)parameters
                     success:(IMSuccessBlock)success
                     failure:(IMFailureBlock)failure;

//取得用户关注列表
- (void)getFavList:(NSDictionary *)parameters
                success:(IMSuccessBlock)success
                failure:(IMFailureBlock)failure;

//取得用户粉丝列表
- (void)getFansList:(NSDictionary *)parameters
                success:(IMSuccessBlock)success
                failure:(IMFailureBlock)failure;

//查找用户列表
- (void)findUserList:(NSDictionary *)parameters
            success:(IMSuccessBlock)success
            failure:(IMFailureBlock)failure;

//取得反馈团队信息
- (void)getPersonList:(NSDictionary *)parameters
            success:(IMSuccessBlock)success
            failure:(IMFailureBlock)failure;

//取得消息中心－消息列表
- (void)getUserMessageList:(NSDictionary *)parameters
              success:(IMSuccessBlock)success
              failure:(IMFailureBlock)failure;

//取得消息中心－赞的列表
- (void)getUserLikeList:(NSDictionary *)parameters
              success:(IMSuccessBlock)success
              failure:(IMFailureBlock)failure;

//取得消息中心－动态列表
- (void)getUserDynamicList:(NSDictionary *)parameters
              success:(IMSuccessBlock)success
              failure:(IMFailureBlock)failure;

//发送消息到指定的用户
- (void)addUserMessage:(NSDictionary *)parameters
             success:(IMSuccessBlock)success
               failure:(IMFailureBlock)failure;

//发送消息到所有的粉丝用户
- (void)pushMessageToFans:(NSDictionary *)parameters
               success:(IMSuccessBlock)success
               failure:(IMFailureBlock)failure;

//查找用户名是否存在
- (bool)isHaveUserName:(NSString *)userName;

//取得未读消息数量
- (void)getUserNotReadMessageCount:(IMSuccessInteger)success
                           failure:(IMFailureBlock)failure;

//取得当前用户与传入用户的关系
- (ViewUser *)getUserRelation:(AVUser *)user;

//取得用户粉丝列表并加入是否已经邀请的信息
- (void)getFansForTaskList:(NSDictionary *)parameters
            success:(IMSuccessBlock)success
            failure:(IMFailureBlock)failure;

//发送邀请到指定的用户
- (void)addInviteToUser:(NSDictionary *)parameters
               success:(IMSuccessBlock)success
               failure:(IMFailureBlock)failure;
@end
