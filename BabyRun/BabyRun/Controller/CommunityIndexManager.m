//
//  CommunityIndexManager.m
//  BabyRun
//
//  Created by fengqiang on 14-12-15.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "CommunityIndexManager.h"
#import "TextShared.h"
#import "Comment.h"
#import "ViewUser.h"

@implementation CommunityIndexManager

SINGLETON_IMPLEMENTATION(CommunityIndexManager)

- (void)getTextGroupList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"textGroup"];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    //[query orderByDescending:@"orderIndex"];
    //每页取得的记录数,并做分页处理
    
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (AVObject *object in objects) {
                [[NSUserDefaults standardUserDefaults] setObject:object[@"textGroupName"] forKey:[NSString stringWithFormat:@"textGroup_%@",object[@"textGroup"]]];
            }
            
            success(objects);
        } else {
            failure(error);
        }
    }];
}

- (void)getCommunityIndexList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"textShared"];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    //每页取得的记录数,并做分页处理
    query.limit = 10;
    if(parameters){
        if (parameters[@"skip"]) {
            query.skip = [parameters[@"skip"] integerValue];
        }
        //取得对应当前TextGroup的结果集合
        if (parameters[@"textGroup"]) {
            [query whereKey:@"sharedGroup" equalTo:[NSNumber numberWithInteger:[parameters[@"textGroup"] integerValue]]];
        }
        if (parameters[@"objectId"]) {
            [query whereKey:@"objectId" equalTo:parameters[@"objectId"]];
        }
    }
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *ImgSharedArray = [[NSMutableArray alloc] init];
        for (AVObject *object in objects) {
            AVQuery *query = [AVUser query];
            AVUser *user = (AVUser *)[query getObjectWithId:object[@"userid"]];
            if (user) {
                TextShared *shared = [[TextShared alloc] init];
                shared.user = user;
                shared.textBackground = [object[@"textBackground"] stringValue];
                shared.sharedTitle = object[@"sharedTitle"];
                shared.sharedGroup = [object[@"sharedGroup"] intValue];
                shared.readCount = object[@"readCount"];
                shared.commentCount = object[@"commentCount"];
                shared.likeCount = object[@"praiseCount"];
                shared.updateTime = object.createdAt;
                shared.objectId = object.objectId;
                [ImgSharedArray addObject:shared];
                
                [object incrementKey:@"readCount"];
                [object saveInBackground];
            }
        }
        if (!error) {
            success(ImgSharedArray);
        } else {
            failure(error);
        }
    }];
}

- (void)getCommunityIndexForUserList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    AVQuery *favsQuery = [AVUser followeeQuery:[AVUser currentUser].objectId];
    [favsQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *favsResult = [[NSMutableArray alloc] init];
        for (AVUser *user in objects) {
            [favsResult addObject:user.objectId];
        }
        if (favsResult.count>0) {
            //取得主要类型
            AVQuery *query = [AVQuery queryWithClassName:@"textShared"];
            //设置缓存策略
            query.cachePolicy = kPFCachePolicyNetworkElseCache;
            //按生成时间排序
            [query orderByDescending:@"createdAt"];
            [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
            //每页取得的记录数,并做分页处理
            query.limit = 10;
            query.skip = [parameters[@"skip"] integerValue];
            [query whereKey:@"userid" containedIn:favsResult];
            
            //生成展示Object
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                NSMutableArray *ImgSharedArray = [[NSMutableArray alloc] init];
                for (AVObject *object in objects) {
                    AVQuery *query = [AVUser query];
                    AVUser *user = (AVUser *)[query getObjectWithId:object[@"userid"]];
                    if (user) {
                        TextShared *shared = [[TextShared alloc] init];
                        shared.user = user;
                        shared.textBackground = [object[@"textBackground"] stringValue];
                        shared.sharedTitle = object[@"sharedTitle"];
                        shared.sharedGroup = [object[@"sharedGroup"] intValue];
                        shared.readCount = object[@"readCount"];
                        shared.commentCount = object[@"commentCount"];
                        shared.likeCount = object[@"praiseCount"];
                        shared.updateTime = object.createdAt;
                        shared.objectId = object.objectId;
                        [ImgSharedArray addObject:shared];
                        
                        [object incrementKey:@"readCount"];
                        [object saveInBackground];
                    }
                }
                if (!error) {
                    success(ImgSharedArray);
                } else {
                    failure(error);
                }
            }];
        }
        else{
            success(nil);
        }
    }];
}

- (void)getCommunityCommentList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"textComment"];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    if(parameters[@"objectId"]!=nil){
        [query whereKey:@"sharedId" equalTo:parameters[@"objectId"]];
        
        //生成展示Object
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            NSMutableArray *resultArray = [[NSMutableArray alloc] init];
            for (AVObject *object in objects) {
                AVQuery *query = [AVUser query];
                AVUser *user = (AVUser *)[query getObjectWithId:object[@"userId"]];
                
                Comment *comment = [[Comment alloc] init];
                comment.user = user;
                comment.objectId = object.objectId;
                comment.content = object[@"content"];
                comment.updateTime = object.createdAt;
                [resultArray addObject:comment];
            }
            
            if (!error) {
                success(resultArray);
            } else {
                failure(error);
            }
        }];
    }
}

- (void)getTextSharedCommentCount:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    AVQuery *query = [AVQuery queryWithClassName:@"textComment"];
    [query whereKey:@"sharedId" equalTo:parameters[@"objectId"]];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    [query countObjectsInBackgroundWithBlock:^(NSInteger count, NSError *error) {
        if (!error) {
            success([NSNumber numberWithInteger:count]);
        } else {
            failure(error);
        }
    }];
}


- (void)getCommunityLikeIndex:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"textPraise"];
    [query whereKey:@"sharedId" equalTo:parameters[@"objectId"]];
    [query orderByDescending:@"createdAt"];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    query.limit = 6;
    [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
        if (!error) {
            NSMutableArray *resultArray = [[NSMutableArray alloc] init];
            for (AVObject *object in results) {
                AVQuery *query = [AVUser query];
                AVUser *user = (AVUser *)[query getObjectWithId:object[@"userId"]];
                if(user){
                    [resultArray addObject:user];
                }
            }
            success(resultArray);
        } else {
            failure(error);
        }
    }];
}

- (void)getCommunityLikeList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:parameters[@"objectClass"]];
    [query whereKey:parameters[@"objectKey"] equalTo:parameters[@"objectValue"]];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    
    query.limit = 20;
    if(parameters){
        if (parameters[@"skip"]) {
            query.skip = [parameters[@"skip"] integerValue];
        }
    }
    [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
        if (!error) {
            NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] init];
            NSMutableArray *resultArray = [[NSMutableArray alloc] init];
            for (AVObject *object in results) {
                ViewUser *viewUser = [[ViewUser alloc] init];
                //查询对应的用户
                AVQuery *query = [AVUser query];
                viewUser.user = (AVUser *)[query getObjectWithId:object[@"userId"]];
                if (![viewUser.user.objectId isEqualToString:[AVUser currentUser].objectId]) {
                    //查询用户之间的所属关系
                    AVQuery *query1 = [AVUser currentUser].followeeQuery;
                    [query1 includeKey:@"followee"];
                    [query1 whereKey:@"followee" equalTo:viewUser.user];
                    if ([query1 getFirstObject]) {
                        viewUser.relation = 1;
                        AVQuery *query2 = [AVUser currentUser].followerQuery;
                        [query2 includeKey:@"follower"];
                        [query2 whereKey:@"follower" equalTo:viewUser.user];
                        if ([query2 getFirstObject]) {
                            viewUser.relation = 2;
                        }
                    }
                    else{
                        viewUser.relation = 0;
                    }
                    [resultArray addObject:viewUser];
                }
                else{
                    [resultDic setObject:@"YES" forKey:@"isLiked"];
                }
            }
            [resultDic setObject:resultArray forKey:@"userList"];
            success(resultDic);
        } else {
            failure(error);
        }
    }];
}

- (void)addTextSharedComment:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVObject *object = [AVObject objectWithClassName:@"textComment"];
    [object setObject:parameters[@"sharedId"] forKey:@"sharedId"];
    [object setObject:parameters[@"content"] forKey:@"content"];
    [object setObject:parameters[@"userId"] forKey:@"userId"];
    [object setObject:[NSNumber numberWithInt:0] forKey:@"deleteFlag"];
    [object setObject:@"-1" forKey:@"parentId"];
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            AVQuery *query = [AVQuery queryWithClassName:@"textShared"];
            AVObject *imgShared = [query getObjectWithId:parameters[@"sharedId"]];
            [imgShared addUniqueObject:object.objectId forKey:@"commentArray"];
            [imgShared incrementKey:@"commentCount"];
            [imgShared saveInBackground];
            
            success(object);
        } else {
            failure(error);
        }
    }];
}

- (void)removeTextSharedComment:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"textComment"];
    [query whereKey:@"objectId" equalTo:parameters[@"objectId"]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            AVObject *object = objects[0];
            [object setObject:[NSNumber numberWithInt:1] forKey:@"deleteFlag"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    AVQuery *query = [AVQuery queryWithClassName:@"textShared"];
                    AVObject *textShared = [query getObjectWithId:parameters[@"sharedId"]];
                    [textShared setObject:[NSNumber numberWithInt:[textShared[@"commentCount"] intValue]-1] forKey:@"commentCount"];
                    [textShared saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (succeeded) {
                            success(object);
                        } else {
                            failure(error);
                        }
                    }];
                } else {
                    failure(error);
                }
            }];
        } else {
            failure(error);
        }
    }];
}

- (void)addTextSharedLike:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVObject *object = [AVObject objectWithClassName:@"textPraise"];
    [object setObject:parameters[@"sharedId"] forKey:@"sharedId"];
    [object setObject:parameters[@"userId"] forKey:@"userId"];
    [object setObject:[NSNumber numberWithInt:0] forKey:@"deleteFlag"];
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            AVQuery *query = [AVQuery queryWithClassName:@"textShared"];
            AVObject *imgShared = [query getObjectWithId:parameters[@"sharedId"]];
            [imgShared incrementKey:@"praiseCount"];
            [imgShared saveInBackground];
            success(object);
        } else {
            failure(error);
        }
    }];
}

- (void)removeTextSharedLike:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"textPraise"];
    [query whereKey:@"sharedId" equalTo:parameters[@"sharedId"]];
    [query whereKey:@"userId" equalTo:parameters[@"userId"]];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            AVObject *object = objects[0];
            [object setObject:[NSNumber numberWithInt:1] forKey:@"deleteFlag"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    AVQuery *query = [AVQuery queryWithClassName:@"textShared"];
                    AVObject *imgShared = [query getObjectWithId:parameters[@"sharedId"]];
                    [imgShared setObject:[NSNumber numberWithInt:[imgShared[@"praiseCount"] intValue]-1] forKey:@"praiseCount"];
                    [imgShared saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (succeeded) {
                            success(object);
                        } else {
                            failure(error);
                        }
                    }];
                } else {
                    failure(error);
                }
            }];
        } else {
            failure(error);
        }
    }];
}

- (void)removeTextShared:(NSString *)objectId success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"textPraise"];
    [query whereKey:@"objectId" equalTo:objectId];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            AVObject *object = objects[0];
            [object setObject:[NSNumber numberWithInt:1] forKey:@"deleteFlag"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    success(object);
                } else {
                    failure(error);
                }
            }];
        } else {
            failure(error);
        }
    }];
}

@end
