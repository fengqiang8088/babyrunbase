//
//  PicIndexManager.m
//  BabyRun
//
//  Created by fengqiang on 14-11-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "PicIndexManager.h"

#import "ImgShared.h"
#import "ImgTag.h"
#import "Comment.h"
#import "ViewUser.h"

@interface PicIndexManager ()

@end

@implementation PicIndexManager

SINGLETON_IMPLEMENTATION(PicIndexManager)

- (void)getPicGroupList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"imageGroup"];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    //[query orderByDescending:@"orderIndex"];
    //每页取得的记录数,并做分页处理
    
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            success(objects);
        } else {
            failure(error);
        }
    }];
}

- (void)getPicIndexList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"imgShared"];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    //每页取得的记录数,并做分页处理
    query.limit = 10;
    if(parameters){
        if (parameters[@"skip"]) {
            query.skip = [parameters[@"skip"] integerValue];
        }
        //取得对应当前ImgGroup的结果集合
        if (parameters[@"imgGroup"]) {
            [query whereKey:@"imgGroup" equalTo:parameters[@"imgGroup"]];
        }
        //只取得对应当前标签的结果集合
        if (parameters[@"imgTagId"]) {
            [query whereKey:@"tagArray" equalTo:parameters[@"imgTagId"]];
        }
        if(parameters[@"objectId"]){
            [query whereKey:@"objectId" equalTo:parameters[@"objectId"]];
        }
    }
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *resultArray = [[NSMutableArray alloc] init];
        for (AVObject *object in objects) {
            AVQuery *query = [AVUser query];
            AVUser *user = (AVUser *)[query getObjectWithId:object[@"userid"]];
            if(user){
                ImgShared *shared = [[ImgShared alloc] init];
                shared.user = user;
                shared.imgText = object[@"sharedTitle"];
                shared.imgUrl = object[@"imgUrl"];
                shared.readCount = object[@"readCount"];
                shared.commentCount = object[@"commentCount"];
                shared.likeCount = object[@"praiseCount"];
                shared.tags = object[@"tagArray"];
                shared.updateTime = object.createdAt;
                shared.objectId = object.objectId;
                [resultArray addObject:shared];
                
                [object incrementKey:@"readCount"];
                [object saveInBackground];
            }
        }
        
        if (!error) {
            success(resultArray);
        } else {
            failure(error);
        }
    }];
}

- (void)getPicIndexForUserList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *favsQuery = [AVUser followeeQuery:[AVUser currentUser].objectId];
    [favsQuery selectKeys:@[@"objectId"]];
    [favsQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *resultf = [[NSMutableArray alloc] init];
        for (AVUser *user in objects) {
            [resultf addObject:user.objectId];
        }
        NSMutableArray *result = [[NSMutableArray alloc] init];
        if (resultf.count>0) {
            //取得主要类型
            AVQuery *query = [AVQuery queryWithClassName:@"imgShared"];
            [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
            //设置缓存策略
            query.cachePolicy = kPFCachePolicyNetworkElseCache;
            //按生成时间排序
            [query orderByDescending:@"createdAt"];
            //每页取得的记录数,并做分页处理
            query.limit = 10;
            query.skip = [parameters[@"skip"] integerValue];
            [query whereKey:@"userid" containedIn:resultf];
            
            //生成展示Object
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                NSMutableArray *resultArray = [[NSMutableArray alloc] init];
                for (AVObject *object in objects) {
                    AVUser *user = (AVUser *)[[AVUser query] getObjectWithId:object[@"userid"]];
                    if(user){
                        ImgShared *shared = [[ImgShared alloc] init];
                        shared.user = user;
                        shared.imgText = object[@"sharedTitle"];
                        shared.imgUrl = object[@"imgUrl"];
                        shared.readCount = object[@"readCount"];
                        shared.commentCount = object[@"commentCount"];
                        shared.likeCount = object[@"praiseCount"];
                        shared.tags = object[@"tagArray"];
                        shared.updateTime = object.createdAt;
                        shared.objectId = object.objectId;
                        [resultArray addObject:shared];
                    }
                }
                if (!error) {
                    success(resultArray);
                } else {
                    failure(error);
                }
            }];
        }
        else{
            success(result);
        }
    }];
}

- (void)getPicTagList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"imgTag"];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    [query whereKey:@"sharedId" equalTo:parameters[@"sharedId"]];
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *resultArray = [[NSMutableArray alloc] init];
        for (AVObject *object in objects) {
            ImgTag *imgTag = [[ImgTag alloc] init];
            imgTag.tagId = object.objectId;
            imgTag.tagText = object[@"localData"][@"tagContent"];
            imgTag.tagGroup = object[@"localData"][@"tagGroup"];
            imgTag.tagPosition = CGPointMake([object[@"localData"][@"tagPosition"][0] doubleValue],[object[@"localData"][@"tagPosition"][1] doubleValue]);
            imgTag.tagOrientation = [object[@"localData"][@"orientation"] intValue];
            
            [resultArray addObject:imgTag];
        }
        
        if (!error) {
            success(resultArray);
        } else {
            failure(error);
        }
    }];
}

- (void)getTagForPicList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"imgShared"];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    //每页取得的记录数,并做分页处理
    query.limit = 12;
    if(parameters){
        if (parameters[@"skip"]) {
            query.skip = [parameters[@"skip"] integerValue];
        }
        //取得图片贴就合
        if (parameters[@"imgTagId"]) {
            [query whereKey:@"tagArray" containsAllObjectsInArray:@[parameters[@"imgTagId"]]];
            //生成展示Object
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                NSMutableArray *resultArray = [[NSMutableArray alloc] init];
                for (AVObject *object in objects) {
                    AVQuery *query = [AVUser query];
                    AVUser *user = (AVUser *)[query getObjectWithId:object[@"userid"]];
                    if(user){
                        ImgShared *shared = [[ImgShared alloc] init];
                        shared.user = user;
                        shared.imgText = object[@"sharedTitle"];
                        shared.imgUrl = object[@"imgUrl"];
                        shared.readCount = object[@"readCount"];
                        shared.commentCount = object[@"commentCount"];
                        shared.likeCount = object[@"praiseCount"];
                        shared.tags = object[@"tagArray"];
                        shared.updateTime = object.createdAt;
                        shared.objectId = object.objectId;
                        
                        [resultArray addObject:shared];
                        [object incrementKey:@"readCount"];
                        [object saveInBackground];
                    }
                }
                
                if (!error) {
                    success(resultArray);
                } else {
                    failure(error);
                }
            }];
        }
    }
}

- (void)getPicCommentList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"imgComment"];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    if(parameters[@"objectId"]!=nil){
        [query whereKey:@"sharedId" equalTo:parameters[@"objectId"]];
        
        //生成展示Object
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            NSMutableArray *resultArray = [[NSMutableArray alloc] init];
            for (AVObject *object in objects) {
                AVQuery *query = [AVUser query];
                AVUser *user = (AVUser *)[query getObjectWithId:object[@"userId"]];
                
                Comment *comment = [[Comment alloc] init];
                comment.user = user;
                comment.objectId = object.objectId;
                comment.content = object[@"content"];
                comment.updateTime = object.createdAt;
                [resultArray addObject:comment];
            }
            
            if (!error) {
                success(resultArray);
            } else {
                failure(error);
            }
        }];
    }
}

- (void)getPicLikeIndex:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"imgPraise"];
    [query whereKey:@"sharedId" equalTo:parameters[@"objectId"]];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    query.limit = 6;
    [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
        if (!error) {
            NSMutableArray *resultArray = [[NSMutableArray alloc] init];
            for (AVObject *object in results) {
                AVQuery *query = [AVUser query];
                AVUser *user = (AVUser *)[query getObjectWithId:object[@"userId"]];
                if(user){
                    [resultArray addObject:user];
                }
            }
            success(resultArray);
        } else {
            failure(error);
        }
    }];
}

- (void)getPicLikeList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{

    AVQuery *query = [AVQuery queryWithClassName:parameters[@"objectClass"]];
    [query whereKey:parameters[@"objectKey"] equalTo:parameters[@"objectValue"]];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    
    query.limit = 20;
    if(parameters){
        if (parameters[@"skip"]) {
            query.skip = [parameters[@"skip"] integerValue];
        }
    }
    [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
        if (!error) {
            NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] init];
            NSMutableArray *resultArray = [[NSMutableArray alloc] init];
            for (AVObject *object in results) {
                ViewUser *viewUser = [[ViewUser alloc] init];
                //查询对应的用户
                AVQuery *query = [AVUser query];
                viewUser.user = (AVUser *)[query getObjectWithId:object[@"userId"]];
                if (viewUser.user!=nil &&![viewUser.user.objectId isEqualToString:[AVUser currentUser].objectId]) {
                    //查询用户之间的所属关系
                    AVQuery *query1 = [AVUser currentUser].followeeQuery;
                    [query1 includeKey:@"followee"];
                    [query1 whereKey:@"followee" equalTo:viewUser.user];
                    if ([query1 getFirstObject]) {
                        viewUser.relation = 1;
                        AVQuery *query2 = [AVUser currentUser].followerQuery;
                        [query2 includeKey:@"follower"];
                        [query2 whereKey:@"follower" equalTo:viewUser.user];
                        if ([query2 getFirstObject]) {
                            viewUser.relation = 2;
                        }
                    }
                    else{
                        viewUser.relation = 0;
                    }
                    [resultArray addObject:viewUser];
                }
                else{
                    [resultDic setObject:@"YES" forKey:@"isLiked"];
                }
            }
            [resultDic setObject:resultArray forKey:@"userList"];
            success(resultDic);
        } else {
            failure(error);
        }
    }];
}

- (void)getTagCommentList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVQuery queryWithClassName:@"imgTagComment"];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    if(parameters[@"tagId"]!=nil){
        [query whereKey:@"tagId" equalTo:parameters[@"tagId"]];
        
        //生成展示Object
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            NSMutableArray *resultArray = [[NSMutableArray alloc] init];
            for (AVObject *object in objects) {
                AVQuery *query = [AVUser query];
                AVUser *user = (AVUser *)[query getObjectWithId:object[@"userId"]];
                
                Comment *comment = [[Comment alloc] init];
                comment.user = user;
                comment.objectId = object.objectId;
                comment.content = object[@"content"];
                comment.updateTime = object.createdAt;
                [resultArray addObject:comment];
            }
            
            if (!error) {
                success(resultArray);
            } else {
                failure(error);
            }
        }];
    }
}

- (void)addImgSharedComment:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVObject *object = [AVObject objectWithClassName:@"imgComment"];
    [object setObject:parameters[@"sharedId"] forKey:@"sharedId"];
    [object setObject:parameters[@"content"] forKey:@"content"];
    [object setObject:parameters[@"userId"] forKey:@"userId"];
    [object setObject:[NSNumber numberWithInt:0] forKey:@"deleteFlag"];
    [object setObject:@"-1" forKey:@"parentId"];
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            AVQuery *query = [AVQuery queryWithClassName:@"imgShared"];
            AVObject *imgShared = [query getObjectWithId:parameters[@"sharedId"]];
            [imgShared addUniqueObject:object.objectId forKey:@"commentArray"];
            [imgShared incrementKey:@"commentCount"];
            [imgShared saveInBackground];
            
            success(object);
        } else {
            failure(error);
        }
    }];
}

- (void)removeImgSharedComment:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"imgComment"];
    [query whereKey:@"objectId" equalTo:parameters[@"objectId"]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            AVObject *object = objects[0];
            [object setObject:[NSNumber numberWithInt:1] forKey:@"deleteFlag"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    AVQuery *query = [AVQuery queryWithClassName:@"imgShared"];
                    AVObject *imgShared = [query getObjectWithId:parameters[@"sharedId"]];
                    [imgShared setObject:[NSNumber numberWithInt:[imgShared[@"commentCount"] intValue]-1] forKey:@"commentCount"];
                    [imgShared saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (succeeded) {
                            success(object);
                        } else {
                            failure(error);
                        }
                    }];
                } else {
                    failure(error);
                }
            }];
        } else {
            failure(error);
        }
    }];
}

- (void)getImgSharedCommentCount:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    AVQuery *query = [AVQuery queryWithClassName:@"imgComment"];
    [query whereKey:@"sharedId" equalTo:parameters[@"objectId"]];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    [query countObjectsInBackgroundWithBlock:^(NSInteger count, NSError *error) {
        if (!error) {
            success([NSNumber numberWithInteger:count]);
        } else {
            failure(error);
        }
    }];
}

- (void)addImgSharedLike:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVObject *object = [AVObject objectWithClassName:@"imgPraise"];
    [object setObject:parameters[@"sharedId"] forKey:@"sharedId"];
    [object setObject:parameters[@"userId"] forKey:@"userId"];
    [object setObject:[NSNumber numberWithInt:0] forKey:@"deleteFlag"];
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            AVQuery *query = [AVQuery queryWithClassName:@"imgShared"];
            AVObject *imgShared = [query getObjectWithId:parameters[@"sharedId"]];
            [imgShared incrementKey:@"praiseCount"];
            [imgShared saveInBackground];
            success(object);
        } else {
            failure(error);
        }
    }];
}

- (void)removeImgSharedLike:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"imgPraise"];
    [query whereKey:@"sharedId" equalTo:parameters[@"sharedId"]];
    [query whereKey:@"userId" equalTo:parameters[@"userId"]];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            AVObject *object = objects[0];
            [object setObject:[NSNumber numberWithInt:1] forKey:@"deleteFlag"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    AVQuery *query = [AVQuery queryWithClassName:@"imgShared"];
                    AVObject *imgShared = [query getObjectWithId:parameters[@"sharedId"]];
                    [imgShared setObject:[NSNumber numberWithInt:[imgShared[@"praiseCount"] intValue]-1] forKey:@"praiseCount"];
                    [imgShared saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        if (succeeded) {
                            success(object);
                        } else {
                            failure(error);
                        }
                    }];
                } else {
                    failure(error);
                }
            }];
        } else {
            failure(error);
        }
    }];
}

- (void)addImgTagComment:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVObject *object = [AVObject objectWithClassName:@"imgTagComment"];
    [object setObject:parameters[@"tagId"] forKey:@"tagId"];
    [object setObject:parameters[@"content"] forKey:@"content"];
    [object setObject:parameters[@"userId"] forKey:@"userId"];
    [object setObject:[NSNumber numberWithInt:0] forKey:@"deleteFlag"];
    [object setObject:@"-1" forKey:@"parentId"];
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            AVQuery *query = [AVQuery queryWithClassName:@"imgShared"];
            AVObject *imgShared = [query getObjectWithId:parameters[@"sharedId"]];
            [imgShared addUniqueObject:object.objectId forKey:@"commentArray"];
            [imgShared incrementKey:@"commentCount"];
            [imgShared saveInBackground];
            
            success(object);
        } else {
            failure(error);
        }
    }];
}

- (void)removeImgTagComment:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"imgTagComment"];
    [query whereKey:@"objectId" equalTo:parameters[@"objectId"]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            AVObject *object = objects[0];
            [object setObject:[NSNumber numberWithInt:1] forKey:@"deleteFlag"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    success(object);
                } else {
                    failure(error);
                }
            }];
        } else {
            failure(error);
        }
    }];
}

- (void)removeImgShared:(NSString *)objectId success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:@"imgShared"];
    [query whereKey:@"objectId" equalTo:objectId];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            AVObject *object = objects[0];
            [object setObject:[NSNumber numberWithInt:1] forKey:@"deleteFlag"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    success(object);
                } else {
                    failure(error);
                }
            }];
        } else {
            failure(error);
        }
    }];
}

- (void)getPicGroupListForCloud:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    [AVCloud setProductionMode:NO];
    [AVCloud callFunctionInBackground:@"getPicGroup" withParameters:parameters block:^(id object, NSError *error) {
        // 执行结果
        if (!error) {
            success(object);
        } else {
            failure(error);
        }
    }];
}

@end
