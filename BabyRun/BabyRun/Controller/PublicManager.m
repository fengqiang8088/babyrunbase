//
//  PublicManager.m
//  BabyRun
//
//  Created by fengqiang on 15-1-2.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "PublicManager.h"

#import "PicIndexViewController.h"
#import "CommunityViewController.h"
#import "PostViewController.h"
#import "EducationViewController.h"
#import "UserViewController.h"
#import "RDVTabBarItem.h"
#import "ViewUser.h"
#import "UIViewController+MaryPopin.h"
#import "IndexViewController.h"
#import "PostTextViewController.h"
#import "AddTagViewController.h"

#import <ShareSDK/ShareSDK.h>
#import <AVFoundation/AVFoundation.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WeiboSDK.h"
#import "WXApi.h"

@interface PublicManager () <RDVTabBarControllerDelegate>

@end

@implementation PublicManager

SINGLETON_IMPLEMENTATION(PublicManager)

- (void)getRecommendUserList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVUser query];
    [query whereKey:@"level" equalTo:@"0"];
    //设置缓存策略
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    //按生成时间排序
    [query orderByDescending:@"createdAt"];
    //每页取得的记录数,并做分页处理
    query.limit = 20;
    if(parameters){
        if (parameters[@"skip"]) {
            query.skip = [parameters[@"skip"] integerValue];
        }
    }
    //生成展示Object
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSMutableArray *ResultArray = [[NSMutableArray alloc] init];
            for (AVUser *user in objects) {
                ViewUser *userObject = [ViewUser alloc];
                userObject.user = user;
                [ResultArray addObject:userObject];
            }
            success(ResultArray);
        } else {
            failure(error);
        }
    }];
}

- (void)getRecommendUserInfo:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    //取得主要类型
    AVQuery *query = [AVUser query];
    [query whereKey:@"level" equalTo:@"0"];
    [query countObjectsInBackgroundWithBlock:^(NSInteger count, NSError *error) {
        if (!error) {
            NSString *str = [NSString stringWithFormat:@"我们为您推荐了%ld位热门用户",(long)count];
            success(str);
        } else {
            failure(error);
        }
    }];
}

//设置初始化五个ViewController
- (UIViewController *)setupViewControllers {
    //加载图片首页
    UIViewController *picIndexViewController = [PicIndexViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"PicIndexStoryboard"];
    UIViewController *picIndexNavigationController = [[UINavigationController alloc]
                                                      initWithRootViewController:picIndexViewController];
    //加载社区首页
    UIViewController *communityViewController = [CommunityViewController brCreateFor:@"CommunityStoryboard" andClassIdentifier:@"CommunityStoryboard"];
    UIViewController *communityNavigationController = [[UINavigationController alloc]
                                                       initWithRootViewController:communityViewController];
    //加载发布首页
    UIViewController *viewController = [[UIViewController alloc] init];
    UIViewController *postNavigationController = [[UINavigationController alloc]
                                                  initWithRootViewController:viewController];
    //加载奶瓶志首页
    UIViewController *educationViewController = [EducationViewController brCreateFor:@"EducationStoryboard" andClassIdentifier:@"EducationStoryboard"];
    UIViewController *educationNavigationController = [[UINavigationController alloc]
                                                       initWithRootViewController:educationViewController];
    //加载个人中心首页
    UIViewController *userViewController = [UserViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserStoryboard"];
    UIViewController *userNavigationController = [[UINavigationController alloc]
                                                  initWithRootViewController:userViewController];
    //设置基础TabBar
    RDVTabBarController *tabBarController = [[RDVTabBarController alloc] init];
    [tabBarController setViewControllers:@[picIndexNavigationController, communityNavigationController,
                                           postNavigationController, educationNavigationController,
                                           userNavigationController]];
    tabBarController.delegate = self;
    //设置底部按钮
    [self customizeTabBarForController:tabBarController];
    
    return tabBarController;
}

//设置app底部按钮
- (void)customizeTabBarForController:(RDVTabBarController *)tabBarController {
    int index = 0;
    //设置选中状态Dict
    NSDictionary* selectDic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:10.0f],NSFontAttributeName
                               ,[UIColor colorWithRed:149.0f/255.0f green:132.0f/255.0f blue:152.0f/255.0f alpha:1],NSForegroundColorAttributeName
                               ,nil];
    //设置未选中状态Dict
    NSDictionary* unSelectDic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:10.0f],NSFontAttributeName
                                 ,[UIColor colorWithRed:109.0f/255.0f green:110.0f/255.0f blue:113.0f/255.0f alpha:1],NSForegroundColorAttributeName
                                 ,nil];
    //设置按钮图片
    for (RDVTabBarItem *item in [[tabBarController tabBar] items]) {
        UIImage *selectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%d_select",index]];
        UIImage *unselectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"%d_unselect",index]];
        [item setFinishedSelectedImage:selectedimage withFinishedUnselectedImage:unselectedimage];
        [item setTitle:[[PublicManager shared] getTypeForTabbar:index]];
        item.selectedTitleAttributes = selectDic;
        item.unselectedTitleAttributes = unSelectDic;
        index++;
    }
}

//取得按钮类型名称
- (NSString *)getTypeForTabbar:(int)index{
    switch (index) {
        case 0:
            return @"图片";
            break;
        case 1:
            return @"社区";
            break;
        case 2:
            return @"发布";
            break;
        case 3:
            return @"育儿";
            break;
        case 4:
            return @"我的";
            break;
        default:
            return @"";
            break;
    }
}

//判断点击底部按钮
-(void)tabBarController:(RDVTabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    if (viewController!=nil) {
        if(tabBarController.selectedIndex==2){
            PostViewController *postViewController = [PostViewController brCreateFor:@"PostStoryboard" andClassIdentifier:@"PostStoryboard"];
            postViewController.finishBlock = ^(PostTextViewController *viewController1) {
                [viewController presentViewController:viewController1 animated:YES completion:nil];
            };
            [postViewController setPopinTransitionStyle:BKTPopinTransitionStyleSpringyZoom];
            [postViewController setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
            
            [viewController presentPopinController:postViewController animated:YES completion:nil];
            
        }
        else if (tabBarController.selectedIndex == 4){
            ((UserViewController *)viewController.childViewControllers[0]).showTypeUser = 0;
            ((UserViewController *)viewController.childViewControllers[0]).user = [AVUser currentUser];
        }
    }
    else{
        //进入未登录主页面
        IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
        indexViewController.fromType = 1;
        UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                       initWithRootViewController:indexViewController];
        [self.rootViewController presentViewController:indexNavigationController animated:YES completion:nil];
    }
}

- (NSString *) returnUploadTime:(NSDate *)dic{
    //Tue May 21 10:56:45 +0800 2013
    //NSString *timeStr = [dic objectForKey:@"created_at"];
    
    NSDateFormatter *date=[[NSDateFormatter alloc] init];
    [date setDateFormat:@"E MMM d HH:mm:SS Z y"];
    //NSDate *d =[date dateFromString:timeStr];
    NSDate *d = dic;
    
    NSTimeInterval late=[d timeIntervalSince1970]*1;
    
    
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval now=[dat timeIntervalSince1970]*1;
    NSString *timeString=@"";
    
    NSTimeInterval cha=now-late;
    
    if (cha/3600<1) {
        timeString = [NSString stringWithFormat:@"%f", cha/60];
        timeString = [timeString substringToIndex:timeString.length-7];
        timeString=[NSString stringWithFormat:@"%@分钟前", timeString];
        
    }
    if (cha/3600>1&&cha/86400<1) {
        //        timeString = [NSString stringWithFormat:@"%f", cha/3600];
        //        timeString = [timeString substringToIndex:timeString.length-7];
        //        timeString=[NSString stringWithFormat:@"%@小时前", timeString];
        NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"HH:mm"];
        timeString = [NSString stringWithFormat:@"今天 %@",[dateformatter stringFromDate:d]];
        
    }
    if (cha/86400>1)
    {
        //        timeString = [NSString stringWithFormat:@"%f", cha/86400];
        //        timeString = [timeString substringToIndex:timeString.length-7];
        //        timeString=[NSString stringWithFormat:@"%@天前", timeString];
        NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"YY-MM-dd HH:mm"];
        timeString = [NSString stringWithFormat:@"%@",[dateformatter stringFromDate:d]];
        
    }
    
    return timeString;
}

- (NSString *)getTimeDiffString:(NSTimeInterval)timestamp{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *todate = [NSDate dateWithTimeIntervalSince1970:timestamp];
    NSDate *today = [NSDate date];//当前时间
    unsigned int unitFlag = NSDayCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit;
    NSDateComponents *gap = [cal components:unitFlag fromDate:today toDate:todate options:0];//计算时间差
    
    if (ABS([gap day]) > 0)
    {
        return [NSString stringWithFormat:@"%d天前",(int)ABS([gap day])];
    }else if(ABS([gap hour]) > 0)
    {
        return [NSString stringWithFormat:@"%d小时前", (int)ABS([gap hour])];
    }else
    {
        return [NSString stringWithFormat:@"%d分钟前",  (int)ABS([gap minute])];
    }
}

-(void)shareData:(id)data type:(int)shareType{
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:data[@"content"]
                                       defaultContent:data[@"defaultContent"]
                                                image:[ShareSDK imageWithUrl:data[@"imgUrl"]]
                                                title:data[@"title"]
                                                  url:data[@"url"]
                                          description:data[@"description"]
                                            mediaType:SSPublishContentMediaTypeNews];
    
    [ShareSDK clientShareContent:publishContent
                            type:shareType
                   statusBarTips:YES
                          result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                              
                              if (state == SSPublishContentStateSuccess)
                              {
                                  [TSMessage showNotificationWithTitle:@"分享成功" type:TSMessageNotificationTypeSuccess];
                              }
                              else if (state == SSPublishContentStateFail)
                              {
                                  if(type == ShareTypeQQ){
                                      //判断是否安装了qq
                                      if(![TencentOAuth iphoneQQInstalled]){
                                          [TSMessage showNotificationWithTitle:@"请您安装qq后进行分享。" type:TSMessageNotificationTypeWarning];
                                      }
                                  }
                                  /*
                                  if(type == ShareTypeSinaWeibo){
                                      //判断是否安装了微博
                                      if(![WXApi isWXAppInstalled]){
                                          [TSMessage showNotificationWithTitle:@"请您安装新浪微博后进行分享。" type:TSMessageNotificationTypeWarning];
                                      }
                                  }
                                  */
                                  if(type == ShareTypeWeixiSession||type == ShareTypeWeixiTimeline){
                                      //判断是否安装了微信
                                      if(![WeiboSDK isWeiboAppInstalled]){
                                          [TSMessage showNotificationWithTitle:@"请您安装微信后进行分享。" type:TSMessageNotificationTypeWarning];
                                      }
                                  }
                              }
                          }];
}

- (void)getLikeAllList:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    
    AVQuery *query = [AVQuery queryWithClassName:parameters[@"objectClass"]];
    [query whereKey:parameters[@"objectKey"] equalTo:parameters[@"objectValue"]];
    [query whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
    //取得赞的总数
    [query countObjectsInBackgroundWithBlock:^(NSInteger number, NSError *error) {
        if (!error) {
            NSMutableDictionary *resultDic = [[NSMutableDictionary alloc] init];
            [resultDic setObject:@"NO" forKey:@"isLiked"];
            [resultDic setObject:[NSNumber numberWithInteger:number] forKey:@"likeCount"];
            if (number > 0) {
                if ([AVUser currentUser]!=nil) {
                    [query whereKey:@"userId" equalTo:[AVUser currentUser].objectId];
                    [query getFirstObjectInBackgroundWithBlock:^(AVObject *object, NSError *error) {
                        if (!error&&object) {
                            [resultDic setObject:@"YES" forKey:@"isLiked"];
                        }
                        AVQuery *queryt = [AVQuery queryWithClassName:parameters[@"objectClass"]];
                        [queryt whereKey:parameters[@"objectKey"] equalTo:parameters[@"objectValue"]];
                        [queryt whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
                        queryt.limit = 20;
                        if(parameters){
                            if (parameters[@"skip"]) {
                                queryt.skip = [parameters[@"skip"] integerValue];
                            }
                        }
                        [queryt findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
                            if (!error) {
                                
                                NSMutableArray *resultArray = [[NSMutableArray alloc] init];
                                for (AVObject *object in results) {
                                    ViewUser *viewUser = [[ViewUser alloc] init];
                                    //查询对应的用户
                                    AVQuery *query = [AVUser query];
                                    viewUser.user = (AVUser *)[query getObjectWithId:object[@"userId"]];
                                    if (viewUser.user!=nil &&![viewUser.user.objectId isEqualToString:[AVUser currentUser].objectId]) {
                                        //查询用户之间的所属关系
                                        AVQuery *query1 = [AVUser currentUser].followeeQuery;
                                        [query1 includeKey:@"followee"];
                                        [query1 whereKey:@"followee" equalTo:viewUser.user];
                                        if ([query1 getFirstObject]) {
                                            viewUser.relation = 1;
                                            AVQuery *query2 = [AVUser currentUser].followerQuery;
                                            [query2 includeKey:@"follower"];
                                            [query2 whereKey:@"follower" equalTo:viewUser.user];
                                            if ([query2 getFirstObject]) {
                                                viewUser.relation = 2;
                                            }
                                        }
                                        else{
                                            viewUser.relation = 0;
                                        }
                                        [resultArray addObject:viewUser];
                                    }
                                    else{
                                        
                                    }
                                }
                                [resultDic setObject:resultArray forKey:@"userList"];
                                success(resultDic);
                            } else {
                                failure(error);
                            }
                        }];
                        
                    }];
                }
            }
            else{
                success(resultDic);
            }
        }
    }];
}

-(void)addReport:(NSDictionary *)parameters success:(IMSuccessBlock)success failure:(IMFailureBlock)failure{
    AVObject *object = [AVObject objectWithClassName:@"report"];
    [object setObject:parameters[@"reportType"] forKey:@"reportType"];
    [object setObject:[AVUser currentUser].objectId forKey:@"userId"];
    switch ([parameters[@"reportType"] intValue]) {
        case 0:
        {
            [object setObject:parameters[@"sharedId"] forKey:@"sharedId"];
        }
            break;
        case 1:
        {
            [object setObject:parameters[@"sharedId"] forKey:@"sharedId"];
        }
            break;
        case 2:
        {
            [object setObject:parameters[@"sharedId"] forKey:@"sharedId"];
            [object setObject:parameters[@"commentId"] forKey:@"commentId"];
        }
            break;
        case 3:
        {
            [object setObject:parameters[@"sharedId"] forKey:@"sharedId"];
            [object setObject:parameters[@"commentId"] forKey:@"commentId"];
        }
            break;
        case 4:
        {
            [object setObject:parameters[@"tagId"] forKey:@"tagId"];
            [object setObject:parameters[@"commentId"] forKey:@"commentId"];
        }
            break;
        default:
            break;
    }
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            success(object);
        } else {
            failure(error);
        }
    }];
}

-(BOOL)isOpenCarma{
    BOOL result = NO;
    NSString *mediaType = AVMediaTypeVideo;// Or AVMediaTypeAudio
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    // This status is normally not visible—the AVCaptureDevice class methods for discovering devices do not return devices the user is restricted from accessing.
    if(authStatus ==AVAuthorizationStatusRestricted){
        NSLog(@"Restricted");
    }
    // The user has explicitly denied permission for media capture.
    else if(authStatus == AVAuthorizationStatusDenied){
        NSLog(@"Denied");     //应该是这个，如果不允许的话
    }
    // The user has explicitly granted permission for media capture, or explicit user permission is not necessary for the media type in question.
    else if(authStatus == AVAuthorizationStatusAuthorized){
        result = YES;
    }
    // Explicit user permission is required for media capture, but the user has not yet granted or denied such permission.
    else if(authStatus == AVAuthorizationStatusNotDetermined){
        
    }
    else {
        NSLog(@"Unknown authorization status");
    }
    return result;
}

@end
