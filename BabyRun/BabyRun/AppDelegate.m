//
//  AppDelegate.m
//  BabyRun
//
//  Created by fengqiang on 14-11-18.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "AppDelegate.h"

#import "PublicManager.h"
#import "IndexViewController.h"

#import <ShareSDK/ShareSDK.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <AVOSCloudSNS/AVOSCloudSNS.h>
#import "WXApi.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    //注册AVOS服务--babyrun数据
    //[AVOSCloud setApplicationId:@"e12j1xzbrcup5a12j47yno6qkdxit4q8g4t0lrxyenc06dm3"
    //                  clientKey:@"al68niski3qq3h7idvnjyp6v1g7ly6a4whbm5bv1hf5ojx6a"];
    
    //注册AVOS服务--BabyRun数据
    [AVOSCloud setApplicationId:@"d1u04oaa116w0tojv2jjow0jfuoholny78wxukkldeivm4a4"
                      clientKey:@"wlzifedqudsn4ph2mwykil7jjuqw2tk4ws6ylxgeeoktg23a"];

    //统计打开情况
    [AVAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    //设置初始结构界面
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    //设置shareSDK
    [self initShareSDK];
    
    //设置初始模块ViewController
    [self checkFirstOpen];
    
    [self.window setRootViewController:self.viewController];
    [self.window makeKeyAndVisible];
    
    return YES;
}

//判断是否是首次登陆
- (void)checkFirstOpen{
    //判断用户是否登陆
    if ([AVUser currentUser] != nil) {
        //进入内容主页面
        self.viewController = [[PublicManager shared] setupViewControllers];
        [PublicManager shared].rootViewController = self.viewController;
    } else {
        //进入未登录主页面
        IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
        indexViewController.fromType = 0;
        UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                       initWithRootViewController:indexViewController];
        self.viewController = indexNavigationController;
    }
}

-(void)initShareSDK{
    [ShareSDK registerApp:@"3217548e7888"];
    
    //添加新浪微博应用 注册网址 http://open.weibo.com
    [ShareSDK connectSinaWeiboWithAppKey:sinaAppKey
                               appSecret:sinaAppSecret
                             redirectUri:sinaAppRedirectUri];
    
    //添加微信的应用
    [ShareSDK connectWeChatWithAppId:wxAppKey   //微信APPID
                           appSecret:wxAppSecret  //微信APPSecret
                           wechatCls:[WXApi class]];
    
    //添加QQ应用  注册网址  http://open.qq.com/
    [ShareSDK connectQQWithQZoneAppKey:qqAppID
                     qqApiInterfaceCls:[QQApiInterface class]
                       tencentOAuthCls:[TencentOAuth class]];
    
    //添加QQ空间应用
    [ShareSDK connectQZoneWithAppKey:qqAppID
                           appSecret:qqAppKEY
                   qqApiInterfaceCls:[QQApiInterface class]
                     tencentOAuthCls:[TencentOAuth class]];
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    return [ShareSDK handleOpenURL:url wxDelegate:nil];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    return [ShareSDK handleOpenURL:url
                 sourceApplication:sourceApplication
                        annotation:annotation
                        wxDelegate:nil];
}

@end
