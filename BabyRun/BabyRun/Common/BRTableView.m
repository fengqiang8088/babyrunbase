//
//  BRTableView.m
//  BabyRun
//
//  Created by fengqiang on 14-11-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "BRTableView.h"

@implementation BRTableView

- (id)init{
    self = [super init];
    if (self) {
        [self setup];
    }
    return  self;
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return  self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup{
    [self addHeaderWithTarget:self action:@selector(headerRereshing)];
    [self addFooterWithTarget:self action:@selector(footerRereshing)];
    
    self.headerPullToRefreshText = @"下拉进行刷新";
    self.headerReleaseToRefreshText = @"松开开始刷新";
    self.headerRefreshingText = @"正在刷新...请稍等";
    
    self.footerPullToRefreshText = @"上拉可以加载更多数据";
    self.footerReleaseToRefreshText = @"松开加载更多数据";
    self.footerRefreshingText = @"正在加载中...请稍等";
}

- (void)headerRereshing{
    [self.BRTableViewDelegate refreshTable:self];
}

- (void)footerRereshing{
    [self.BRTableViewDelegate loadMoreDataTable:self];
}

@end
