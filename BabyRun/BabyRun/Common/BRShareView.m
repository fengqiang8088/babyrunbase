//
//  BRShareView.m
//  BabyRun
//
//  Created by fengqiang on 15-1-11.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "BRShareView.h"
#import "PublicManager.h"
#import <ShareSDK/ShareSDK.h>
#import "ImgShared.h"
#import "TextShared.h"
#import "Activity.h"
#import "FeederDetail.h"
#import "PicIndexManager.h"
#import "CommunityIndexManager.h"
#import "PublicManager.h"

@implementation BRShareView

- (instancetype)init{
    self = [super init];
    if (self) {
        
        //初始化背景视图，添加手势
        self.frame = CGRectMake(0, 0, SCREEN_WIDTH,SCREEN_HEIGHT);
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCancel)];
        [self addGestureRecognizer:tapGesture];
        
        //生成主要ActionSheetView
        self.shareView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 276)];
        [self.shareView setBackgroundColor:[UIColor whiteColor]];
        
        //生成顶部线条
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 3)];
        [lineView setBackgroundColor:BRPurpleColor];
        [self.shareView addSubview:lineView];
        
        //生成标题
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(112, 18, 96, 21)];
        [titleLabel setText:@"分享到"];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setFont:[UIFont fontWithName:@"HiraKakuProN-W3" size:17]];
        [titleLabel setTextColor:BRPurpleColor];
        [self.shareView addSubview:titleLabel];
        
        //生成包含分享按钮的view
        UIView *actionView = [[UIView alloc] initWithFrame:CGRectMake(20, 56, 280, 70)];
        [self.shareView addSubview:actionView];
        
        UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 53, 53)];
        [btn1 setImage:[UIImage imageNamed:@"wx"] forState:UIControlStateNormal];
        [btn1 addTarget:self action:@selector(shareToWX) forControlEvents:UIControlEventTouchUpInside];
        [actionView addSubview:btn1];
        
        UILabel *fxLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(16, 55, 42, 15)];
        [fxLabel1 setTextColor:BRPurpleColor];
        [fxLabel1 setText:@"微信好友"];
        [fxLabel1 setTextAlignment:NSTextAlignmentCenter];
        [fxLabel1 setFont:[UIFont fontWithName:@"HiraKakuProN-W3" size:10]];
        [actionView addSubview:fxLabel1];
        
        UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(78, 0, 53, 53)];
        [btn2 setImage:[UIImage imageNamed:@"pyq"] forState:UIControlStateNormal];
        [btn2 addTarget:self action:@selector(shareToPYQ) forControlEvents:UIControlEventTouchUpInside];
        [actionView addSubview:btn2];
        
        UILabel *fxLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(83, 55, 42, 15)];
        [fxLabel2 setTextColor:BRPurpleColor];
        [fxLabel2 setText:@"朋友圈"];
        [fxLabel2 setTextAlignment:NSTextAlignmentCenter];
        [fxLabel2 setFont:[UIFont fontWithName:@"HiraKakuProN-W3" size:10]];
        [actionView addSubview:fxLabel2];
        
        UIButton *btn3 = [[UIButton alloc] initWithFrame:CGRectMake(148, 0, 53, 53)];
        [btn3 setImage:[UIImage imageNamed:@"sina"] forState:UIControlStateNormal];
        [btn3 addTarget:self action:@selector(shareToSINA) forControlEvents:UIControlEventTouchUpInside];
        [actionView addSubview:btn3];
        
        UILabel *fxLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(154, 55, 42, 15)];
        [fxLabel3 setTextColor:BRPurpleColor];
        [fxLabel3 setText:@"新浪微博"];
        [fxLabel3 setTextAlignment:NSTextAlignmentCenter];
        [fxLabel3 setFont:[UIFont fontWithName:@"HiraKakuProN-W3" size:10]];
        [actionView addSubview:fxLabel3];
        
        UIButton *btn4 = [[UIButton alloc] initWithFrame:CGRectMake(216, 0, 53, 53)];
        [btn4 setImage:[UIImage imageNamed:@"qq_icon"] forState:UIControlStateNormal];
        [btn4 addTarget:self action:@selector(shareToQQ) forControlEvents:UIControlEventTouchUpInside];
        [actionView addSubview:btn4];
        
        UILabel *fxLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(220, 55, 42, 15)];
        [fxLabel4 setTextColor:BRPurpleColor];
        [fxLabel4 setText:@"QQ"];
        [fxLabel4 setTextAlignment:NSTextAlignmentCenter];
        [fxLabel4 setFont:[UIFont fontWithName:@"HiraKakuProN-W3" size:10]];
        [actionView addSubview:fxLabel4];
        
        //生成举报或删除按钮
        self.jbBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, 143, 280, 45)];
        [self.jbBtn setTitle:@"举报不良内容" forState:UIControlStateNormal];
        [self.jbBtn setBackgroundImage:[UIImage imageNamed:@"actionsheet3"] forState:UIControlStateNormal];
        [self.jbBtn addTarget:self action:@selector(juOrDel) forControlEvents:UIControlEventTouchUpInside];
        [self.shareView addSubview:self.jbBtn];
        
        //生成取消按钮
        UIButton *cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, 197, 280, 45)];
        [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        [cancelBtn setTitleColor:BRPurpleColor forState:UIControlStateNormal];
        [cancelBtn setBackgroundImage:[UIImage imageNamed:@"actionsheet1"] forState:UIControlStateNormal];
        [cancelBtn addTarget:self action:@selector(tappedCancel) forControlEvents:UIControlEventTouchUpInside];
        [self.shareView addSubview:cancelBtn];
        
        //添加主View
        [self addSubview:self.shareView];
    }
    return self;
}

-(void)showInView{
    [self updateBtn];
    [[UIApplication sharedApplication].delegate.window.rootViewController.view addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        [self.shareView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-self.shareView.frame.size.height/2)];
    } completion:^(BOOL finished) {
    }];
    
}

- (void)tappedCancel{
    [UIView animateWithDuration:0.3 animations:^{
        [self.shareView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT+self.shareView.frame.size.height/2)];
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)shareToWX{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    if ([self.data isKindOfClass:[ImgShared class]]) {
        ImgShared *imgShared = self.data;
        [dic setObject:imgShared.imgText forKey:@"content"];
        [dic setObject:@"defaultContent" forKey:@"defaultContent"];
        [dic setObject:imgShared.imgUrl forKey:@"imgUrl"];
        [dic setObject:@"title" forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/shared.html?imgSharedId=%@",imgShared.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"description" forKey:@"description"];
    }
    else if ([self.data isKindOfClass:[TextShared class]]) {
        TextShared *textShared = self.data;
        [dic setObject:textShared.sharedTitle forKey:@"content"];
        [dic setObject:@"defaultContent" forKey:@"defaultContent"];
        [dic setObject:@"" forKey:@"imgUrl"];
        [dic setObject:@"title" forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/textShared.html?textSharedId=%@",textShared.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"description" forKey:@"description"];
    }
    else if ([self.data isKindOfClass:[Activity class]]) {
        Activity *activity = self.data;
        [dic setObject:activity.title forKey:@"content"];
        [dic setObject:activity.content forKey:@"defaultContent"];
        [dic setObject:activity.image.url forKey:@"imgUrl"];
        [dic setObject:@"参加了活动" forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/task.html?taskId=%@",activity.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"活动详情" forKey:@"description"];
    }
    else if ([self.data isKindOfClass:[FeederDetail class]]) {
        FeederDetail *feed = self.data;
        [dic setObject:feed.content forKey:@"content"];
        [dic setObject:@"defalutContent" forKey:@"defaultContent"];
        [dic setObject:feed.image.url forKey:@"imgUrl"];
        [dic setObject:feed.title forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/bottlelog.html?knowledgeId=%@",feed.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"description" forKey:@"description"];
    }
    [[PublicManager shared] shareData:dic type:ShareTypeWeixiSession];
}

- (void)shareToPYQ{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    if ([self.data isKindOfClass:[ImgShared class]]) {
        ImgShared *imgShared = self.data;
        [dic setObject:imgShared.imgText forKey:@"content"];
        [dic setObject:@"defaultContent" forKey:@"defaultContent"];
        [dic setObject:imgShared.imgUrl forKey:@"imgUrl"];
        [dic setObject:@"title" forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/shared.html?imgSharedId=%@",imgShared.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"description" forKey:@"description"];
    }
    else if ([self.data isKindOfClass:[TextShared class]]) {
        TextShared *textShared = self.data;
        [dic setObject:textShared.sharedTitle forKey:@"content"];
        [dic setObject:@"defaultContent" forKey:@"defaultContent"];
        [dic setObject:@"" forKey:@"imgUrl"];
        [dic setObject:@"title" forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/textShared.html?textSharedId=%@",textShared.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"description" forKey:@"description"];
    }
    else if ([self.data isKindOfClass:[Activity class]]) {
        Activity *activity = self.data;
        [dic setObject:activity.title forKey:@"content"];
        [dic setObject:activity.content forKey:@"defaultContent"];
        [dic setObject:activity.image.url forKey:@"imgUrl"];
        [dic setObject:@"参加了活动" forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/task.html?taskId=%@",activity.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"活动详情" forKey:@"description"];
    }
    else if ([self.data isKindOfClass:[FeederDetail class]]) {
        FeederDetail *feed = self.data;
        [dic setObject:feed.content forKey:@"content"];
        [dic setObject:@"defalutContent" forKey:@"defaultContent"];
        [dic setObject:feed.image.url forKey:@"imgUrl"];
        [dic setObject:feed.title forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/bottlelog.html?knowledgeId=%@",feed.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"description" forKey:@"description"];
    }
    [[PublicManager shared] shareData:dic type:ShareTypeWeixiTimeline];
}

- (void)shareToSINA{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    if ([self.data isKindOfClass:[ImgShared class]]) {
        ImgShared *imgShared = self.data;
        [dic setObject:imgShared.imgText forKey:@"content"];
        [dic setObject:@"defaultContent" forKey:@"defaultContent"];
        [dic setObject:imgShared.imgUrl forKey:@"imgUrl"];
        [dic setObject:@"title" forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/shared.html?imgSharedId=%@",imgShared.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"description" forKey:@"description"];
    }
    else if ([self.data isKindOfClass:[TextShared class]]) {
        TextShared *textShared = self.data;
        [dic setObject:textShared.sharedTitle forKey:@"content"];
        [dic setObject:@"defaultContent" forKey:@"defaultContent"];
        [dic setObject:@"" forKey:@"imgUrl"];
        [dic setObject:@"title" forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/textShared.html?textSharedId=%@",textShared.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"description" forKey:@"description"];
    }
    else if ([self.data isKindOfClass:[Activity class]]) {
        Activity *activity = self.data;
        [dic setObject:activity.title forKey:@"content"];
        [dic setObject:activity.content forKey:@"defaultContent"];
        [dic setObject:activity.image.url forKey:@"imgUrl"];
        [dic setObject:@"参加了活动" forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/task.html?taskId=%@",activity.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"活动详情" forKey:@"description"];
    }
    else if ([self.data isKindOfClass:[FeederDetail class]]) {
        FeederDetail *feed = self.data;
        [dic setObject:feed.content forKey:@"content"];
        [dic setObject:@"defalutContent" forKey:@"defaultContent"];
        [dic setObject:feed.image.url forKey:@"imgUrl"];
        [dic setObject:feed.title forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/bottlelog.html?knowledgeId=%@",feed.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"description" forKey:@"description"];
    }
    [[PublicManager shared] shareData:dic type:ShareTypeSinaWeibo];
}

- (void)shareToQQ{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    if ([self.data isKindOfClass:[ImgShared class]]) {
        ImgShared *imgShared = self.data;
        [dic setObject:imgShared.imgText forKey:@"content"];
        [dic setObject:@"defaultContent" forKey:@"defaultContent"];
        [dic setObject:imgShared.imgUrl forKey:@"imgUrl"];
        [dic setObject:@"title" forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/shared.html?imgSharedId=%@",imgShared.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"description" forKey:@"description"];
    }
    else if ([self.data isKindOfClass:[TextShared class]]) {
        TextShared *textShared = self.data;
        [dic setObject:textShared.sharedTitle forKey:@"content"];
        [dic setObject:@"defaultContent" forKey:@"defaultContent"];
        [dic setObject:@"" forKey:@"imgUrl"];
        [dic setObject:@"title" forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/textShared.html?textSharedId=%@",textShared.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"description" forKey:@"description"];
    }
    else if ([self.data isKindOfClass:[Activity class]]) {
        Activity *activity = self.data;
        [dic setObject:activity.title forKey:@"content"];
        [dic setObject:activity.content forKey:@"defaultContent"];
        [dic setObject:activity.image.url forKey:@"imgUrl"];
        [dic setObject:@"参加了活动" forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/task.html?taskId=%@",activity.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"活动详情" forKey:@"description"];
    }
    else if ([self.data isKindOfClass:[FeederDetail class]]) {
        FeederDetail *feed = self.data;
        [dic setObject:feed.content forKey:@"content"];
        [dic setObject:@"defalutContent" forKey:@"defaultContent"];
        [dic setObject:feed.image.url forKey:@"imgUrl"];
        [dic setObject:feed.title forKey:@"title"];
        NSString *url = [NSString stringWithFormat:@"http://backend.keegoo.net/shareds/bottlelog.html?knowledgeId=%@",feed.objectId];
        [dic setObject:url forKey:@"url"];
        [dic setObject:@"description" forKey:@"description"];
    }
    [[PublicManager shared] shareData:dic type:ShareTypeQQ];
}

- (void)updateBtn{
    if ([self.data isKindOfClass:[ImgShared class]]) {
        ImgShared *imgShared = self.data;
        if ([imgShared.user.objectId isEqualToString:[AVUser currentUser].objectId]) {
            [self.jbBtn setTitle:@"删除" forState:UIControlStateNormal];
        }
    }
    else if ([self.data isKindOfClass:[TextShared class]]) {
        TextShared *textShared = self.data;
        if ([textShared.user.objectId isEqualToString:[AVUser currentUser].objectId]) {
            [self.jbBtn setTitle:@"删除" forState:UIControlStateNormal];
        }
    }
    else if ([self.data isKindOfClass:[Activity class]]) {
    }
    else if ([self.data isKindOfClass:[FeederDetail class]]) {
    }
}

- (void)juOrDel{
    if ([self.data isKindOfClass:[ImgShared class]]) {
        ImgShared *imgShared = self.data;
        if ([imgShared.user.objectId isEqualToString:[AVUser currentUser].objectId]) {
            [[PicIndexManager shared] removeImgShared:imgShared.objectId success:^(id objects) {
                [TSMessage showNotificationWithTitle:@"删除成功" type:TSMessageNotificationTypeSuccess];
                [self tappedCancel];
            } failure:^(NSError *error) {
                [TSMessage showNotificationWithTitle:@"删除失败" type:TSMessageNotificationTypeError];
            }];
        }
        else{
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[NSNumber numberWithInt:0] forKey:@"reportType"];
            [dic setObject:imgShared.objectId forKey:@"sharedId"];
            [[PublicManager shared] addReport:dic success:^(id objects) {
                [TSMessage showNotificationWithTitle:@"举报成功" type:TSMessageNotificationTypeSuccess];
                [self tappedCancel];
            } failure:^(NSError *error) {
                [TSMessage showNotificationWithTitle:@"举报失败" type:TSMessageNotificationTypeError];
            }];
        }
    }
    else if ([self.data isKindOfClass:[TextShared class]]) {
        TextShared *textShared = self.data;
        if ([textShared.user.objectId isEqualToString:[AVUser currentUser].objectId]) {
            [[CommunityIndexManager shared] removeTextShared:textShared.objectId success:^(id objects) {
                [TSMessage showNotificationWithTitle:@"删除成功" type:TSMessageNotificationTypeSuccess];
                [self tappedCancel];
            } failure:^(NSError *error) {
                [TSMessage showNotificationWithTitle:@"删除失败" type:TSMessageNotificationTypeError];
            }];
        }
        else{
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[NSNumber numberWithInt:1] forKey:@"reportType"];
            [dic setObject:textShared.objectId forKey:@"sharedId"];
            [[PublicManager shared] addReport:dic success:^(id objects) {
                [TSMessage showNotificationWithTitle:@"举报成功" type:TSMessageNotificationTypeSuccess];
                [self tappedCancel];
            } failure:^(NSError *error) {
                [TSMessage showNotificationWithTitle:@"举报失败" type:TSMessageNotificationTypeError];
            }];
        }
    }
    else if ([self.data isKindOfClass:[Activity class]]) {
    }
    else if ([self.data isKindOfClass:[FeederDetail class]]) {
    }
}

@end
