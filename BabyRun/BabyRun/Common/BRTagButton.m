//
//  BRTagButton.m
//  BabyRun
//
//  Created by fengqiang on 14-12-15.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "BRTagButton.h"

@implementation BRTagButton

- (void)setTagText:(NSString *)text andPosition:(int)position andGroupId:(NSInteger)groupId{
    UIImage *image = [[UIImage imageNamed:@"tag_b"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,25,0,25) resizingMode:UIImageResizingModeStretch];
    
    //计算文字长度
    [self.titleLabel setFont:[UIFont fontWithName:@"HiraKakuProN-W3" size:12]];
    CGSize size = [text sizeWithAttributes: @{NSFontAttributeName: [UIFont fontWithName:@"HiraKakuProN-W3" size:12]}];
    if (size.width < 85) {
        size.width = 85;
    }
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width+10, 26)];
    [self setTitle:text forState:UIControlStateNormal];
    [self setBackgroundImage:image forState:UIControlStateNormal];
    
    UIImage *imageO = [UIImage imageNamed:@"tag_0"];
    UIImageView *imageViewO = [[UIImageView alloc] initWithImage:imageO];
    if (position == 0) {
        [imageViewO setCenter:CGPointMake(-10, self.frame.size.height/2)];
    }
    else{
        [imageViewO setCenter:CGPointMake(self.frame.size.width+10, self.frame.size.height/2)];
    }
    
    [self addSubview:imageViewO];
    [self setClipsToBounds:NO];

}

@end
