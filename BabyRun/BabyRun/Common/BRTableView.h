//
//  BRTableView.h
//  BabyRun
//
//  Created by fengqiang on 14-11-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"

@class BRTableView;

@protocol BRTableViewDelegate <NSObject>

@required
- (void)refreshTable:(BRTableView *)tableview;
- (void)loadMoreDataTable:(BRTableView *)tableview;

@end

@interface BRTableView : UITableView

@property (nonatomic, weak) id<BRTableViewDelegate> BRTableViewDelegate;

@end
