//
//  BRShowTagButton.h
//  BabyRun
//
//  Created by fengqiang on 15-1-8.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//
#import "ImgTag.h"
@class BREditTagButton;

@protocol BREditTagButtonDelegate <NSObject>

@required
- (void)longPress:(BREditTagButton *)btn;
@end

@interface BREditTagButton : UIButton

@property (nonatomic,strong) ImgTag *imgTag;
@property (nonatomic,weak) id<BREditTagButtonDelegate> delegate;

- (void)setTagText:(NSString *)text andPosition:(int)position andGroupId:(NSInteger)groupId;



@end
