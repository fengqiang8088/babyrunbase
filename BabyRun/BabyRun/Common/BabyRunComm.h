//
//  BabyRunComm.h
//  BabyRun
//
//  Created by fengqiang on 14-11-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

// Singleton
#define SINGLETON_INTERFACE(CLASSNAME)              \
+ (CLASSNAME*)shared;

#define SINGLETON_IMPLEMENTATION(CLASSNAME)         \
\
static CLASSNAME* g_shared##CLASSNAME = nil;        \
\
+ (CLASSNAME*)shared                                \
{                                                   \
static dispatch_once_t oncePredicate;           \
dispatch_once(&oncePredicate, ^{                \
g_shared##CLASSNAME = [[self alloc] init];  \
});                                             \
return g_shared##CLASSNAME;                     \
}                                                   \
\
+ (id)allocWithZone:(NSZone*)zone                   \
{                                                   \
@synchronized(self) {                               \
if (g_shared##CLASSNAME == nil) {                   \
g_shared##CLASSNAME = [super allocWithZone:zone];   \
return g_shared##CLASSNAME;                         \
}                                                   \
}                                                   \
NSAssert(NO, @ "[" #CLASSNAME                       \
" alloc] explicitly called on singleton class.");   \
return nil;                                         \
}                                                   \
\
- (id)copyWithZone:(NSZone*)zone                    \
{                                                   \
return self;                                        \
}                                                   \

#define SCREEN_HEIGHT CGRectGetHeight([UIScreen mainScreen].bounds)
#define SCREEN_WIDTH CGRectGetWidth([UIScreen mainScreen].bounds)

#define BRPurpleColor [UIColor colorWithRed:149.0f/255.0f green:132.0f/255.0f blue:152.0f/255.0f alpha:1.0f]
#define BRBlackColor [UIColor colorWithRed:109.0f/255.0f green:110.0f/255.0f blue:113.0f/255.0f alpha:1.0f]
#define BRGRAYColor [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1.0f]
#define BRGRAY1Color [UIColor colorWithRed:225.0f/255.0f green:225.0f/255.0f blue:225.0f/255.0f alpha:1.0f]

#define BRBLUEColor [UIColor colorWithRed:107.0f/255.0f green:204.0f/255.0f blue:210.0f/255.0f alpha:1.0f]

#define sinaAppKey @"2923671362"
#define sinaAppSecret @"0e08f56fbb469d21b1530b9253ac503b"
#define sinaAppRedirectUri @"https://api.weibo.com/oauth2/default.html"

#define qqAppID @"1101768122"
#define qqAppKEY @"HXyl1nSXQDTbOBjR"

#define wxAppKey @"wxda7b00952bde06fc"
#define wxAppSecret @"941e1264fd030455362f5e0b15dc081a"

#define babyRunId @"932730914"


#define BRNOTI_UPDATE_GZ @"BRNOTI_UPDATE_GZ"

typedef NS_ENUM(NSInteger, UserLoginFromType) {
    UserLoginFromTypeIndex = 0, // 直接主页进入
    UserLoginFromTypeSecond     // 点击相关操作弹出
};

#define IOS8 [[[UIDevice currentDevice]systemVersion] floatValue] >= 8.0
