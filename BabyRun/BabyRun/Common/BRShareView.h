//
//  BRShareView.h
//  BabyRun
//
//  Created by fengqiang on 15-1-11.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BRShareView : UIView

@property (nonatomic,strong) UIView *shareView;
@property (nonatomic,strong) UIButton *jbBtn;
@property (nonatomic,strong) id data;

-(void)showInView;

@end
