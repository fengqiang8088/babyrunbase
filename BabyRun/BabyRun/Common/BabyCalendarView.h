//
//  BabyCalendarView.h
//  sampleCalendar
//
//  Created by fengqiang on 15-1-16.
//  Copyright (c) 2015年 Attinad. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CalendarDelegate <NSObject>

-(void)tappedOnDate:(NSDate *)selectedDate;

@end

@interface BabyCalendarView : UIView{
    NSArray *_weekNames;
    NSCalendar *gregorian;
    NSInteger _selectedDate;
    NSInteger _selectedMonth;
    NSInteger _selectedYear;
}

@property (nonatomic,strong) NSDate *calendarDate;
@property (nonatomic,weak) id<CalendarDelegate> delegate;

@end
