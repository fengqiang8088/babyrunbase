//
//  UIView+BTPosition.h
//  Venti
//
//  Created by Wangdi on 13-1-16.
//  Copyright (c) 2013年 Beijing Zhixun Innovation Co. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BTPosition)
- (void)adjustFrameByMovingDown:(NSInteger)offsetDown;
- (void)adjustFrameByMovingUp:(NSInteger)offsetUp;
- (void)adjustFrameX:(CGFloat)xOffset;
- (void)adjustFrameY:(CGFloat)yOffset;
- (void)adjustFrameWidth:(CGFloat)widthOffset;
- (void)adjustFrameHeight:(CGFloat)heightOffset;

- (void)setFrameX:(CGFloat)x;
- (void)setFrameY:(CGFloat)y;
- (void)setFrameHeight:(CGFloat)height;
- (void)setFrameWidth:(CGFloat)width;

- (void)setFrameOrigin:(CGPoint)origin;
- (void)setFrameSize:(CGSize)size;

- (void)setFrameCenterVertically;
- (void)setFrameCenterHorizontally;

- (void)setFrameWidth:(CGFloat)width alignment:(UIControlContentHorizontalAlignment)alignment;
- (void)adjustFrameWidth:(CGFloat)widthOffset alignment:(UIControlContentHorizontalAlignment)alignment;

// AutoLayout
- (void)setFrameToFrame:(CGRect)frame;

@end

UIEdgeInsets UIEdgeInsetsUnionEdgeInsets(UIEdgeInsets inset1, UIEdgeInsets inset2);