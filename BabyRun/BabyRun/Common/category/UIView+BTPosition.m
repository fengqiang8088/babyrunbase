//
//  UIView+BTPosition.m
//  Venti
//
//  Created by Wangdi on 13-1-16.
//  Copyright (c) 2013年 Beijing Zhixun Innovation Co. Ltd. All rights reserved.
//

#import "UIView+BTPosition.h"
//#import "UIView+Constraint.h"

@implementation UIView (BTPosition)

- (void)adjustFrameByMovingDown:(NSInteger)offsetDown {
    [self adjustFrameY:offsetDown];
}

- (void)adjustFrameByMovingUp:(NSInteger)offsetUp {
    [self adjustFrameY:-offsetUp];
}

- (void)adjustFrameX:(CGFloat)xOffset {
    CGRect frame = self.frame;
    frame.origin.x += xOffset;
    self.frame = frame;
}

- (void)adjustFrameY:(CGFloat)yOffset {
    CGRect frame = self.frame;
    frame.origin.y += yOffset;
    self.frame = frame;
}

- (void)adjustFrameWidth:(CGFloat)widthOffset {
    CGRect frame = self.frame;
    frame.size.width += widthOffset;
    self.frame = frame;
}

- (void)adjustFrameWidth:(CGFloat)widthOffset alignment:(UIControlContentHorizontalAlignment)alignment {
    CGRect frame = self.frame;
    frame.size.width += widthOffset;
    
    if (alignment == UIControlContentHorizontalAlignmentRight)
        frame.origin.x += -widthOffset;
    else if (alignment == UIControlContentHorizontalAlignmentCenter)
        frame.origin.x += roundf(-widthOffset / 2);
    
    self.frame = frame;
}

- (void)adjustFrameHeight:(CGFloat)heightOffset {
    CGRect frame = self.frame;
    frame.size.height += heightOffset;
    self.frame = frame;
}

- (void)setFrameX:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (void)setFrameY:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (void)setFrameHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (void)setFrameWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (void)setFrameWidth:(CGFloat)width alignment:(UIControlContentHorizontalAlignment)alignment {
    CGRect frame = self.frame;
    CGFloat originWidth = frame.size.width;
    frame.size.width = width;

    if (alignment == UIControlContentHorizontalAlignmentRight)
        frame.origin.x += originWidth - width;
    else if (alignment == UIControlContentHorizontalAlignmentCenter)
        frame.origin.x += roundf((originWidth - width) / 2);
    
    self.frame = frame;
}

- (void)setFrameOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (void)setFrameSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (void)setFrameCenterVertically {
    CGRect frame = self.frame;
    frame.origin.y = roundf((self.superview.bounds.size.height - frame.size.height) / 2);
    self.frame = frame;
}

- (void)setFrameCenterHorizontally {
    CGRect frame = self.frame;
    frame.origin.x = roundf((self.superview.bounds.size.width - frame.size.width) / 2);
    self.frame = frame;
}

- (void)setFrameToFrame:(CGRect)frame {
    
}

@end

#pragma mark - Functions

UIEdgeInsets UIEdgeInsetsUnionEdgeInsets(UIEdgeInsets inset1, UIEdgeInsets inset2) {
    return UIEdgeInsetsMake(inset1.top + inset2.top,
                            inset1.left + inset2.left,
                            inset1.bottom + inset2.bottom,
                            inset1.right + inset2.right);
}
