//
//  BRShowTagButton.m
//  BabyRun
//
//  Created by fengqiang on 15-1-8.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "BREditTagButton.h"

@implementation BREditTagButton

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    UILongPressGestureRecognizer * longPressGr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressToDo:)];
    longPressGr.minimumPressDuration = 1.0;
    [self addGestureRecognizer:longPressGr];
    self.imgTag = [[ImgTag alloc] init];
    
    return self;
}

- (void)setTagText:(NSString *)text andPosition:(int)position andGroupId:(NSInteger)groupId{
    UIImage *image = [[UIImage imageNamed:@"tag_b"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,25,0,25) resizingMode:UIImageResizingModeStretch];
    
    //计算文字长度
    [self.titleLabel setFont:[UIFont fontWithName:@"HiraKakuProN-W3" size:12]];
    CGSize size = [text sizeWithAttributes: @{NSFontAttributeName: [UIFont fontWithName:@"HiraKakuProN-W3" size:12]}];
    if (size.width < 85) {
        size.width = 85;
    }
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width+10, 26)];
    [self setTitle:text forState:UIControlStateNormal];
    [self setBackgroundImage:image forState:UIControlStateNormal];
    
    UIImage *imageO = [UIImage imageNamed:@"tag_0"];
    UIImageView *imageViewO = [[UIImageView alloc] initWithImage:imageO];
    if (position == 0) {
        [imageViewO setCenter:CGPointMake(-10, self.frame.size.height/2)];
    }
    else{
        [imageViewO setCenter:CGPointMake(self.frame.size.width+10, self.frame.size.height/2)];
    }
    
    [self addSubview:imageViewO];
    [self setClipsToBounds:NO];
    
    self.imgTag.tagText = text;
    self.imgTag.tagOrientation = position;
    self.imgTag.tagGroup = [NSNumber numberWithInteger:groupId];
    self.imgTag.tagPosition = self.frame.origin;
}

-(void)longPressToDo:(UILongPressGestureRecognizer *)gesture{
    if(gesture.state == UIGestureRecognizerStateBegan)
    {
        //CGPoint point = [gesture locationInView:self];
        //NSLog(@"%f,%f",point.x,point.y);
        //[self.delegate longPress:self];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"确定要删除该标签吗？" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertView show];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
     if (buttonIndex == 1) {
         [self.delegate longPress:self];
     }
}

CGPoint beginPoint;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.highlighted = YES;
    UITouch *touch = [touches anyObject];
    beginPoint = [touch locationInView:self];
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.highlighted = NO;
    UITouch *touch = [touches anyObject];
    CGPoint nowPoint = [touch locationInView:self];
    
    float offsetX = nowPoint.x - beginPoint.x;
    float offsetY = nowPoint.y - beginPoint.y;
    
    if(self.frame.origin.x>=0&&self.frame.origin.y>=0){
        if ((self.frame.origin.x+self.frame.size.width)<=self.superview.frame.size.width) {
            if((self.frame.origin.y+self.frame.size.height)<=self.superview.frame.size.height){
                self.center = CGPointMake(self.center.x + offsetX, self.center.y + offsetY);
            }
        }
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(self.frame.origin.x < 0){
        [self setCenter:CGPointMake(0+self.frame.size.width/2, self.center.y)];
    }
    if(self.frame.origin.y < 0){
        [self setCenter:CGPointMake(self.center.x,0+self.frame.size.height/2)];
    }
    if(self.frame.origin.x+self.frame.size.width > self.superview.frame.size.width){
        [self setCenter:CGPointMake(self.superview.frame.size.width-self.frame.size.width/2, self.center.y)];
    }
    if(self.frame.origin.y+self.frame.size.height > self.superview.frame.size.height){
        [self setCenter:CGPointMake(self.center.x,self.superview.frame.size.height-self.frame.size.height/2)];
    }
    
    self.imgTag.tagPosition = self.frame.origin;
}

@end
