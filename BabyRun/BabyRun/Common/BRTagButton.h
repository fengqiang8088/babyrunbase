//
//  BRTagButton.h
//  BabyRun
//
//  Created by fengqiang on 14-12-15.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "ImgTag.h"

@interface BRTagButton : UIButton

@property (nonatomic,strong) ImgTag *imgTag;

- (void)setTagText:(NSString *)text andPosition:(int)position andGroupId:(NSInteger)groupId;

@end
