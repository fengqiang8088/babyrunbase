//
//  BRCollectionView.h
//  BabyRun
//
//  Created by fengqiang on 14-12-15.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"

@class BRCollectionView;

@protocol BRCollectionViewDelegate <NSObject>

@required
- (void)refreshCollection:(BRCollectionView *)collectionView;
- (void)loadMoreDataCollection:(BRCollectionView *)collectionView;

@end

@interface BRCollectionView : UICollectionView

@property (nonatomic, weak) id<BRCollectionViewDelegate> BRCollectionViewDelegate;

@end
