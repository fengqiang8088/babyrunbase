//
//  Activity.h
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Activity : NSObject

//活动图片
@property(nonatomic, strong) AVFile *image;

//活动title
@property(nonatomic, strong) NSString *title;

//活动内容
@property(nonatomic, strong) NSString *content;

//加入人数
@property(nonatomic, strong) NSNumber *joinCount;

//喜欢人数
@property(nonatomic, strong) NSNumber *likeCount;

//是否喜欢
@property(nonatomic, assign) BOOL isLike;

//更新时间
@property(nonatomic, strong) NSDate *updateTime;

//ObjectId
@property(nonatomic, strong) NSString *objectId;

@end
