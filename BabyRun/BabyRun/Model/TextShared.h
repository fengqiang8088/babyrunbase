//
//  TextShared.h
//  BabyRun
//
//  Created by fengqiang on 14-12-15.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TextShared : NSObject

//对应的用户信息
@property(nonatomic, strong) AVUser *user;
//对应图片的类型
@property(nonatomic, strong) NSString *textBackground;
//对应的内容
@property(nonatomic, strong) NSString *sharedTitle;
//对应的内容所属类型
@property(nonatomic, assign) int sharedGroup;
//阅读数
@property(nonatomic, strong) NSNumber *readCount;
//喜欢数
@property(nonatomic, strong) NSNumber *likeCount;
//是否喜欢
@property(nonatomic, assign) BOOL isLike;
//评论数
@property(nonatomic, strong) NSNumber *commentCount;
//更新时间
@property(nonatomic, strong) NSDate *updateTime;
//ObjectId
@property(nonatomic, strong) NSString *objectId;

@end
