//
//  ImgShared.m
//  BabyRun
//
//  Created by fengqiang on 14-12-7.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "ImgShared.h"

@implementation ImgShared

@synthesize user;
@synthesize imgText;
@synthesize imgUrl;
@synthesize readCount;
@synthesize likeCount;
@synthesize commentCount;
@synthesize isLike;
@synthesize relation;
@synthesize updateTime;
@synthesize objectId;
@synthesize tags;
@synthesize image;

@end
