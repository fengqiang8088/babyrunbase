//
//  Personnel.h
//  BabyRun
//
//  Created by fengqiang on 14-12-24.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Personnel : NSObject

//人员图片地址
@property(nonatomic, strong) NSString *imgUrl;
//人员名称
@property(nonatomic, strong) NSString *name;
//人员职位
@property(nonatomic, strong) NSString *position;
//更新时间
@property(nonatomic, strong) NSDate *updateTime;
//ObjectId
@property(nonatomic, strong) NSString *objectId;

@end
