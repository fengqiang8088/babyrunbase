//
//  ImgTag.h
//  BabyRun
//
//  Created by fengqiang on 14-12-14.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImgTag : NSObject

@property (nonatomic,assign) CGPoint tagPosition;
@property (nonatomic,strong) NSString *tagText;
@property (nonatomic,strong) NSString *tagId;
@property (nonatomic,assign) NSNumber *tagGroup;
@property (nonatomic,assign) int tagOrientation;

@end
