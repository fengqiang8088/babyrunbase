//
//  FeederDetail.m
//  BabyRun
//
//  Created by fengqiang on 14-12-17.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "FeederDetail.h"

@implementation FeederDetail

@synthesize image;
@synthesize title;
@synthesize content;
@synthesize subTitle;
@synthesize likeCount;
@synthesize isLike;
@synthesize readCount;
@synthesize objectId;
@synthesize updateTime;

@end
