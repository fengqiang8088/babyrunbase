//
//  Personnel.m
//  BabyRun
//
//  Created by fengqiang on 14-12-24.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "Personnel.h"

@implementation Personnel

@synthesize imgUrl;
@synthesize name;
@synthesize position;
@synthesize updateTime;
@synthesize objectId;

@end
