//
//  NotiMessage.m
//  BabyRun
//
//  Created by fengqiang on 15-1-4.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "NotiMessage.h"

@implementation NotiMessage

@synthesize user;
@synthesize relation;
@synthesize content;
@synthesize type;
@synthesize updateTime;
@synthesize targetClass;
@synthesize targetId;
@synthesize targetContent;
@synthesize messageId;

@end
