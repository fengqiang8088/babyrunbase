//
//  Activity.m
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "Activity.h"

@implementation Activity

@synthesize image;
@synthesize title;
@synthesize content;
@synthesize joinCount;
@synthesize likeCount;
@synthesize objectId;
@synthesize updateTime;

@end
