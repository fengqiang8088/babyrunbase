//
//  NotiMessage.h
//  BabyRun
//
//  Created by fengqiang on 15-1-4.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

@interface NotiMessage : NSObject

//对应的用户信息
@property(nonatomic, strong) AVUser *user;
//用户关系
@property(nonatomic, assign) int relation;  //0:未关注,1:已关注,2:相互关注
//对应的消息内容
@property(nonatomic, strong) NSString *content;
//对应的消息类型
@property(nonatomic, assign) int type;  //0:关注用户,1:评论图片,2:评论文字,3:赞图片,4:赞文字,5:系统消息
//评论时间
@property(nonatomic, strong) NSDate *updateTime;
//对应的内容Class
@property(nonatomic, strong) NSString *targetClass;
//对应的内容ID
@property(nonatomic, strong) NSString *targetId;
//对应的内容对象,url或文字
@property(nonatomic, strong) NSString *targetContent;
@property(nonatomic, assign) NSUInteger messageId;

@end
