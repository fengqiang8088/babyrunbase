//
//  FeederDetail.h
//  BabyRun
//
//  Created by fengqiang on 14-12-17.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeederDetail : NSObject

//奶瓶志图片
@property(nonatomic, strong) AVFile *image;

@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *subTitle;
@property(nonatomic, strong) NSString *content;

//阅读数
@property(nonatomic, strong) NSNumber *readCount;
//喜欢数
@property(nonatomic,strong) NSNumber *likeCount;
//是否喜欢
@property(nonatomic, assign) BOOL isLike;

//更新时间
@property(nonatomic, strong) NSDate *updateTime;

//ObjectId
@property(nonatomic, strong) NSString *objectId;

@end
