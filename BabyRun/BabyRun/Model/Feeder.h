//
//  Feeder.h
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Feeder : NSObject

//奶瓶志图片
@property(nonatomic, strong) AVFile *image;

//奶瓶志title
@property(nonatomic, strong) NSString *title;

//更新时间
@property(nonatomic, strong) NSDate *updateTime;

//ObjectId
@property(nonatomic, strong) NSString *objectId;

@end
