//
//  ViewUser.h
//  BabyRun
//
//  Created by fengqiang on 14-12-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

//首页展示图片信息对象
@interface ViewUser : NSObject

//对应的用户信息
@property(nonatomic, strong) AVUser *user;
//对应的用户与当前用户关系
@property(nonatomic, assign) int relation;  //0:未关注,1:已关注,2:相互关注

@end
