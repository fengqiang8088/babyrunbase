//
//  TextShared.m
//  BabyRun
//
//  Created by fengqiang on 14-12-15.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "TextShared.h"

@implementation TextShared

@synthesize user;
@synthesize textBackground;
@synthesize sharedTitle;
@synthesize sharedGroup;
@synthesize readCount;
@synthesize likeCount;
@synthesize commentCount;
@synthesize isLike;
@synthesize updateTime;
@synthesize objectId;

@end
