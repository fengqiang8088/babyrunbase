//
//  TagComment.h
//  BabyRun
//
//  Created by fengqiang on 14-12-27.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Comment : NSObject

//评论用户
@property(nonatomic, strong) AVUser *user;
//评论内容
@property(nonatomic, strong) NSString *content;
//评论时间
@property(nonatomic, strong) NSDate *updateTime;
//ObjectId
@property(nonatomic, strong) NSString *objectId;

@end
