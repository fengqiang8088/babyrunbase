//
//  ImgTag.m
//  BabyRun
//
//  Created by fengqiang on 14-12-14.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "ImgTag.h"

@implementation ImgTag

@synthesize tagPosition;
@synthesize tagText;
@synthesize tagId;
@synthesize tagGroup;
@synthesize tagOrientation;

@end
