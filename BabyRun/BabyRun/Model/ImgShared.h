//
//  ImgShared.h
//  BabyRun
//
//  Created by fengqiang on 14-12-7.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//


//首页展示图片信息对象
@interface ImgShared : NSObject

//对应的用户信息
@property(nonatomic, strong) AVUser *user;
//图片评论内容
@property(nonatomic, strong) NSString *imgText;
//用户发布的图片地址
@property(nonatomic, strong) NSString *imgUrl;
//阅读数
@property(nonatomic, strong) NSNumber *readCount;
//喜欢数
@property(nonatomic, strong) NSNumber *likeCount;
//是否喜欢
@property(nonatomic, assign) BOOL isLike;
//用户关系
@property(nonatomic, assign) int relation;  //0:未关注,1:已关注,2:相互关注
//评论数
@property(nonatomic, strong) NSNumber *commentCount;
//更新时间
@property(nonatomic, strong) NSDate *updateTime;
//ObjectId
@property(nonatomic, strong) NSString *objectId;
//图片标签
@property(nonatomic, strong) NSArray *tags;
//图片image，用于将图片分享传递使用
@property(nonatomic, strong) UIImage *image;



@end
