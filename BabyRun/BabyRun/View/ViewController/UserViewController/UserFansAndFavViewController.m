//
//  UserFansAndFavViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-23.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "UserFansAndFavViewController.h"

#import "BRTableView.h"
#import "UserManager.h"
#import "UserFFTableViewCell.h"

@interface UserFansAndFavViewController ()<UITableViewDelegate,BRTableViewDelegate>

@property(nonatomic,strong) IBOutlet BRTableView *tableView;
@property(nonatomic,strong) IBOutlet UILabel *titleLabel;
@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;

@end

@implementation UserFansAndFavViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

-(void)loadData{
    [self showLoading];
    [self.parm setObject:@"0" forKey:@"skip"];
    [self.parm setObject:self.user.objectId forKey:@"objectId"];
    [self.result removeAllObjects];
    if (self.type == 0) {
        [[UserManager shared] getFansList:self.parm
                          success:^(id objects){
                              for (NSDictionary *dict in objects) {
                                  [self.result addObject:dict];
                              }
                              // 刷新表格
                              [self.tableView reloadData];
                              [self.tableView headerEndRefreshing];
                              self.isLoadModel = YES;
                              [self hideLoading];
                          }failure:^(NSError *error){
                              self.isLoadModel = NO;
                              [self hideLoading];
                          }];
    }
    else{
        [[UserManager shared] getFavList:self.parm
                          success:^(id objects){
                              for (NSDictionary *dict in objects) {
                                  [self.result addObject:dict];
                              }
                              // 刷新表格
                              [self.tableView reloadData];
                              [self.tableView headerEndRefreshing];
                              self.isLoadModel = YES;
                              [self hideLoading];
                          }failure:^(NSError *error){
                              self.isLoadModel = NO;
                              [self hideLoading];
                          }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.tableView setBRTableViewDelegate:self];
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    if (self.type == 0) {
        [self.titleLabel setText:@"粉丝列表"];
    }
    else{
        [self.titleLabel setText:@"关注列表"];
    }
    [self loadData];
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.result.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 61;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserFFTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[UserFFTableViewCell reuseIdentifier]];
    cell.user = self.result[indexPath.row];
    cell.type = self.type;
    [cell updateUI];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)refreshTable:(BRTableView *)tableview{
    [self showLoading];
    [self.parm setObject:@"0" forKey:@"skip"];
    
    if(self.type == 0){
        [[UserManager shared] getFansList:self.parm
                                  success:^(id objects){
                                      [self.result removeAllObjects];
                                      for (NSDictionary *dict in objects) {
                                          [self.result addObject:dict];
                                      }
                                      // 刷新表格
                                      [tableview reloadData];
                                      [tableview headerEndRefreshing];
                                      [self hideLoading];
                                  }failure:^(NSError *error){
                                      [self hideLoading];
                                  }];
    }
    else{
        [[UserManager shared] getFavList:self.parm
                                  success:^(id objects){
                                      for (NSDictionary *dict in objects) {
                                          [self.result addObject:dict];
                                      }
                                      // 刷新表格
                                      [tableview reloadData];
                                      [tableview headerEndRefreshing];
                                  }failure:^(NSError *error){
                                      
                                  }];
    }
    
}

- (void)loadMoreDataTable:(BRTableView *)tableview{
    [self.parm setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.result.count] forKey:@"skip"];
    if(self.type == 0){
        [[UserManager shared] getFansList:self.parm
                                  success:^(id objects){
                                      for (NSDictionary *dict in objects) {
                                          [self.result addObject:dict];
                                      }
                                      [tableview reloadData];
                                      [tableview footerEndRefreshing];
                                  }failure:^(NSError *error){
                                      
                                  }];
    }
    else{
        [[UserManager shared] getFavList:self.parm
                                  success:^(id objects){
                                      for (NSDictionary *dict in objects) {
                                          [self.result addObject:dict];
                                      }
                                      [tableview reloadData];
                                      [tableview footerEndRefreshing];
                                  }failure:^(NSError *error){
                                      
                                  }];
    }
}

@end
