//
//  UpdatePassViewController.h
//  BabyRun
//
//  Created by fengqiang on 15-1-9.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdatePassViewController : BaseViewController

@property(nonatomic,strong) NSString *smsCode;
@property (nonatomic,assign) int fromType; //0:初始化进入,1:验证登录进入

@end
