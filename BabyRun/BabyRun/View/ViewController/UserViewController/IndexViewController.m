//
//  IndexViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-2.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "IndexViewController.h"
#import "LoginViewController.h"
#import "PublicManager.h"
#import "RegIndexViewController.h"
#import "RegPhoneViewController.h"
#import <TencentOpenAPI/TencentOAuth.h>

@implementation IndexViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
}

-(IBAction)onSeeAnyWhere:(id)sender{
    if (self.fromType == UserLoginFromTypeIndex) {
        UIViewController *tabBarController = [[PublicManager shared] setupViewControllers];
        [PublicManager shared].rootViewController = tabBarController;
        [self.navigationController pushViewController:tabBarController animated:YES];
    }
    else if (self.fromType == UserLoginFromTypeSecond){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(IBAction)onLogin{
    LoginViewController *viewController = [LoginViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
    viewController.fromType = self.fromType;
}

-(IBAction)onReg{
    if([TencentOAuth iphoneQQInstalled]){
        RegIndexViewController *viewController = [RegIndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"RegIndexViewController"];
        [self.navigationController pushViewController:viewController animated:YES];
        viewController.fromType = self.fromType;
    }
    else{
        RegPhoneViewController *viewController = [RegPhoneViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"RegPhoneViewController"];
        viewController.fromType = self.fromType;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

@end
