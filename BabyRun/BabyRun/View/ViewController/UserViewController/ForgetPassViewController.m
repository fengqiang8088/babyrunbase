//
//  ForgetPassViewController.m
//  BabyRun
//
//  Created by fengqiang on 15-1-9.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "ForgetPassViewController.h"
#import "MZTimerLabel.h"
#import "UpdatePassViewController.h"

@interface ForgetPassViewController () <UITextFieldDelegate>

@property(nonatomic,strong) MZTimerLabel *timeLabel;
@property(nonatomic,strong) IBOutlet UILabel *_timeLabelInit;
@property(nonatomic,strong) IBOutlet UITextField *phoneField;
@property(nonatomic,strong) IBOutlet UITextField *codeField;
@property(nonatomic,strong) IBOutlet UIButton *sendBtn;

@end

@implementation ForgetPassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    self.timeLabel = [[MZTimerLabel alloc] initWithLabel:self._timeLabelInit andTimerType:MZTimerLabelTypeTimer];
    [self.timeLabel setCountDownTime:60];
    self.timeLabel.resetTimerAfterFinish = YES;
    self.timeLabel.timeFormat = @"ss";
}

-(IBAction)onSendCode{
    if(self.phoneField.text == nil || self.phoneField.text.length < 1){
        [TSMessage showNotificationInViewController:self title:@"出现错误" subtitle:@"请输入手机号码" type:TSMessageNotificationTypeError];
        return;
    }
    
    //需要打开“启用帐号无关短信验证服务（针对 requestSmsCode 和 verifySmsCode 接口）”
    [AVOSCloud requestSmsCodeWithPhoneNumber:self.phoneField.text
                                         appName:@"奶瓶快跑"
                                       operation:nil
                                      timeToLive:10
                                        callback:^(BOOL succeeded, NSError *error) {
                                            // 执行结果
                                            if (succeeded) {
                                                [self.sendBtn setEnabled:NO];
                                                [TSMessage showNotificationInViewController:self title:@"信息提示" subtitle:@"认证码已经发送至您的手机" type:TSMessageNotificationTypeSuccess];
                                                if(![self.timeLabel counting]){
                                                    [self._timeLabelInit setHidden:NO];
                                                    [self.timeLabel startWithEndingBlock:^(NSTimeInterval countTime) {
                                                        [self._timeLabelInit setHidden:YES];
                                                        [self.sendBtn setEnabled:YES];
                                                    }];
                                                }
                                            }
                                            else{
                                                [TSMessage showNotificationInViewController:self title:@"出现错误" subtitle:@"手机号码格式不正确" type:TSMessageNotificationTypeError];
                                            }
                                        }];
}

-(IBAction)onNext{
    if(self.phoneField.text == nil || self.phoneField.text.length < 1){
        [TSMessage showNotificationInViewController:self title:@"出现错误" subtitle:@"请输入手机号码" type:TSMessageNotificationTypeError];
        return;
    }
    
    if(self.codeField.text == nil || self.codeField.text.length < 1){
        [TSMessage showNotificationInViewController:self title:@"出现错误" subtitle:@"请填写手机认证码" type:TSMessageNotificationTypeError];
        return;
    }
    
    [AVOSCloud verifySmsCode:self.codeField.text callback:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            UpdatePassViewController *viewController = [UpdatePassViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"UpdatePassViewController"];
            viewController.smsCode = self.codeField.text;
            viewController.fromType = self.fromType;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else{
            [TSMessage showNotificationInViewController:self title:@"出现错误" subtitle:@"认证码错误！" type:TSMessageNotificationTypeError];
        }
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
