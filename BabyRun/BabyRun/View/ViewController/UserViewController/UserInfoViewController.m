//
//  UserInfoViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-23.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "UserInfoViewController.h"
#import "UIImageView+WebCache.h"
#import "TSLocateView.h"
#import "UserManager.h"
#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraLibraryViewController.h"

@interface UserInfoViewController ()<UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UIActionSheetDelegate,DBCameraViewControllerDelegate,DBCameraContainerDelegate>

@property (nonatomic,strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) IBOutlet UIImageView *headerView;
@property (nonatomic,strong) IBOutlet UITextField *userPhoneField;
@property (nonatomic,strong) IBOutlet UITextField *userNameField;
@property (nonatomic,strong) IBOutlet UITextField *userInfoField;
@property (nonatomic,strong) IBOutlet UITextField *userAgeField;
@property (nonatomic,strong) IBOutlet UITextField *userGenderField;
@property (nonatomic,strong) IBOutlet UITextField *userAddressField;

@property (nonatomic,strong) IBOutlet UIButton *statusBtn1;
@property (nonatomic,strong) IBOutlet UIButton *statusBtn2;
@property (nonatomic,strong) IBOutlet UIButton *statusBtn3;
@property (nonatomic,strong) IBOutlet UILabel *statusLabel1;
@property (nonatomic,strong) IBOutlet UILabel *statusLabel2;
@property (nonatomic,strong) IBOutlet UILabel *statusLabel3;

@property (nonatomic,strong) IBOutlet UIView *blackView;
//预产期View
@property (nonatomic,strong) IBOutlet UIView *ycqView;
@property (nonatomic,strong) IBOutlet UIDatePicker *ycqdatePicker;
//宝宝信息View
@property (nonatomic,strong) IBOutlet UIView *bbView;
@property (nonatomic,strong) IBOutlet UIButton *bbSexBtn1;
@property (nonatomic,strong) IBOutlet UIButton *bbSexBtn2;
@property (nonatomic,strong) IBOutlet UIDatePicker *bbdatePicker;
@property (nonatomic,strong) TSLocateView *locateView;
@property (nonatomic,assign) BOOL locateViewShow;

//宝宝性别 0:男孩 1:女孩
@property (nonatomic,assign) int bbSex;
//宝宝出生日期
@property (nonatomic,strong) NSDate *bbBirthday;
//宝宝预产期
@property (nonatomic,strong) NSDate *bbWillBirthday;
//妈妈状态 0:备孕 1:已孕 2:已生
@property (nonatomic,assign) int mStatus;

//年龄结果集合
@property (nonatomic,strong) NSMutableArray *arrayAge;
@property (nonatomic,strong) UIPickerView *agePickerVIew;
//性别结果集合
@property (nonatomic,strong) NSMutableArray *arrayGender;
@property (nonatomic,strong) UIPickerView *genderPickerVIew;
@property (nonatomic,strong) UIToolbar *doneToolbar;

@property (nonatomic,assign) BOOL isUserNameHave;

@end

@implementation UserInfoViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    [self setupSelectStutsView];
    [self setupPickerView];
    [self loadData];
}

-(void)setupSelectStutsView{
    //设置用户头像
    [self.headerView sd_setImageWithURL:[NSURL URLWithString:((AVFile *)self.user[@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
    
    [self.ycqdatePicker addTarget:self action:@selector(ycqDateChanged:) forControlEvents:UIControlEventValueChanged];
    [self.bbdatePicker addTarget:self action:@selector(bbDateChanged:) forControlEvents:UIControlEventValueChanged];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onblackView)];
    [self.blackView addGestureRecognizer:tapGesture];
}

-(void)setupPickerView{
    self.doneToolbar = [[UIToolbar alloc] initWithFrame:
                        CGRectMake(0.0f, 0.0f, SCREEN_WIDTH, 44.0f)];
    NSMutableArray *doneToolbarItems = [NSMutableArray array];
    [doneToolbarItems addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    [doneToolbarItems addObject:[[UIBarButtonItem alloc]
                                 initWithTitle:@"确定"
                                 style:UIBarButtonItemStyleDone
                                 target:self
                                 action:@selector(onToolBarDone)]];
    [self.doneToolbar setItems:doneToolbarItems animated:YES];
    
    //初始化年龄相关
    self.arrayAge = [[NSMutableArray alloc] init];
    for (int i=0;i<90;i++) {
        [self.arrayAge addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    self.agePickerVIew = [[UIPickerView alloc] init];
    [self.agePickerVIew setDelegate:self];
    [self.agePickerVIew setDataSource:self];
    
    self.userAgeField.inputView = self.agePickerVIew;
    self.userAgeField.inputAccessoryView = self.doneToolbar;
    self.userAgeField.delegate = self;
    
    //初始化性别相关
    self.arrayGender = [[NSMutableArray alloc] init];
    [self.arrayGender addObject:@"男"];
    [self.arrayGender addObject:@"女"];
    
    self.genderPickerVIew = [[UIPickerView alloc] init];
    [self.genderPickerVIew setDelegate:self];
    [self.genderPickerVIew setDataSource:self];
    
    self.userGenderField.inputView = self.genderPickerVIew;
    self.userGenderField.inputAccessoryView = self.doneToolbar;
    self.userGenderField.delegate = self;
    
    self.locateView = [[TSLocateView alloc] initWithTitle:@"" delegate:self];
}

-(void)loadData{
    [self.userNameField setText:[AVUser currentUser].username];
    [self.userAgeField setText:@""];
    [self.userGenderField setText:[[AVUser currentUser][@"gender"] integerValue] == 0?@"男":@"女"];
    [self.userAddressField setText:[AVUser currentUser][@"address"]?:@""];
    [self.userInfoField setText:[AVUser currentUser][@"userIntroduction"]?:@""];
    [self.userPhoneField setText:[AVUser currentUser].mobilePhoneNumber];
}

-(void)viewDidAppear:(BOOL)animated{
    [self.scrollView setContentSize:CGSizeMake(0, 700)];
    [super viewDidAppear:animated];
}

-(IBAction)onSelectStatus:(UIButton *)btn{
    [self.statusBtn1 setSelected:NO];
    [self.statusBtn2 setSelected:NO];
    [self.statusBtn3 setSelected:NO];
    [self.statusLabel1 setTextColor:BRGRAYColor];
    [self.statusLabel2 setTextColor:BRGRAYColor];
    [self.statusLabel3 setTextColor:BRGRAYColor];
    [btn setSelected:YES];
    self.mStatus = (int)btn.tag;
    
    switch (btn.tag) {
        case 0:
        {
            [self.statusLabel1 setTextColor:BRPurpleColor];
        }
            break;
        case 1:
        {
            [self.statusLabel2 setTextColor:BRPurpleColor];
            [self.blackView setHidden:NO];
            [UIView animateWithDuration:0.3 animations:^{
                [self.ycqView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-self.ycqView.frame.size.height/2)];
            } completion:^(BOOL finished) {
                
            }];
        }
            break;
        case 2:
        {
            [self.statusLabel3 setTextColor:BRPurpleColor];
            [self.blackView setHidden:NO];
            [UIView animateWithDuration:0.3 animations:^{
                [self.bbView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-self.bbView.frame.size.height/2)];
            } completion:^(BOOL finished) {
                
            }];
        }
            break;
        default:
            break;
    }
}

-(void)ycqDateChanged:(UIDatePicker *)sender{
    self.bbWillBirthday = sender.date;
}

-(void)bbDateChanged:(UIDatePicker *)sender{
    self.bbBirthday = sender.date;
}

-(IBAction)sexSelect:(UIButton *)btn{
    [self.bbSexBtn1 setSelected:NO];
    [self.bbSexBtn2 setSelected:NO];
    [btn setSelected:YES];
    self.bbSex = (int)btn.tag;
}

-(void)onblackView{
    switch (self.mStatus) {
        case 1:
        {
            [UIView animateWithDuration:0.3 animations:^{
                [self.ycqView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT+self.ycqView.frame.size.height/2)];
            } completion:^(BOOL finished) {
                [self.blackView setHidden:YES];
            }];
        }
            break;
        case 2:
        {
            [UIView animateWithDuration:0.3 animations:^{
                [self.bbView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT+self.bbView.frame.size.height/2)];
            } completion:^(BOOL finished) {
                [self.blackView setHidden:YES];
            }];
        }
            break;
            
        default:
            break;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)onUserHeader{
    UIActionSheet *headerSheet = [[UIActionSheet alloc] initWithTitle:@"请选择" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册选择", nil];
    headerSheet.tag = 9;
    [headerSheet showInView:self.view];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView == self.agePickerVIew) {
        return [self.arrayAge count];
    }
    else if(pickerView == self.genderPickerVIew){
        return [self.arrayGender count];
    }
    else{
        return 0;
    }
}
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView == self.agePickerVIew) {
        return [self.arrayAge objectAtIndex:row];
    }
    else if(pickerView == self.genderPickerVIew){
        return [self.arrayGender objectAtIndex:row];
    }
    else{
        return @"";
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == self.userAgeField) {
        NSInteger row = [self.agePickerVIew selectedRowInComponent:0];
        textField.text = [self.arrayAge objectAtIndex:row];
    }
    else if(textField == self.userGenderField){
        NSInteger row = [self.genderPickerVIew selectedRowInComponent:0];
        textField.text = [self.arrayGender objectAtIndex:row];
    }
    else if(textField == self.userNameField){
        if (textField.text != nil && textField.text.length > 0) {
            if ([[UserManager shared] isHaveUserName:self.userNameField.text]) {
                [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                   subtitle:NSLocalizedString(@"您的用户名已经被占用，请更换一个", nil)
                                                       type:TSMessageNotificationTypeError];
                self.isUserNameHave = YES;
            }
            else{
                self.isUserNameHave = NO;
            }
        }
    }
}

-(void)onToolBarDone{
    [self.userAgeField resignFirstResponder];
    [self.userGenderField resignFirstResponder];
}

-(IBAction)onUserAddress{
    [self onToolBarDone];
    if (!self.locateViewShow) {
        [self.locateView showInView:self.view];
        self.locateViewShow = YES;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(actionSheet.tag == 9){
        switch (buttonIndex) {
            case 0:
            {
                DBCameraViewController *cameraController =
                [DBCameraViewController initWithDelegate:self];
                [cameraController setForceQuadCrop:YES];
                
                DBCameraContainerViewController *container =
                [[DBCameraContainerViewController alloc] initWithDelegate:self];
                [container setCameraViewController:cameraController];
                [container setFullScreenMode];
                
                UINavigationController *nav =
                [[UINavigationController alloc] initWithRootViewController:container];
                [nav setNavigationBarHidden:YES];
                [self presentViewController:nav animated:YES completion:nil];
            }
                break;
            case 1:
            {
                DBCameraLibraryViewController *libViewController = [[DBCameraLibraryViewController alloc] init];
                [libViewController setDelegate:self];
                [libViewController setContainerDelegate:self];
                
                UINavigationController *nav =
                [[UINavigationController alloc] initWithRootViewController:libViewController];
                [nav setNavigationBarHidden:YES];
                [self presentViewController:nav animated:YES completion:nil];
            }
                break;
            default:
                break;
        }
    }
    else{
        TSLocateView *locateView = (TSLocateView *)actionSheet;
        TSLocation *location = locateView.locate;
        [self.userAddressField setText:[NSString stringWithFormat:@"%@ %@",location.state,location.city]];
        self.locateViewShow = NO;
        //You can uses location to your application.
        if(buttonIndex == 0) {
            NSLog(@"Cancel");
        }else {
            NSLog(@"Select");
        }
    }
}

- (IBAction)onSave{
    if (self.isUserNameHave) {
        [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                    subtitle:NSLocalizedString(@"您的用户名已经被占用，请更换一个", nil)
                                        type:TSMessageNotificationTypeError];
        return;
    }
    AVUser *user = [AVUser currentUser];
    [user setObject:self.userAddressField.text forKey:@"address"];
    [user setObject:self.userInfoField.text forKey:@"userIntroduction"];
    if ([self.userGenderField.text isEqualToString:@"男"]) {
        [user setObject:[NSNumber numberWithInt:0] forKey:@"gender"];
    }
    else{
        [user setObject:[NSNumber numberWithInt:1] forKey:@"gender"];
    }
    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"恭喜", nil)
                                        subtitle:NSLocalizedString(@"修改信息成功", nil)
                                            type:TSMessageNotificationTypeSuccess];
        } else {
            [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                        subtitle:NSLocalizedString(@"修改信息失败", nil)
                                            type:TSMessageNotificationTypeError];
        }
    }];
}

- (void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata {
    //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    [cameraViewController restoreFullScreenMode];
    //更改图片大小
    if (image.size.width>160) {
        image = [self scaleToSize:image size:CGSizeMake(160, 160*image.size.width/image.size.height)];
    }
    
    [cameraViewController dismissViewControllerAnimated:YES completion:^{
        
        //进行图片压缩
        NSData *imageData = UIImageJPEGRepresentation(image,0.75);
        AVFile *imageFile = [AVFile fileWithName:@"postUserImgShared.png" data:imageData];
        
        [self showLoading];
        [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
            AVUser *user = [AVUser currentUser];
            [user setObject:imageFile forKey:@"usericon"];
            [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"恭喜", nil)
                                                subtitle:NSLocalizedString(@"修改信息成功", nil)
                                                    type:TSMessageNotificationTypeSuccess];
                    [self.headerView setImage:image];
                } else {
                    [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                subtitle:NSLocalizedString(@"修改信息失败", nil)
                                                    type:TSMessageNotificationTypeError];
                }
                [self hideLoading];
            }];
        }];
    }];
}

- (void)dismissCamera:(id)cameraViewController {
    [cameraViewController restoreFullScreenMode];
    [cameraViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)backFromController:(id)fromController{
    [fromController dismissViewControllerAnimated:YES completion:nil];
}

-(void)switchFromController:(id)fromController toController:(id)controller{
    
}

- (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size{
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(size);
    // 绘制改变大小的图片
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    // 返回新的改变大小后的图片
    return scaledImage;
}

@end
