//
//  UserInfoViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-12-23.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInfoViewController : BaseViewController

@property (nonatomic,strong) AVUser *user;

@end
