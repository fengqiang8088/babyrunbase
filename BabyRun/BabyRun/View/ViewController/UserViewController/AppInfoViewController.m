//
//  AppInfoViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-23.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "AppInfoViewController.h"

@interface AppInfoViewController ()

@property(nonatomic,strong) IBOutlet UILabel *infoLabel;

@end

@implementation AppInfoViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    
    NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
    [self.infoLabel setText:[NSString stringWithFormat:@"版本信息: v%@",[infoDict objectForKey:@"CFBundleVersion"]]];
}

@end
