//
//  RecommendUserViewController.m
//  BabyRun
//
//  Created by fengqiang on 15-1-2.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "RecommendUserViewController.h"

#import "PublicManager.h"
#import "LikeDetailTableViewCell.h"
#import "BRTableView.h"
#import "UIImageView+WebCache.h"

@interface RecommendUserViewController ()<UITableViewDelegate>

@property(nonatomic,strong) IBOutlet UILabel *titleLabel;
@property(nonatomic,strong) IBOutlet UITableView *tableView;
@property(nonatomic,strong) IBOutlet UILabel *recommendInfo;

@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;

@end

@implementation RecommendUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    [self loadData];
    [self updateUI];
}

-(void)loadData{
    [self showLoading];
    [self.parm setObject:@"0" forKey:@"skip"];
    [self.result removeAllObjects];
    [[PublicManager shared] getRecommendUserList:self.parm
                                     success:^(id objects){
                                         self.result = objects;
                                         // 刷新表格
                                         [self.tableView reloadData];
                                         [self.tableView headerEndRefreshing];
                                         self.isLoadModel = YES;
                                         [self hideLoading];
                                     }failure:^(NSError *error){
                                         self.isLoadModel = NO;
                                         [self hideLoading];
                                     }];
    
    [[PublicManager shared] getRecommendUserInfo:nil
                                    success:^(id objects){
                                        [self.recommendInfo setText:objects];
                                    }failure:^(NSError *error){
                                        self.isLoadModel = NO;
                                    }];
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.result.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LikeDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[LikeDetailTableViewCell reuseIdentifier]];
    cell.data = self.result[indexPath.row];
    [cell updateUI];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)updateUI{
    [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"", nil)
                                subtitle:NSLocalizedString(@"恭喜您注册成功,请选择关注达人", nil)
                                    type:TSMessageNotificationTypeSuccess];
}

-(IBAction)onDone{
    UIViewController *tabBarController = [[PublicManager shared] setupViewControllers];
    [PublicManager shared].rootViewController = tabBarController;
    [self.navigationController pushViewController:tabBarController animated:YES];
}

@end
