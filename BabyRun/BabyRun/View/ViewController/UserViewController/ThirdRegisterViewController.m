//
//  ThirdRegisterViewController.m
//  BabyRun
//
//  Created by fengqiang on 15-1-6.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "ThirdRegisterViewController.h"

#import "RecommendUserViewController.h"
#import "UIImageView+WebCache.h"
#import "UserManager.h"
#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraLibraryViewController.h"

@interface ThirdRegisterViewController () <UIActionSheetDelegate,UITextFieldDelegate,DBCameraViewControllerDelegate,DBCameraContainerDelegate>

@property (nonatomic,strong) IBOutlet UITextField *userNameField;
@property (nonatomic,strong) IBOutlet UIImageView *userHeaderView;

@property (nonatomic,strong) IBOutlet UIButton *statusBtn1;
@property (nonatomic,strong) IBOutlet UIButton *statusBtn2;
@property (nonatomic,strong) IBOutlet UIButton *statusBtn3;
@property (nonatomic,strong) IBOutlet UILabel *statusLabel1;
@property (nonatomic,strong) IBOutlet UILabel *statusLabel2;
@property (nonatomic,strong) IBOutlet UILabel *statusLabel3;

@property (nonatomic,strong) IBOutlet UIView *blackView;
//预产期View
@property (nonatomic,strong) IBOutlet UIView *ycqView;
@property (nonatomic,strong) IBOutlet UIDatePicker *ycqdatePicker;
//宝宝信息View
@property (nonatomic,strong) IBOutlet UIView *bbView;
@property (nonatomic,strong) IBOutlet UIButton *bbSexBtn1;
@property (nonatomic,strong) IBOutlet UIButton *bbSexBtn2;
@property (nonatomic,strong) IBOutlet UIDatePicker *bbdatePicker;

//宝宝性别 0:男孩 1:女孩
@property (nonatomic,assign) int bbSex;
//宝宝出生日期
@property (nonatomic,strong) NSDate *bbBirthday;
//宝宝预产期
@property (nonatomic,strong) NSDate *bbWillBirthday;
//妈妈状态 0:备孕 1:已孕 2:已生
@property (nonatomic,assign) int mStatus;
@property (nonatomic,assign) BOOL isUserNameHave;
@property (nonatomic,strong) AVFile *userHeaderFile;

@end

@implementation ThirdRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
    [self updateUI];
}

- (void)initSetup{
    [self.ycqdatePicker addTarget:self action:@selector(ycqDateChanged:) forControlEvents:UIControlEventValueChanged];
    [self.bbdatePicker addTarget:self action:@selector(bbDateChanged:) forControlEvents:UIControlEventValueChanged];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onblackView)];
    [self.blackView addGestureRecognizer:tapGesture];
    self.userHeaderView.userInteractionEnabled = YES;
    UITapGestureRecognizer *onUserTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onUser)];
    [self.userHeaderView addGestureRecognizer:onUserTap];
    self.mStatus = 0;
    self.bbSex = 0;
    self.bbBirthday = [NSDate date];
    self.bbWillBirthday = [NSDate date];
}

-(void)updateUI{
    [self.userNameField setText:self.data[@"username"]];
    if ([[UserManager shared] isHaveUserName:self.userNameField.text]) {
        [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                    subtitle:NSLocalizedString(@"您的用户名已经被占用，请更换一个", nil)
                                        type:TSMessageNotificationTypeError];
        self.isUserNameHave = YES;
    }
    else{
        self.isUserNameHave = NO;
    }
    
    [self.userHeaderView sd_setImageWithURL:[NSURL URLWithString:self.data[@"avatar"]] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
}

-(IBAction)onDone{
    if(self.userNameField.text == nil || [self.userNameField.text isEqualToString:@""]){
        [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                    subtitle:NSLocalizedString(@"请输入用户名", nil)
                                        type:TSMessageNotificationTypeError];
        return;
    }
    
    if (self.isUserNameHave) {
        [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                    subtitle:NSLocalizedString(@"您的用户名已经被占用，请更换一个", nil)
                                        type:TSMessageNotificationTypeError];
        return;
    }
    
    [self showLoading];
    //设置用户名称
    [self.user setUsername:self.userNameField.text];
    
    [self.user setObject:[NSString stringWithFormat:@"%d",self.mStatus] forKey:@"stage"];
    
    switch (self.mStatus) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *currentDateStr = [dateFormatter stringFromDate:self.bbWillBirthday];
            [self.user setObject:currentDateStr forKey:@"babyWillBirthday"];
        }
            break;
        case 2:
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *currentDateStr = [dateFormatter stringFromDate:self.bbBirthday];
            [self.user setObject:[NSNumber numberWithInt:self.bbSex] forKey:@"babyGender"];
            [self.user setObject:currentDateStr forKey:@"babyBirthday"];
        }
            break;
            
        default:
            break;
    }
    if (self.userHeaderFile==nil) {
        //进行图片压缩
        NSData *imageData = UIImageJPEGRepresentation(self.userHeaderView.image,1);
        self.userHeaderFile = [AVFile fileWithName:@"postUserImgShared.png" data:imageData];
    }
    [self.user setObject:self.userHeaderFile forKey:@"usericon"];

    [self.user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            if (succeeded) {
                [self.user setObject:self.userHeaderFile forKey:@"usericon"];
                [self hideLoading];
                RecommendUserViewController *recommendUserViewController = [RecommendUserViewController brCreateFor:@"PublicStoryboard" andClassIdentifier:@"RecommendUserViewController"];
                [self.navigationController pushViewController:recommendUserViewController animated:YES];
            }
            else{
                [self hideLoading];
                [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                   subtitle:NSLocalizedString(@"头像上传错误，请稍后再试", nil)
                                                       type:TSMessageNotificationTypeError];
            }
        } else {
            [self hideLoading];
            [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                        subtitle:NSLocalizedString(@"注册失败", nil)
                                            type:TSMessageNotificationTypeError];
        }
    }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.text != nil && textField.text.length > 0) {
        [self showLoading];
        if ([[UserManager shared] isHaveUserName:self.userNameField.text]) {
            [self hideLoading];
            [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                               subtitle:NSLocalizedString(@"您的用户名已经被占用，请更换一个", nil)
                                                   type:TSMessageNotificationTypeError];
            self.isUserNameHave = YES;
        }
        else{
            [self hideLoading];
            self.isUserNameHave = NO;
        }
    }
    else{
        self.isUserNameHave = YES;
    }
}

-(IBAction)onSelectStatus:(UIButton *)btn{
    [self.statusBtn1 setSelected:NO];
    [self.statusBtn2 setSelected:NO];
    [self.statusBtn3 setSelected:NO];
    [self.statusLabel1 setTextColor:BRGRAYColor];
    [self.statusLabel2 setTextColor:BRGRAYColor];
    [self.statusLabel3 setTextColor:BRGRAYColor];
    
    [btn setSelected:YES];
    self.mStatus = (int)btn.tag;
    
    switch (btn.tag) {
        case 0:
        {
            [self.statusLabel1 setTextColor:BRPurpleColor];
        }
            break;
        case 1:
        {
            [self.statusLabel2 setTextColor:BRPurpleColor];
            [self.blackView setHidden:NO];
            [UIView animateWithDuration:0.3 animations:^{
                [self.ycqView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-self.ycqView.frame.size.height/2)];
            } completion:^(BOOL finished) {
                
            }];
        }
            break;
        case 2:
        {
            [self.statusLabel3 setTextColor:BRPurpleColor];
            [self.blackView setHidden:NO];
            [UIView animateWithDuration:0.3 animations:^{
                [self.bbView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-self.bbView.frame.size.height/2)];
            } completion:^(BOOL finished) {
                
            }];
        }
            break;
        default:
            break;
    }
}

-(void)ycqDateChanged:(UIDatePicker *)sender{
    self.bbWillBirthday = sender.date;
}

-(void)bbDateChanged:(UIDatePicker *)sender{
    self.bbBirthday = sender.date;
}

-(IBAction)sexSelect:(UIButton *)btn{
    [self.bbSexBtn1 setSelected:NO];
    [self.bbSexBtn2 setSelected:NO];
    [btn setSelected:YES];
    self.bbSex = (int)btn.tag;
}

-(void)onblackView{
    switch (self.mStatus) {
        case 1:
        {
            [UIView animateWithDuration:0.3 animations:^{
                [self.ycqView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT+self.ycqView.frame.size.height/2)];
            } completion:^(BOOL finished) {
                [self.blackView setHidden:YES];
            }];
        }
            break;
        case 2:
        {
            [UIView animateWithDuration:0.3 animations:^{
                [self.bbView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT+self.bbView.frame.size.height/2)];
            } completion:^(BOOL finished) {
                [self.blackView setHidden:YES];
            }];
        }
            break;
            
        default:
            break;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)onUser{
    
}

- (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size{
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(size);
    // 绘制改变大小的图片
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    // 返回新的改变大小后的图片
    return scaledImage;
}

-(IBAction)onUserHeader{
    UIActionSheet *headerSheet = [[UIActionSheet alloc] initWithTitle:@"请选择" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册选择", nil];
    headerSheet.tag = 9;
    [headerSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(actionSheet.tag == 9){
        switch (buttonIndex) {
            case 0:
            {
                DBCameraViewController *cameraController =
                [DBCameraViewController initWithDelegate:self];
                [cameraController setForceQuadCrop:YES];
                
                DBCameraContainerViewController *container =
                [[DBCameraContainerViewController alloc] initWithDelegate:self];
                [container setCameraViewController:cameraController];
                [container setFullScreenMode];
                
                UINavigationController *nav =
                [[UINavigationController alloc] initWithRootViewController:container];
                [nav setNavigationBarHidden:YES];
                [self presentViewController:nav animated:YES completion:nil];
            }
                break;
            case 1:
            {
                DBCameraLibraryViewController *libViewController = [[DBCameraLibraryViewController alloc] init];
                [libViewController setDelegate:self];
                [libViewController setContainerDelegate:self];
                
                UINavigationController *nav =
                [[UINavigationController alloc] initWithRootViewController:libViewController];
                [nav setNavigationBarHidden:YES];
                [self presentViewController:nav animated:YES completion:nil];
            }
                break;
            default:
                break;
        }
    }
}

- (void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata {
    //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    [cameraViewController restoreFullScreenMode];
    //更改图片大小
    if (image.size.width>160) {
        image = [self scaleToSize:image size:CGSizeMake(160, 160)];
    }
    
    [cameraViewController dismissViewControllerAnimated:YES completion:^{
        
        //进行图片压缩
        NSData *imageData = UIImageJPEGRepresentation(image,0.75);
        AVFile *imageFile = [AVFile fileWithName:@"postUserImgShared.png" data:imageData];
        [self.userHeaderView setImage:image];
        [self showLoading];
        [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
            self.userHeaderFile = imageFile;
            [self hideLoading];
        }];
    }];
}

- (void)dismissCamera:(id)cameraViewController {
    [cameraViewController restoreFullScreenMode];
    [cameraViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)backFromController:(id)fromController{
    [fromController dismissViewControllerAnimated:YES completion:nil];
}

-(void)switchFromController:(id)fromController toController:(id)controller{
    
}

@end
