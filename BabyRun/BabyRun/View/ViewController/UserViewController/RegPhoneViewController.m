//
//  RegPhoneViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-4.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "RegPhoneViewController.h"
#import "RecommendUserViewController.h"
#import "UIImageView+WebCache.h"
#import "TSLocateView.h"
#import "UserManager.h"
#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraLibraryViewController.h"

@interface RegPhoneViewController () <UITextFieldDelegate,UIActionSheetDelegate,DBCameraViewControllerDelegate,DBCameraContainerDelegate>

@property (nonatomic,strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) IBOutlet UITextField *userPhoneField;
@property (nonatomic,strong) IBOutlet UITextField *userNameField;
@property (nonatomic,strong) IBOutlet UITextField *userPassField;
@property (nonatomic,strong) IBOutlet UIImageView *userHeader;

@property (nonatomic,strong) IBOutlet UIButton *statusBtn1;
@property (nonatomic,strong) IBOutlet UIButton *statusBtn2;
@property (nonatomic,strong) IBOutlet UIButton *statusBtn3;
@property (nonatomic,strong) IBOutlet UILabel *statusLabel1;
@property (nonatomic,strong) IBOutlet UILabel *statusLabel2;
@property (nonatomic,strong) IBOutlet UILabel *statusLabel3;

@property (nonatomic,strong) IBOutlet UIView *blackView;
//预产期View
@property (nonatomic,strong) IBOutlet UIView *ycqView;
@property (nonatomic,strong) IBOutlet UIDatePicker *ycqdatePicker;
//宝宝信息View
@property (nonatomic,strong) IBOutlet UIView *bbView;
@property (nonatomic,strong) IBOutlet UIButton *bbSexBtn1;
@property (nonatomic,strong) IBOutlet UIButton *bbSexBtn2;
@property (nonatomic,strong) IBOutlet UIDatePicker *bbdatePicker;

//宝宝性别 0:男孩 1:女孩
@property (nonatomic,assign) int bbSex;
//宝宝出生日期
@property (nonatomic,strong) NSDate *bbBirthday;
//宝宝预产期
@property (nonatomic,strong) NSDate *bbWillBirthday;
//妈妈状态 0:备孕 1:已孕 2:已生
@property (nonatomic,assign) int mStatus;
@property (nonatomic,assign) BOOL isUserNameHave;

@property (nonatomic,strong) AVFile *userHeaderFile;

@end

@implementation RegPhoneViewController


-(void)viewDidAppear:(BOOL)animated{
    [self.scrollView setContentSize:CGSizeMake(0, 580)];
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.ycqdatePicker addTarget:self action:@selector(ycqDateChanged:) forControlEvents:UIControlEventValueChanged];
    [self.bbdatePicker addTarget:self action:@selector(bbDateChanged:) forControlEvents:UIControlEventValueChanged];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onblackView)];
    [self.blackView addGestureRecognizer:tapGesture];
    self.mStatus = 0;
    self.bbSex = 0;
    self.bbBirthday = [NSDate date];
    self.bbWillBirthday = [NSDate date];
}

-(IBAction)onDone{
    if (self.userPhoneField.text == nil || [self.userPhoneField.text isEqualToString:@""]) {
        [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                    subtitle:NSLocalizedString(@"请输入手机号", nil)
                                        type:TSMessageNotificationTypeError];
        return;
    }
    
    if(self.userNameField.text == nil || [self.userNameField.text isEqualToString:@""]){
        [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                    subtitle:NSLocalizedString(@"请输入用户名", nil)
                                        type:TSMessageNotificationTypeError];
        return;
    }
    if (self.userPassField.text == nil || [self.userPassField.text isEqualToString:@""]) {
        [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                    subtitle:NSLocalizedString(@"请输入用户密码", nil)
                                        type:TSMessageNotificationTypeError];
        return;
    }
    
    [self showLoading];
    
    AVUser * user = [AVUser user];
    user.username = self.userNameField.text;
    user.password =  self.userPassField.text;
    user.mobilePhoneNumber = self.userPhoneField.text;
    [user setObject:self.userHeaderFile forKey:@"usericon"];
    [user setObject:[NSString stringWithFormat:@"%d",self.mStatus] forKey:@"stage"];
    
    switch (self.mStatus) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *currentDateStr = [dateFormatter stringFromDate:self.bbWillBirthday];
            [user setObject:currentDateStr forKey:@"babyWillBirthday"];
        }
            break;
        case 2:
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *currentDateStr = [dateFormatter stringFromDate:self.bbBirthday];
            [user setObject:[NSNumber numberWithInt:self.bbSex] forKey:@"babyGender"];
            [user setObject:currentDateStr forKey:@"babyBirthday"];
        }
            break;
            
        default:
            break;
    }
    [user setObject:self.userHeaderFile forKey:@"usericon"];
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [self hideLoading];
        if (succeeded) {
            RecommendUserViewController *recommendUserViewController = [RecommendUserViewController brCreateFor:@"PublicStoryboard" andClassIdentifier:@"RecommendUserViewController"];
            [self.navigationController pushViewController:recommendUserViewController animated:YES];
            
        } else {
            if (error.code == 214) {
                [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                   subtitle:NSLocalizedString(@"您的手机号已经注册，请进行登录", nil)
                                                       type:TSMessageNotificationTypeError];
            }
            else{
                [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                   subtitle:NSLocalizedString(@"注册失败,请检查手机号是否格式正确或网络是否畅通", nil)
                                                       type:TSMessageNotificationTypeError];
            }
        }
    }];
}

-(IBAction)onSelectStatus:(UIButton *)btn{
    [self.statusBtn1 setSelected:NO];
    [self.statusBtn2 setSelected:NO];
    [self.statusBtn3 setSelected:NO];
    [self.statusLabel1 setTextColor:BRGRAYColor];
    [self.statusLabel2 setTextColor:BRGRAYColor];
    [self.statusLabel3 setTextColor:BRGRAYColor];
    
    [btn setSelected:YES];
    self.mStatus = (int)btn.tag;
    
    switch (btn.tag) {
        case 0:
        {
            [self.statusLabel1 setTextColor:BRPurpleColor];
        }
            break;
        case 1:
        {
            [self.statusLabel2 setTextColor:BRPurpleColor];
            [self.blackView setHidden:NO];
            [UIView animateWithDuration:0.3 animations:^{
                [self.ycqView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-self.ycqView.frame.size.height/2)];
            } completion:^(BOOL finished) {
                
            }];
        }
            break;
        case 2:
        {
            [self.statusLabel3 setTextColor:BRPurpleColor];
            [self.blackView setHidden:NO];
            [UIView animateWithDuration:0.3 animations:^{
                [self.bbView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT-self.bbView.frame.size.height/2)];
            } completion:^(BOOL finished) {
                
            }];
        }
            break;
        default:
            break;
    }
}

-(void)ycqDateChanged:(UIDatePicker *)sender{
    self.bbWillBirthday = sender.date;
}

-(void)bbDateChanged:(UIDatePicker *)sender{
    self.bbBirthday = sender.date;
}

-(IBAction)sexSelect:(UIButton *)btn{
    [self.bbSexBtn1 setSelected:NO];
    [self.bbSexBtn2 setSelected:NO];
    [btn setSelected:YES];
    self.bbSex = (int)btn.tag;
}

-(IBAction)onblackView{
    switch (self.mStatus) {
        case 1:
        {
            [UIView animateWithDuration:0.3 animations:^{
                [self.ycqView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT+self.ycqView.frame.size.height/2)];
            } completion:^(BOOL finished) {
                [self.blackView setHidden:YES];
            }];
        }
            break;
        case 2:
        {
            [UIView animateWithDuration:0.3 animations:^{
                [self.bbView setCenter:CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT+self.bbView.frame.size.height/2)];
            } completion:^(BOOL finished) {
                [self.blackView setHidden:YES];
            }];
        }
            break;
            
        default:
            break;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self.scrollView setContentOffset:CGPointMake(0,textField.frame.origin.y+textField.frame.size.height) animated:YES];
}

-(IBAction)onUserHeader{
    UIActionSheet *headerSheet = [[UIActionSheet alloc] initWithTitle:@"请选择" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册选择", nil];
    headerSheet.tag = 9;
    [headerSheet showInView:self.view];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == self.userNameField){
        if (textField.text != nil && textField.text.length > 0) {
            [self showLoading];
            if ([[UserManager shared] isHaveUserName:textField.text]) {
                [self hideLoading];
                [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                   subtitle:NSLocalizedString(@"您的用户名已经被占用，请更换一个", nil)
                                                       type:TSMessageNotificationTypeError];
                self.isUserNameHave = YES;
            }
            else{
                [self hideLoading];
                self.isUserNameHave = NO;
            }
        }
        else{
            self.isUserNameHave = YES;
        }
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(actionSheet.tag == 9){
        switch (buttonIndex) {
            case 0:
            {
                DBCameraViewController *cameraController =
                [DBCameraViewController initWithDelegate:self];
                [cameraController setForceQuadCrop:YES];
                
                DBCameraContainerViewController *container =
                [[DBCameraContainerViewController alloc] initWithDelegate:self];
                [container setCameraViewController:cameraController];
                [container setFullScreenMode];
                
                UINavigationController *nav =
                [[UINavigationController alloc] initWithRootViewController:container];
                [nav setNavigationBarHidden:YES];
                [self presentViewController:nav animated:YES completion:nil];
            }
                break;
            case 1:
            {
                DBCameraLibraryViewController *libViewController = [[DBCameraLibraryViewController alloc] init];
                [libViewController setDelegate:self];
                [libViewController setContainerDelegate:self];
                
                UINavigationController *nav =
                [[UINavigationController alloc] initWithRootViewController:libViewController];
                [nav setNavigationBarHidden:YES];
                [self presentViewController:nav animated:YES completion:nil];
            }
                break;
            default:
                break;
        }
    }
}

- (void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata {
    //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    [cameraViewController restoreFullScreenMode];
    //更改图片大小
    if (image.size.width>160) {
        image = [self scaleToSize:image size:CGSizeMake(160, 160)];
    }
    
    [cameraViewController dismissViewControllerAnimated:YES completion:^{
        
        //进行图片压缩
        NSData *imageData = UIImageJPEGRepresentation(image,0.75);
        AVFile *imageFile = [AVFile fileWithName:@"postUserImgShared.png" data:imageData];
        [self.userHeader setImage:image];
        [self showLoading];
        [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
            self.userHeaderFile = imageFile;
            [self hideLoading];
        }];
    }];
}

- (void)dismissCamera:(id)cameraViewController {
    [cameraViewController restoreFullScreenMode];
    [cameraViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)backFromController:(id)fromController{
    [fromController dismissViewControllerAnimated:YES completion:nil];
}

-(void)switchFromController:(id)fromController toController:(id)controller{
    
}

- (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size{
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(size);
    // 绘制改变大小的图片
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    // 返回新的改变大小后的图片
    return scaledImage;
}

@end
