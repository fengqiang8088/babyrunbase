//
//  UserViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-12-1.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserViewController : BaseViewController

@property (nonatomic,strong) AVUser *user;
@property (nonatomic,assign) int showTypeUser;  //0:self，1:other
@property (nonatomic,assign) int showTypeCollection;  //0:small，1:big 2:text

@end
