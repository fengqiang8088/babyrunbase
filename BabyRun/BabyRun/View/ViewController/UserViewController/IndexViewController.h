//
//  IndexViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-12-2.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndexViewController : BaseViewController

@property (nonatomic,assign) int fromType; //0:初始化进入,1:验证登录进入

@end
