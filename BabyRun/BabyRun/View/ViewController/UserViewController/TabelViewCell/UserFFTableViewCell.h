//
//  UserFFTableViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-23.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//
#import "ViewUser.h"

@interface UserFFTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *userHeader;
@property (nonatomic,strong) IBOutlet UILabel *userName;
@property (nonatomic,strong) IBOutlet UILabel *userInfo;
@property (nonatomic,strong) IBOutlet UIButton *opBtn;

@property (nonatomic,strong) ViewUser *user;
@property (nonatomic,assign) int type;  //0:fans，1:fav

+ (NSString *)reuseIdentifier;
-(void)updateUI;

@end
