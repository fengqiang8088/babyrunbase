//
//  UserFFTableViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-23.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "UserFFTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "UserManager.h"

@implementation UserFFTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)updateUI{
    
    //设置用户名称
    [self.userName setText:self.user.user.username];
    
    //设置用户头像
    [self.userHeader sd_setImageWithURL:[NSURL URLWithString:((AVFile *)self.user.user[@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
    
    //设置用户信息
    [self.userInfo setText:self.user.user[@"address"]?:@""];
    
    switch (self.user.relation) {
        case 0:
            [self.opBtn setImage:[UIImage imageNamed:@"fav_no"] forState:UIControlStateNormal];
            break;
        case 1:
            [self.opBtn setImage:[UIImage imageNamed:@"fav_yes"] forState:UIControlStateNormal];
            break;
        case 2:
            [self.opBtn setImage:[UIImage imageNamed:@"fav_too"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }

}

-(IBAction)onLikeBtn{
    if (self.user.relation == 0){
        //关注
        [[AVUser currentUser] follow:self.user.user.objectId andCallback:^(BOOL succeeded, NSError *error) {
            if(succeeded){
                //查询用户之间的所属关系
                AVQuery *queryfr = [AVUser currentUser].followerQuery;
                [queryfr includeKey:@"follower"];
                [queryfr whereKey:@"follower" equalTo:self.user.user];
                if ([queryfr getFirstObject]) {
                    self.user.relation = 2;
                    [self.opBtn setImage:[UIImage imageNamed:@"fav_too"] forState:UIControlStateNormal];
                }
                else{
                    [self.opBtn setImage:[UIImage imageNamed:@"fav_yes"] forState:UIControlStateNormal];
                    self.user.relation = 1;
                }
                [TSMessage showNotificationWithTitle:@"关注成功" type:TSMessageNotificationTypeSuccess];
                
                //发送给指定对象通知
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                [dic setObject:self.user.user.objectId forKey:@"objectId"];
                [dic setObject:@"关注了你" forKey:@"content"];
                [dic setObject:@"0" forKey:@"statusType"];
                [dic setObject:@"0" forKey:@"messageType"];
                [[UserManager shared] addUserMessage:dic success:^(id objects){
                    if ([objects[@"success"] boolValue]) {
                    }
                }failure:^(NSError *error){
                    
                }];
                
                //发送给全体粉丝对象通知
                NSMutableDictionary *dicAll = [[NSMutableDictionary alloc] init];
                NSString *content = [NSString stringWithFormat:@"关注了 %@",self.user.user.username];
                [dicAll setObject:content forKey:@"content"];
                [dicAll setObject:self.user.user.objectId forKey:@"targetId"];
                [dicAll setObject:@"user" forKey:@"targetClass"];
                [dicAll setObject:@"0" forKey:@"messageType"];
                [dicAll setObject:@"" forKey:@"targetContent"];
                
                [[UserManager shared] pushMessageToFans:dicAll success:^(id objects){
                    if ([objects[@"success"] boolValue]) {
                        
                    }
                }failure:^(NSError *error){
                    
                }];
            }
            else{
                [TSMessage showNotificationWithTitle:@"关注失败，请稍后再试" type:TSMessageNotificationTypeError];
            }
        }];
    }
    else{
        [[AVUser currentUser] unfollow:self.user.user.objectId andCallback:^(BOOL succeeded, NSError *error) {
            if(succeeded){
                //查询用户之间的所属关系
                [self.opBtn setImage:[UIImage imageNamed:@"PicIndexCell_unfav_btn"] forState:UIControlStateNormal];
                self.user.relation = 0;
                [TSMessage showNotificationWithTitle:@"取消关注" type:TSMessageNotificationTypeWarning];
            }
            else{
                [TSMessage showNotificationWithTitle:@"取消关注失败，请稍后再试" type:TSMessageNotificationTypeError];
            }
        }];
    }
}

@end
