//
//  UserViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-1.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "UserViewController.h"
#import "UserManager.h"
#import "UserHeadCollectionViewCell.h"
#import "UserSegmentCollectionViewCell.h"
#import "UserPicSmallCollectionViewCell.h"
#import "EdBigPicCollectionViewCell.h"
#import "UserTextCollectionViewCell.h"
#import "AddUserViewController.h"
#import "NotificationViewController.h"
#import "BRCollectionView.h"
#import "UserFansAndFavViewController.h"
#import "UserSettingViewController.h"
#import "IndexViewController.h"
#import "TagIndexViewController.h"
#import "PicDetailViewController.h"
#import "CommunityDetailViewController.h"

@interface UserViewController () <UICollectionViewDelegate,UserSegmentCollectionViewCellDelegate,BRCollectionViewDelegate,EdBigPicCollectionViewCellDelegate,UserHeadCollectionViewCellDelegate,UserTextCollectionViewCellDelegate>

@property(nonatomic,strong) IBOutlet BRCollectionView *collectionView;
@property(nonatomic,strong) IBOutlet UIButton *leftBtn;
@property(nonatomic,strong) IBOutlet UILabel *titleLabel;
@property(nonatomic,strong) IBOutlet UIButton *rightBtn;

@property(nonatomic,strong) NSMutableArray *resultPic;
@property(nonatomic,strong) NSMutableDictionary *parmPic;
@property(nonatomic,strong) NSMutableArray *resultText;
@property(nonatomic,strong) NSMutableDictionary *parmText;
@property(nonatomic,strong) NSString *favsText;
@property(nonatomic,strong) NSString *fansText;
@property(nonatomic,strong) ViewUser *viewUser;

@end

@implementation UserViewController

- (void)viewWillAppear:(BOOL)animated{
    if(self.showTypeUser == 0)
        [[self rdv_tabBarController] setTabBarHidden:NO animated:YES];
    else
        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    [self showLoading];
}

- (void)viewDidAppear:(BOOL)animated{
    [self loadData];
    [self.titleLabel setText:self.user.username];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
    [self initNavBar];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    
    self.parmPic = [[NSMutableDictionary alloc] init];
    self.resultPic = [[NSMutableArray alloc] init];
    self.parmText = [[NSMutableDictionary alloc] init];
    self.resultText = [[NSMutableArray alloc] init];
    [self.collectionView setBRCollectionViewDelegate:self];
    [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
}

-(void)initNavBar{
    if (self.showTypeUser == 0) {
        [self.leftBtn setImage:[UIImage imageNamed:@"adduser"] forState:UIControlStateNormal];
        [self.leftBtn addTarget:self action:@selector(onAdduser) forControlEvents:UIControlEventTouchUpInside];
        [self.rightBtn setImage:[UIImage imageNamed:@"setting"] forState:UIControlStateNormal];
        [self.rightBtn addTarget:self action:@selector(onSetting) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        [self.leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        [self.leftBtn addTarget:self action:@selector(onBack) forControlEvents:UIControlEventTouchUpInside];
        [self.rightBtn setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
        [self.rightBtn addTarget:self action:@selector(onSetting) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)loadData{
    [self.parmPic setObject:self.user forKey:@"user"];
    [self.parmText setObject:self.user forKey:@"user"];
    
    [self.user getFollowersAndFollowees:^(NSDictionary *dict, NSError *error) {
        NSArray *followees = dict[@"followees"];
        self.favsText = [NSString stringWithFormat:@"%lu 关注",(unsigned long)followees.count];
        NSArray *followers = dict[@"followers"];
        self.fansText  = [NSString stringWithFormat:@"%lu 粉丝",(unsigned long)followers.count];
        // 刷新表格
        //[self.collectionView reloadData];
        if(self.showTypeUser == 1){
            self.viewUser = [[UserManager shared] getUserRelation:self.user];
        }
        else{
            self.viewUser = [[ViewUser alloc] init];
            [self.viewUser setUser:self.user];
            self.viewUser.relation = -1;
        }
        
        NSIndexPath *indexPath = [[NSIndexPath alloc] initWithIndex:0];
        [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
        [self hideLoading];
        [self loadDataForCollectionView:0];
    }];
}

-(void)loadDataForCollectionView:(int)type{
    switch (type) {
        case 0:
        {
            [self.parmPic setObject:@"0" forKey:@"skip"];
            [self.resultPic removeAllObjects];
            [[UserManager shared] getPostPicList:self.parmPic
                                         success:^(id objects){
                                             self.resultPic  = objects;
                                             [self.collectionView reloadData];
                                         }failure:^(NSError *error){
                                             
                                         }];
        }
            break;
        case 1:
        {
            [self.parmPic setObject:@"0" forKey:@"skip"];
            [self.resultPic removeAllObjects];
            [[UserManager shared] getPostPicList:self.parmPic
                                         success:^(id objects){
                                             self.resultPic  = objects;
                                             [self.collectionView reloadData];
                                         }failure:^(NSError *error){
                                             
                                         }];
        }
            break;
        case 2:
        {
            [self.parmText setObject:@"0" forKey:@"skip"];
            [self.resultText removeAllObjects];
            [[UserManager shared] getPostTextList:self.parmText
                                          success:^(id objects){
                                              self.resultText = objects;
                                              [self.collectionView reloadData];
                                          }failure:^(NSError *error){
                                              
                                          }];
        }
            break;
        default:
            break;
    }
}

#pragma mark -- UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    switch (self.showTypeCollection) {
        case 0:
            return 2+self.resultPic.count;
            break;
        case 1:
            return 2+self.resultPic.count;
        case 2:
            return 2+self.resultText.count;
        default:
            return 2;
            break;
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
        {
            UserHeadCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UserHeadCollectionViewCell" forIndexPath:indexPath];
            cell.user = self.viewUser;
            cell.favsText = self.favsText;
            cell.fansText = self.fansText;
            cell.delegate = self;
            [cell updateUI];
            return cell;
        }
            break;
        case 1:
        {
            UserSegmentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[UserSegmentCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
            cell.delegate = self;
            return cell;
        }
            break;
        default:
        {
            switch (self.showTypeCollection) {
                case 0:
                {
                    UserPicSmallCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UserPicSmallCollectionViewCell" forIndexPath:indexPath];
                    cell.imgShared = self.resultPic[indexPath.row-2];
                    [cell updateUI];
                    return cell;
                }
                    break;
                case 1:
                {
                    EdBigPicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[EdBigPicCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
                    cell.imgShared = self.resultPic[indexPath.row-2];
                    cell.delegate = self;
                    [cell updateUI];
                    return cell;
                }
                    break;
                case 2:
                {
                    UserTextCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UserTextCollectionViewCell" forIndexPath:indexPath];
                    cell.textShared = self.resultText[indexPath.row-2];
                    cell.delegate = self;
                    [cell updateUI];
                    return cell;
                }
                    break;
                default:
                {
                    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
                    return cell;
                }
                    break;
            }
            
        }
            break;
    }
}

#pragma mark --UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
        {
            return CGSizeMake(SCREEN_WIDTH, 212);
        }
            break;
        case 1:
        {
            return CGSizeMake(SCREEN_WIDTH, 45);
        }
            break;
        default:
        {
            switch (self.showTypeCollection) {
                case 0:
                    return CGSizeMake(106, 100);
                    break;
                    
                default:
                    return CGSizeMake(SCREEN_WIDTH,320);
                    break;
            }
        }
            break;
    }
}

#pragma mark --UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark --UserSegmentCollectionViewCellDelegate

-(void)onChangeType:(int)type{
    self.showTypeCollection = type;
    switch (self.showTypeCollection) {
        case 0:
        {
            if (self.resultPic.count == 0) {
                [self loadDataForCollectionView:type];
            }
        }
            break;
        case 1:
        {
            if (self.resultPic.count == 0) {
                [self loadDataForCollectionView:type];
            }
        }
            break;
        case 2:
        {
            if (self.resultText.count == 0) {
                [self loadDataForCollectionView:type];
            }
        }
            break;
            
        default:
            break;
    }
    [self.collectionView reloadData];
}

-(IBAction)onFansBtn{
    if ([AVUser currentUser]!=nil) {
        UserFansAndFavViewController *viewController = [UserFansAndFavViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserFansAndFavViewController"];
        viewController.type = 0;
        viewController.user = self.user;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else{
        [self onLogin];
    }
    
}

-(IBAction)onFavBtn{
    if ([AVUser currentUser]!=nil) {
        UserFansAndFavViewController *viewController = [UserFansAndFavViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserFansAndFavViewController"];
        viewController.type = 1;
        viewController.user = self.user;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else{
        [self onLogin];
    }
}

-(void)onBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onAdduser{
    AddUserViewController *viewController = [AddUserViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"AddUserViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)onSetting{
    UserSettingViewController *viewController = [UserSettingViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserSettingViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark --BRCollectionView Delegate
- (void)refreshCollection:(BRCollectionView *)collectionView{
    switch (self.showTypeCollection) {
        case 0:
        {
            [self.parmPic setObject:@"0" forKey:@"skip"];
            [[UserManager shared] getPostPicList:self.parmPic
                                         success:^(id objects){
                                             [self.resultPic removeAllObjects];
                                             self.resultPic = objects;
                                             [self.collectionView reloadData];
                                             [self.collectionView headerEndRefreshing];
                                         }failure:^(NSError *error){
                                         }];
        }
            break;
        case 1:
        {
            [self.parmPic setObject:@"0" forKey:@"skip"];
            [[UserManager shared] getPostPicList:self.parmPic
                                         success:^(id objects){
                                             [self.resultPic removeAllObjects];
                                             self.resultPic = objects;
                                             [self.collectionView reloadData];
                                             [self.collectionView headerEndRefreshing];
                                         }failure:^(NSError *error){
                                         }];
        }
            break;
        case 2:
        {
            [self.parmText setObject:@"0" forKey:@"skip"];
            [[UserManager shared] getPostTextList:self.parmText
                                          success:^(id objects){
                                              [self.resultText removeAllObjects];
                                              self.resultText = objects;
                                              [self.collectionView reloadData];
                                              [self.collectionView headerEndRefreshing];
                                          }failure:^(NSError *error){
                                          }];
        }
            break;
            
        default:
            break;
    }
}

-(void)loadMoreDataCollection:(BRCollectionView *)collectionView{
    switch (self.showTypeCollection) {
        case 0:
        {
            [self.parmPic setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.resultPic.count] forKey:@"skip"];
            [[UserManager shared] getPostPicList:self.parmPic
                                         success:^(id objects){
                                             for (NSDictionary *dict in objects) {
                                                 [self.resultPic addObject:dict];
                                             }
                                             [self.collectionView reloadData];
                                             [self.collectionView footerEndRefreshing];
                                         }failure:^(NSError *error){
                                             
                                         }];
        }
            break;
        case 1:
        {
            [self.parmPic setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.resultPic.count] forKey:@"skip"];
            [[UserManager shared] getPostPicList:self.parmPic
                                         success:^(id objects){
                                             for (NSDictionary *dict in objects) {
                                                 [self.resultPic addObject:dict];
                                             }
                                             [self.collectionView reloadData];
                                             [self.collectionView footerEndRefreshing];
                                         }failure:^(NSError *error){
                                             
                                         }];
        }
            break;
        case 2:
        {
            [self.parmText setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.resultText.count] forKey:@"skip"];
            [[UserManager shared] getPostTextList:self.parmText
                                          success:^(id objects){
                                              for (NSDictionary *dict in objects) {
                                                  [self.resultText addObject:dict];
                                              }
                                              [self.collectionView reloadData];
                                              [self.collectionView footerEndRefreshing];
                                          }failure:^(NSError *error){
                                              
                                          }];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - EdBigPicCollectionViewCellDelegate

-(void)selectTag:(ImgTag *)imgTag{
    TagIndexViewController *tagIndexViewController = [TagIndexViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"TagIndexViewController"];
    tagIndexViewController.imgTag = imgTag;
    [self.navigationController pushViewController:tagIndexViewController animated:YES];
}

- (void)selectUser:(AVUser *)user{
    UserViewController *userViewController = [UserViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserStoryboard"];
    userViewController.user = user;
    userViewController.showTypeUser = 1;
    [self.navigationController pushViewController:userViewController animated:YES];
}

-(void)selectComment:(ImgShared *)imgShared{
    PicDetailViewController *picDetailViewController = [PicDetailViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"PicDetailViewController"];
    picDetailViewController.imgShared = imgShared;
    [self.navigationController pushViewController:picDetailViewController animated:YES];
}

-(void)selectTextComment:(TextShared *)textShared{
    CommunityDetailViewController *communityDetailViewController = [CommunityDetailViewController brCreateFor:@"CommunityStoryboard" andClassIdentifier:@"CommunityDetailViewController"];
    communityDetailViewController.textShared = textShared;
    [self.navigationController pushViewController:communityDetailViewController animated:YES];
}

-(void)onLogin{
    IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
    indexViewController.fromType = 1;
    UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:indexViewController];
    [self presentViewController:indexNavigationController animated:YES completion:nil];
}

-(void)selectMore:(ImgShared *)imgShared{
    if([AVUser currentUser]!=nil){
        [self showShareView:imgShared];
    }
    else{
        [self onLogin];
    }
}

-(void)selectTextMore:(TextShared *)textShared{
    if([AVUser currentUser]!=nil){
        [self showShareView:textShared];
    }
    else{
        [self onLogin];
    }
}

@end
