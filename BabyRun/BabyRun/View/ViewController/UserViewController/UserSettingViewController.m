//
//  UserSettingViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-23.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "UserSettingViewController.h"

#import "UserInfoViewController.h"
#import "UserFeedbackViewController.h"
#import "UIImageView+WebCache.h"
#import "AppInfoViewController.h"
#import <ShareSDK/ShareSDK.h>

@interface UserSettingViewController ()<UITableViewDelegate>

@property(nonatomic,strong) IBOutlet UITableView *tableView;
@property(nonatomic,strong) IBOutlet UILabel *titleLabel;

@end

@implementation UserSettingViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    [self.titleLabel setText:@"设置"];
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 4;
            break;
        default:
            return 0;
            break;
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
            // 会员中心
            return @"个人信息与账号";
            break;
        case 1:
            // 推送
            return @"服务与支持";
            break;
        default:
            return @"";
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 单元格标识符
    static NSString *identifier = @"SettingsCell";
    
    // 创建或重用单元格
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        // 无法重用时创建单元格
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.detailTextLabel.text = @"";
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    // 为单元格设置属性
    switch (indexPath.section) {
        case 0:
        {
            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:((AVFile *)[AVUser currentUser][@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
            [cell.textLabel setText:[AVUser currentUser].username];
        }
            break;
        case 1:
        {
            // 推送
            switch (indexPath.row) {
                case 0:{
                    // 查看推送消息
                    cell.textLabel.text = @"关于奶瓶快跑";
                    break;
                }
                case 1:{
                    // 推送设置
                    cell.textLabel.text = @"分享给朋友";
                    break;
                }
                case 2:{
                    // 查看推送消息
                    cell.textLabel.text = @"鼓励一下";
                    break;
                }
                case 3:{
                    // 推送设置
                    cell.textLabel.text = @"意见反馈";
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    UserInfoViewController *viewController = [UserInfoViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserInfoViewController"];
                    viewController.user = [AVUser currentUser];
                    [self.navigationController pushViewController:viewController animated:YES];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    AppInfoViewController *viewController = [AppInfoViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"AppInfoViewController"];
                    [self.navigationController pushViewController:viewController animated:YES];
                }
                    break;
                case 1:
                {
                    
                }
                    break;
                case 2:
                {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/%@",babyRunId]]];
                }
                    break;
                case 3:
                {
                    UserFeedbackViewController *viewController = [UserFeedbackViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserFeedbackViewController"];
                    [self.navigationController pushViewController:viewController animated:YES];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
}

-(IBAction)onLogout{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"是否退出当前账号"
                                                   delegate:self
                                          cancelButtonTitle:@"取消"
                                          otherButtonTitles:@"确定",nil];
    [alert show];
}

//根据被点击按钮的索引处理点击事件
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [ShareSDK cancelAuthWithType:ShareTypeSinaWeibo];
        [ShareSDK cancelAuthWithType:ShareTypeQQSpace];
        [ShareSDK cancelAuthWithType:ShareTypeWeixiSession];
        [ShareSDK cancelAuthWithType:ShareTypeWeixiTimeline];
        [AVUser logOut];
        //发送去掉关注界面的通知
        [[NSNotificationCenter defaultCenter] postNotificationName:BRNOTI_UPDATE_GZ object:nil];
        
        [self.navigationController popViewControllerAnimated:YES];
        [[self rdv_tabBarController] setSelectedIndex:0];
    }
}

@end
