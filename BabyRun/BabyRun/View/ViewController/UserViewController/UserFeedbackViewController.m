//
//  UserFeedbackViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-23.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "UserFeedbackViewController.h"
#import "UserManager.h"
#import "UserFeedbackCollectionViewCell.h"

@interface UserFeedbackViewController () <UICollectionViewDelegate>

@property (nonatomic,strong) IBOutlet UIScrollView *rootView;
@property (nonatomic,strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) IBOutlet UITextView *textView;

@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;
@property(nonatomic,strong) NSString *targetId;

@end

@implementation UserFeedbackViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.rootView setContentSize:CGSizeMake(0, 645)];
    [self loadData];
}

- (void)loadData{
    [[UserManager shared] getPersonList:self.parm
                                 success:^(id objects){
                                     for (NSDictionary *dict in objects) {
                                         [self.result addObject:dict];
                                     }
                                     // 刷新表格
                                     [self.collectionView reloadData];
                                 }failure:^(NSError *error){
                                     
                                 }];
}

#pragma mark -- UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.result.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UserFeedbackCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UserFeedbackCollectionViewCell" forIndexPath:indexPath];
    cell.person = self.result[indexPath.row];
    [cell updateUI];
    return cell;
}

#pragma mark --UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(98,100);
}

#pragma mark --UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UserFeedbackCollectionViewCell *cell = (UserFeedbackCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell setSelected:YES];
    self.targetId = cell.person.objectId;
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(IBAction)onSend{
    if (self.textView.text==nil||self.textView.text.length == 0) {
        [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                    subtitle:NSLocalizedString(@"请填写您的宝贵意见", nil)
                                        type:TSMessageNotificationTypeError];
        return;
    }
    
    if (self.targetId ==nil || self.targetId.length == 0) {
        [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                    subtitle:NSLocalizedString(@"请选择您要提交的相关人员", nil)
                                        type:TSMessageNotificationTypeError];
        return;
    }
    
    AVObject *feedback = [AVObject objectWithClassName:@"feedback"];
    [feedback setObject:self.textView.text forKey:@"content"];
    [feedback setObject:self.targetId forKey:@"targetId"];
    [feedback setObject:[AVUser currentUser].objectId forKey:@"userId"];
    [feedback saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"恭喜", nil)
                                        subtitle:NSLocalizedString(@"您的意见已提交，我们会尽快处理", nil)
                                            type:TSMessageNotificationTypeSuccess];
        } else {
            [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                        subtitle:NSLocalizedString(@"提交失败，请您稍后提交", nil)
                                            type:TSMessageNotificationTypeError];
        }
    }];
}

@end
