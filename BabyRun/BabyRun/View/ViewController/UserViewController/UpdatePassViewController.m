//
//  UpdatePassViewController.m
//  BabyRun
//
//  Created by fengqiang on 15-1-9.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "UpdatePassViewController.h"
#import "LoginViewController.h"

@interface UpdatePassViewController () <UITextFieldDelegate>

@property(nonatomic,strong) IBOutlet UITextField *pass1Field;
@property(nonatomic,strong) IBOutlet UITextField *pass2Field;

@end

@implementation UpdatePassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{

}

-(IBAction)onDone{
    if(self.pass1Field.text == nil || self.pass1Field.text.length < 1){
        [TSMessage showNotificationInViewController:self title:@"出现错误" subtitle:@"请输入密码" type:TSMessageNotificationTypeError];
        return;
    }
    
    if(self.pass2Field.text == nil || self.pass2Field.text.length < 1){
        [TSMessage showNotificationInViewController:self title:@"出现错误" subtitle:@"请再次输入密码" type:TSMessageNotificationTypeError];
        return;
    }
    
    if(![self.pass1Field.text isEqualToString:self.pass2Field.text]){
        [TSMessage showNotificationInViewController:self title:@"出现错误" subtitle:@"两次输入号码不一致" type:TSMessageNotificationTypeError];
        return;
    }
    
    [AVUser resetPasswordWithSmsCode:self.smsCode newPassword:self.pass1Field.text block:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [TSMessage showNotificationWithTitle:@"修改成功" type:TSMessageNotificationTypeSuccess];
            LoginViewController *viewController = [LoginViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"LoginViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
            viewController.fromType = self.fromType;
        } else {
            [TSMessage showNotificationInViewController:self title:@"出现错误" subtitle:@"修改失败，请稍后再试" type:TSMessageNotificationTypeError];
        }
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
