//
//  AddUserViewController.m
//  BabyRun
//
//  Created by fengqiang on 15-1-4.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "AddUserViewController.h"

#import "UserManager.h"
#import "LikeDetailTableViewCell.h"
#import "BRTableView.h"

@interface AddUserViewController ()<UITableViewDelegate,BRTableViewDelegate,UISearchBarDelegate>

@property(nonatomic,strong) IBOutlet BRTableView *tableView;
@property(nonatomic,strong) IBOutlet UIButton *searchBtn;
@property(nonatomic,strong) IBOutlet UIButton *qqBtn;
@property(nonatomic,strong) IBOutlet UIButton *telBtn;
@property(nonatomic,strong) IBOutlet UIButton *sinaBtn;
@property(nonatomic,strong) IBOutlet UIButton *weixinBtn;
@property(nonatomic,strong) IBOutlet UISearchBar *searchBar;
@property(nonatomic,strong) IBOutlet UIView *segmentView;

@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;

@end

@implementation AddUserViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [self initSetup];
}

- (void)initSetup{
    [self.tableView setBRTableViewDelegate:self];
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

-(void)loadData{
    if (self.searchBar.text == nil || self.searchBar.text.length == 0) {
        return;
    }
    [self.parm setObject:@"0" forKey:@"skip"];
    [self.parm setObject:self.searchBar.text forKey:@"username"];
    [[UserManager shared] findUserList:self.parm
                                     success:^(id objects){
                                         self.result = objects;
                                         // 刷新表格
                                         [self.tableView reloadData];
                                         self.isLoadModel = YES;
                                     }failure:^(NSError *error){
                                         self.isLoadModel = NO;
                                     }];
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.result.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LikeDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[LikeDetailTableViewCell reuseIdentifier]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.data = self.result[indexPath.row];
    [cell updateUI];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)refreshTable:(BRTableView *)tableview{
    [self.parm setObject:@"0" forKey:@"skip"];
    
    [[UserManager shared] findUserList:self.parm
                                      success:^(id objects){
                                          [self.result removeAllObjects];
                                          self.result = objects;
                                          // 刷新表格
                                          [tableview reloadData];
                                          [tableview headerEndRefreshing];
                                      }failure:^(NSError *error){
                                          [tableview headerEndRefreshing];
                                      }];
}

- (void)loadMoreDataTable:(BRTableView *)tableview{
    [self.parm setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.result.count] forKey:@"skip"];
    [[UserManager shared] findUserList:self.parm
                                      success:^(id objects){
                                          for (NSDictionary *dict in objects) {
                                              [self.result addObject:dict];
                                          }
                                          [tableview reloadData];
                                          [tableview footerEndRefreshing];
                                      }failure:^(NSError *error){
                                          
                                      }];
}

#pragma mark - UISearchBarDelegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self loadData];
    [searchBar resignFirstResponder];
}

-(IBAction)onChange:(id)sender{
    UIButton *btn = (UIButton *)sender;
    for(UIView *view in [self.segmentView subviews]){
        if ([view isKindOfClass:[UIButton class]]) {
            if (((UIButton *)view) == btn) {
                [((UIButton *)view)setSelected:YES];
            }
            else{
                [((UIButton *)view)setSelected:NO];
            }
        }
    }
    
    switch (btn.tag) {
        case 0:{
            [self loadData];
        }
            break;
        case 1:{
            
        }
            break;
        case 2:{
            
        }
            break;
        case 3:{
            
        }
            break;
        case 4:{
            
        }
            break;
        default:
            break;
    }
}

@end
