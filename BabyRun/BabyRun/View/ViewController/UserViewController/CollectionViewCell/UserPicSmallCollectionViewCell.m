//
//  UserPicSmallCollectionViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-22.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "UserPicSmallCollectionViewCell.h"
#import "UIImageView+WebCache.h"

@implementation UserPicSmallCollectionViewCell

- (void)updateUI{
    
    //设置用户发布图片
    [self.image sd_setImageWithURL:[NSURL URLWithString:self.imgShared.imgUrl] placeholderImage:[UIImage imageNamed:@"picindex_ph"]];
}

@end
