//
//  UserTextCollectionViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-22.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "TextShared.h"

@protocol UserTextCollectionViewCellDelegate <NSObject>

@required
- (void)selectUser:(AVUser *)user;
- (void)selectTextComment:(TextShared *)textShared;
- (void)selectTextMore:(TextShared *)textShared;
- (void)onLogin;
@end

@interface UserTextCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UIImageView *userHeader;
@property (nonatomic,strong) IBOutlet UIImageView *cellBackgroundView;
@property (nonatomic,strong) IBOutlet UILabel *userName;
@property (nonatomic,strong) IBOutlet UILabel *userInfo;
@property (nonatomic,strong) IBOutlet UILabel *userContext;
@property (nonatomic,strong) IBOutlet UILabel *groupName;
@property (nonatomic,strong) IBOutlet UILabel *readCount;
@property (nonatomic,strong) IBOutlet UIButton *likeBtn;
@property (nonatomic,strong) IBOutlet UIButton *commentBtn;

@property (nonatomic,strong) TextShared *textShared;
@property (nonatomic,weak) id<UserTextCollectionViewCellDelegate> delegate;

+ (NSString *)reuseIdentifier;
- (void)updateUI;

@end
