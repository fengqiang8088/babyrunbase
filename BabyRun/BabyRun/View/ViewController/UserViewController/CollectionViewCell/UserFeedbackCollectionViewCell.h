//
//  UserFeedbackCollectionViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-24.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//
#import "Personnel.h"

@interface UserFeedbackCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UIImageView *image;
@property (nonatomic,strong) IBOutlet UIImageView *selectImage;
@property (nonatomic,strong) Personnel *person;

- (void)updateUI;
- (void)updateSelect;

@end
