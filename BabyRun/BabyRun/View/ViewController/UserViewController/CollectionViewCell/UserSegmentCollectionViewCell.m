//
//  UserSegmentCollectionViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-22.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "UserSegmentCollectionViewCell.h"

@implementation UserSegmentCollectionViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

-(IBAction)onChangeType:(UIButton *)btn{
    [self.picSmallBtn setSelected:NO];
    [self.picBigBtn setSelected:NO];
    [self.textBtn setSelected:NO];
    [btn setSelected:YES];
    [self.delegate onChangeType:(int)btn.tag];
}

@end
