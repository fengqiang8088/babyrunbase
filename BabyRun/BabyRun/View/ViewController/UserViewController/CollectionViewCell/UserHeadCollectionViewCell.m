//
//  UserHeadCollectionView.m
//  BabyRun
//
//  Created by fengqiang on 14-12-21.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "UserHeadCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "UserManager.h"

@implementation UserHeadCollectionViewCell

- (void)updateUI{
    
    //设置用户头像
    [self.image sd_setImageWithURL:[NSURL URLWithString:((AVFile *)self.user.user[@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
    
    //设置用户名称
    [self.nameLabel setText:self.user.user.username];
    
    //设置用户信息
    [self.addressLabel setText:self.user.user[@"address"]?:@""];
    
    //设置用户图片介绍内容
    [self.infoLabel setText:self.user.user[@"userIntroduction"]?:@""];
    
    [self.favListBtn setTitle:self.favsText?:@"0 关注" forState:UIControlStateNormal];
    
    [self.fansListBtn setTitle:self.fansText?:@"0 粉丝" forState:UIControlStateNormal];
    
    if (self.user.user[@"gender"]!=nil) {
        if ([self.user.user[@"gender"] integerValue] == 0) {
            [self.gender setImage:[UIImage imageNamed:@"man"]];
        }
        else{
            [self.gender setImage:[UIImage imageNamed:@"women"]];
        }
        [self.gender setHidden:NO];
    }
    
    if(self.user.relation == -1){
        [self.opBtn setHidden:YES];
    }
    else{
        [self.opBtn setHidden:NO];
        switch (self.user.relation) {
            case 0:
                [self.opBtn setImage:[UIImage imageNamed:@"fav_no"] forState:UIControlStateNormal];
                break;
            case 1:
                [self.opBtn setImage:[UIImage imageNamed:@"fav_yes"] forState:UIControlStateNormal];
                break;
            case 2:
                [self.opBtn setImage:[UIImage imageNamed:@"fav_too"] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
    }
}

-(IBAction)onOpBtn{
    if ([AVUser currentUser]!=nil) {
        if (self.user.relation == 0){
            //关注
            [[AVUser currentUser] follow:self.user.user.objectId andCallback:^(BOOL succeeded, NSError *error) {
                if(succeeded){
                    //查询用户之间的所属关系
                    AVQuery *queryfr = [AVUser currentUser].followerQuery;
                    [queryfr includeKey:@"follower"];
                    [queryfr whereKey:@"follower" equalTo:self.user.user];
                    if ([queryfr getFirstObject]) {
                        self.user.relation = 2;
                        [self.opBtn setImage:[UIImage imageNamed:@"fav_too"] forState:UIControlStateNormal];
                    }
                    else{
                        [self.opBtn setImage:[UIImage imageNamed:@"fav_yes"] forState:UIControlStateNormal];
                        self.user.relation = 1;
                    }
                    [TSMessage showNotificationWithTitle:@"关注成功" type:TSMessageNotificationTypeSuccess];
                    
                    //发送给指定对象通知
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                    [dic setObject:self.user.user.objectId forKey:@"objectId"];
                    [dic setObject:@"关注了你" forKey:@"content"];
                    [dic setObject:@"0" forKey:@"statusType"];
                    [dic setObject:@"0" forKey:@"messageType"];
                    [[UserManager shared] addUserMessage:dic success:^(id objects){
                        if ([objects[@"success"] boolValue]) {
                        }
                    }failure:^(NSError *error){
                        
                    }];
                    
                    //发送给全体粉丝对象通知
                    NSMutableDictionary *dicAll = [[NSMutableDictionary alloc] init];
                    NSString *content = [NSString stringWithFormat:@"关注了 %@",self.user.user.username];
                    [dicAll setObject:content forKey:@"content"];
                    [dicAll setObject:self.user.user.objectId forKey:@"targetId"];
                    [dicAll setObject:@"user" forKey:@"targetClass"];
                    [dicAll setObject:@"0" forKey:@"messageType"];
                    [dicAll setObject:@"" forKey:@"targetContent"];
                    
                    [[UserManager shared] pushMessageToFans:dicAll success:^(id objects){
                        if ([objects[@"success"] boolValue]) {
                            
                        }
                    }failure:^(NSError *error){
                        
                    }];
                }
                else{
                    [TSMessage showNotificationWithTitle:@"关注失败，请稍后再试" type:TSMessageNotificationTypeError];
                }
            }];
        }
        else{
            [[AVUser currentUser] unfollow:self.user.user.objectId andCallback:^(BOOL succeeded, NSError *error) {
                if(succeeded){
                    //查询用户之间的所属关系
                    [self.opBtn setImage:[UIImage imageNamed:@"fav_no"] forState:UIControlStateNormal];
                    self.user.relation = 0;
                    [TSMessage showNotificationWithTitle:@"取消关注" type:TSMessageNotificationTypeWarning];
                }
                else{
                    [TSMessage showNotificationWithTitle:@"取消关注失败，请稍后再试" type:TSMessageNotificationTypeError];
                }
            }];
        }
    }
    else{
        [self.delegate onLogin];
    }
}

@end
