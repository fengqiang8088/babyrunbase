//
//  UserPicSmallCollectionViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-22.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "ImgShared.h"

@interface UserPicSmallCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UIImageView *image;
@property (nonatomic,strong) ImgShared *imgShared;

- (void)updateUI;

@end
