//
//  UserSegmentCollectionViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-22.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

@protocol UserSegmentCollectionViewCellDelegate <NSObject>

@required
- (void)onChangeType:(int)type;
@end

@interface UserSegmentCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong) IBOutlet UIButton *picSmallBtn;
@property(nonatomic,strong) IBOutlet UIButton *picBigBtn;
@property(nonatomic,strong) IBOutlet UIButton *textBtn;

@property(nonatomic,weak) id<UserSegmentCollectionViewCellDelegate> delegate;

+ (NSString *)reuseIdentifier;

@end
