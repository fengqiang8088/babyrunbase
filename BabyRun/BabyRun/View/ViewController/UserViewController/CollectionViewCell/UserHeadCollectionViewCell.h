//
//  UserHeadCollectionView.h
//  BabyRun
//
//  Created by fengqiang on 14-12-21.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "ViewUser.h"

@protocol UserHeadCollectionViewCellDelegate <NSObject>

@required
- (void)onLogin;
@end

@interface UserHeadCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong) IBOutlet UIImageView *image;
@property(nonatomic,strong) IBOutlet UILabel *nameLabel;
@property(nonatomic,strong) IBOutlet UILabel *addressLabel;
@property(nonatomic,strong) IBOutlet UILabel *infoLabel;
@property(nonatomic,strong) IBOutlet UIButton *fansListBtn;
@property(nonatomic,strong) IBOutlet UIButton *favListBtn;
@property(nonatomic,strong) IBOutlet UIButton *opBtn;
@property(nonatomic,strong) IBOutlet UIImageView *gender;

@property (nonatomic,strong) ViewUser *user;
@property (nonatomic,strong) NSString *fansText;
@property (nonatomic,strong) NSString *favsText;
@property (nonatomic,weak) id<UserHeadCollectionViewCellDelegate> delegate;

- (void)updateUI;

@end
