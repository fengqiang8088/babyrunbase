//
//  UserFeedbackCollectionViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-24.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "UserFeedbackCollectionViewCell.h"
#import "UIImageView+WebCache.h"

@implementation UserFeedbackCollectionViewCell

- (void)updateUI{
    
    //设置用户发布图片
    [self.image sd_setImageWithURL:[NSURL URLWithString:self.person.imgUrl] placeholderImage:[UIImage imageNamed:@"picindex_ph"]];
}

-(void)updateSelect{
    [self.selectImage setHidden:NO];
}

@end
