//
//  RegIndexViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-4.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "RegIndexViewController.h"
#import <AVOSCloudSNS/AVOSCloudSNS.h>
#import <AVOSCloudSNS/AVUser+SNS.h>
#import "ThirdRegisterViewController.h"
#import "PublicManager.h"
#import "RegPhoneViewController.h"
#import <ShareSDK/ShareSDK.h>

@implementation RegIndexViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{

}

-(IBAction)onSinaLogin{
    [ShareSDK getUserInfoWithType:ShareTypeSinaWeibo authOptions:nil result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {
        if (result) {
            //按照AVOS的平台参数进行配置
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[userInfo uid] forKey:@"uid"];
            id<ISSPlatformCredential> credential = [userInfo credential];
            [dic setObject:[credential token] forKey:@"access_token"];
            NSDate *expriDate = [credential expired];
            [dic setObject:[NSString stringWithFormat:@"%f",[expriDate timeIntervalSinceNow]] forKey:@"expiration_in"];
            
            //用户信息配置
            NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
            [data setObject:[userInfo nickname] forKey:@"username"];
            [data setObject:[userInfo profileImage] forKey:@"avatar"];
            [self regSNSandLogin:dic platform:@"weibo" andSourceData:data];
        }
        else{
            [TSMessage showNotificationWithTitle:@"登录出现错误，请稍后重试" type:TSMessageNotificationTypeError];
        }
    }];
}

-(IBAction)onQQLogin{
    [ShareSDK getUserInfoWithType:ShareTypeQQSpace authOptions:nil result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {
        if (result) {
            //按照AVOS的平台参数进行配置
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[userInfo uid] forKey:@"openid"];
            id<ISSPlatformCredential> credential = [userInfo credential];
            [dic setObject:[credential token] forKey:@"access_token"];
            NSDate *expriDate = [credential expired];
            [dic setObject:[NSString stringWithFormat:@"%f",[expriDate timeIntervalSinceNow]] forKey:@"expires_in"];
            
            //用户信息配置
            NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
            [data setObject:[userInfo nickname] forKey:@"username"];
            [data setObject:[userInfo profileImage] forKey:@"avatar"];
            [self regSNSandLogin:dic platform:@"qq" andSourceData:data];
        }
        else{
            [TSMessage showNotificationWithTitle:@"登录出现错误，请稍后重试" type:TSMessageNotificationTypeError];
        }
    }];
}

//注册并登录
- (void)regSNSandLogin:(id)object platform:(NSString *)platform andSourceData:(id)data{
    [AVUser loginWithAuthData:object platform:platform block:^(AVUser *user, NSError *error) {
        //初次登录，进行相关数据绑定
        if(user&&user.username.length>20){
            //跳转完善信息
            ThirdRegisterViewController *viewController = [ThirdRegisterViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"ThirdRegisterViewController"];
            viewController.data = data;
            viewController.user = user;
            [self hideLoading];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        //已经是老用户，则进行登录
        else{
            [self hideLoading];
            if (self.fromType == UserLoginFromTypeIndex){
                UIViewController *tabBarController = [[PublicManager shared] setupViewControllers];
                [PublicManager shared].rootViewController = tabBarController;
                [self.navigationController pushViewController:tabBarController animated:YES];
            }
            else if (self.fromType == UserLoginFromTypeSecond){
                //发送通知,判断用户是登录还是注销以此更改关注的界面是否存在
                [[NSNotificationCenter defaultCenter] postNotificationName:BRNOTI_UPDATE_GZ object:nil];
                
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
    }];
}

-(IBAction)onRegPhone{
    RegPhoneViewController *viewController = [RegPhoneViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"RegPhoneViewController"];
    viewController.fromType = self.fromType;
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
