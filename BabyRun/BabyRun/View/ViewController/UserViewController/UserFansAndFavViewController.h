//
//  UserFansAndFavViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-12-23.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

@interface UserFansAndFavViewController : BaseViewController

@property (nonatomic,assign) int type;  //0:fans,1:fav
@property (nonatomic,strong) AVUser *user;

@end
