//
//  ForgetPassViewController.h
//  BabyRun
//
//  Created by fengqiang on 15-1-9.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPassViewController : BaseViewController

@property (nonatomic,assign) int fromType; //0:初始化进入,1:验证登录进入

@end
