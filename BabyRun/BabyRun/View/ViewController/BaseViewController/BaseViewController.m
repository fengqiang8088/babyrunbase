//
//  BaseViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-11-27.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "BaseViewController.h"
#import "BRShareView.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
}

- (void)didReceiveMemoryWarning {
}

+ (instancetype)brCreateFor:(NSString *)storyboardName andClassIdentifier:(NSString *)Identifier{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    id vc = [storyboard instantiateViewControllerWithIdentifier:Identifier];
    return vc;
}

-(void)loadData{};

-(void)setupBackBtn{
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(15, 26, 14, 24)];
    [backBtn setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    [self.view addSubview:backBtn];
    [backBtn addTarget:self action:@selector(onBack) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)onBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showLoading{
    UIWindow *topWindow = [[UIApplication sharedApplication] keyWindow];
    if (topWindow.windowLevel != UIWindowLevelNormal){
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(topWindow in windows){
            if (topWindow.windowLevel == UIWindowLevelNormal)
                break;
        }
    }
    UIView *rootView = [[topWindow subviews] objectAtIndex:0];
    
    rootView.userInteractionEnabled = NO;
    NSMutableArray *imageArray = [[NSMutableArray alloc] initWithCapacity:26];
    for (int i=0;i<26;i++) {
        [imageArray addObject:[UIImage imageNamed:[NSString stringWithFormat:@"JZDH_%d",i]]];
    }
    self.loadImageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2-50,SCREEN_HEIGHT/2-50,100,100)];
    [self.loadImageView setAnimationImages:imageArray];
    self.loadImageView.animationDuration = 1.0;
    [rootView addSubview:self.loadImageView];
    [self.loadImageView startAnimating];
}

-(void)hideLoading{
    UIWindow *topWindow = [[UIApplication sharedApplication] keyWindow];
    if (topWindow.windowLevel != UIWindowLevelNormal){
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(topWindow in windows){
            if (topWindow.windowLevel == UIWindowLevelNormal)
                break;
        }
    }
    UIView *rootView = [[topWindow subviews] objectAtIndex:0];
    
    rootView.userInteractionEnabled = YES;
    if (self.loadProgressView) {
        [self.loadProgressView removeFromSuperview];
    }
    [self.loadImageView removeFromSuperview];
}

-(void)showLoadProgressView{
    UIWindow *topWindow = [[UIApplication sharedApplication] keyWindow];
    if (topWindow.windowLevel != UIWindowLevelNormal){
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(topWindow in windows){
            if (topWindow.windowLevel == UIWindowLevelNormal)
                break;
        }
    }
    UIView *rootView = [[topWindow subviews] objectAtIndex:0];
    self.loadProgressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0,60,SCREEN_WIDTH,3)];
    [self.loadProgressView setProgressTintColor:[UIColor redColor]];
    [self.loadProgressView setProgressViewStyle:UIProgressViewStyleBar];
    [rootView addSubview:self.loadProgressView];
}

-(void)hideLoadProgressView:(BOOL)success{
    [self.loadProgressView removeFromSuperview];
    if (success) {
        [TSMessage showNotificationWithTitle:@"发布成功" type:TSMessageNotificationTypeSuccess];
    }
    else{
        [TSMessage showNotificationWithTitle:@"发布失败,请您稍后再试" type:TSMessageNotificationTypeError];
    }
}

- (void)setProgress:(float)value{
    [self.loadProgressView setProgress:value/100 animated:YES];
}

- (void)showShareView:(id)data{
    BRShareView *shareView = [[BRShareView alloc] init];
    shareView.data = data;
    [shareView showInView];
}

@end
