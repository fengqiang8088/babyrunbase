//
//  BaseViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-11-27.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property(nonatomic,assign) BOOL isLoadModel;
@property(nonatomic,assign) BOOL isFirstLoad;
@property(nonatomic,strong) UIImageView *loadImageView;
@property(nonatomic,strong) UIProgressView *loadProgressView;

+ (instancetype)brCreateFor:(NSString *)storyboardName andClassIdentifier:(NSString *)Identifier;

- (void)loadData;

- (void)setupBackBtn;

- (void)showLoading;
- (void)hideLoading;
- (void)showLoadProgressView;
- (void)hideLoadProgressView:(BOOL)success;
- (void)setProgress:(float)value;

- (void)showShareView:(id)data;

@end
