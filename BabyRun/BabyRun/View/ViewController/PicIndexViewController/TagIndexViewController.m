//
//  TagIndexViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-14.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "TagIndexViewController.h"
#import "AKSegmentedControl.h"
#import "PicIndexManager.h"
#import "PicIndexTableCell.h"
#import "PicTagCollectionViewCell.h"
#import "PicDetailViewController.h"
#import "CommentTableViewCell.h"
#import "BRTableView.h"
#import "BRCollectionView.h"
#import "HPGrowingTextView.h"
#import "UIView+BTPosition.h"
#import "IndexViewController.h"
#import "UserViewController.h"
#import "PublicManager.h"

@interface TagIndexViewController () <AKSegmentedControlDelegate,UITableViewDelegate,BRTableViewDelegate,UICollectionViewDelegate,BRCollectionViewDelegate,PicIndexTableCellDelegate,HPGrowingTextViewDelegate,UIActionSheetDelegate>

@property(nonatomic,strong) IBOutlet AKSegmentedControl *segmentedControl;
@property(nonatomic,strong) IBOutlet BRTableView *listView;
@property(nonatomic,strong) IBOutlet BRCollectionView *collectionView;
@property(nonatomic,strong) IBOutlet UIView *commentView;
@property(nonatomic,strong) IBOutlet UILabel *viewTitle;
@property(nonatomic,strong) IBOutlet UITableView *commentTableView;
@property(nonatomic,strong) IBOutlet HPGrowingTextView *inputView;
@property(nonatomic,strong) IBOutlet NSLayoutConstraint *inputViewSuperViewConstraintsHeight;
@property(nonatomic,strong) IBOutlet NSLayoutConstraint *inputViewBottomViewConstraints;

@property(nonatomic,strong) UIButton *collectionBtn;
@property(nonatomic,strong) UIButton *listBtn;
@property(nonatomic,strong) UIButton *commentBtn;

@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;
@property(nonatomic,strong) NSMutableArray *resultComment;
@property(nonatomic,strong) NSMutableDictionary *parmComment;
@property(nonatomic,assign) int selectType; //0:选择图片模式，1:选择评论模式

@property(nonatomic,strong) IBOutlet UIView *commentInputView;

@end

@implementation TagIndexViewController

@synthesize imgTag;
bool isReply;           //记录是否是回复
Comment *selectComment;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
    [self initSegmentBar];
    [self configInputView];
    [self loadData];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    [self.viewTitle setText:self.imgTag.tagText];
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.parm setObject:@"0" forKey:@"skip"];
    //取得对应标签的图片及评论
    [self.parm setObject:self.imgTag.tagId forKey:@"imgTagId"];
    
    self.selectType = 0;
    self.parmComment = [[NSMutableDictionary alloc] init];
    self.resultComment = [[NSMutableArray alloc] init];
    
    [self.listView setBRTableViewDelegate:self];
    [self.collectionView setBRCollectionViewDelegate:self];
}

-(void)initSegmentBar{
    [self.segmentedControl setDelegate:self];
    [self.segmentedControl setContentEdgeInsets:UIEdgeInsetsMake(2.0, 2.0, 3.0, 2.0)];
    [self.segmentedControl setSegmentedControlMode:AKSegmentedControlModeSticky];
    [self.segmentedControl setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin];
    
    // Button 1
    self.collectionBtn = [[UIButton alloc] init];
    self.collectionBtn.tag = 0;
    UIImage *collectionBtnImageNormal = [UIImage imageNamed:@"collection_normal"];
    UIImage *collectionBtnImageSelect = [UIImage imageNamed:@"collection_select"];
    [self.collectionBtn setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 5.0)];
    [self.collectionBtn setImage:collectionBtnImageNormal forState:UIControlStateNormal];
    [self.collectionBtn setImage:collectionBtnImageSelect forState:UIControlStateSelected];
    [self.collectionBtn addTarget:self action:@selector(onChange:) forControlEvents:UIControlEventTouchUpInside];
    
    // Button 2
    self.listBtn = [[UIButton alloc] init];
    self.listBtn.tag = 1;
    UIImage *listBtnImageNormal = [UIImage imageNamed:@"list_normal"];
    UIImage *listBtnImageSelect = [UIImage imageNamed:@"list_select"];
    [self.listBtn setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 5.0)];
    [self.listBtn setImage:listBtnImageNormal forState:UIControlStateNormal];
    [self.listBtn setImage:listBtnImageSelect forState:UIControlStateSelected];
    [self.listBtn addTarget:self action:@selector(onChange:) forControlEvents:UIControlEventTouchUpInside];
    
    // Button 3
    self.commentBtn = [[UIButton alloc] init];
    self.commentBtn.tag = 2;
    UIImage *commentBtnImageNormal = [UIImage imageNamed:@"comment_normal"];
    UIImage *commentBtnImageSelect = [UIImage imageNamed:@"comment_select"];
    [self.commentBtn setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 5.0)];
    [self.commentBtn setImage:commentBtnImageNormal forState:UIControlStateNormal];
    [self.commentBtn setImage:commentBtnImageSelect forState:UIControlStateSelected];
    [self.commentBtn addTarget:self action:@selector(onChange:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.segmentedControl setButtonsArray:@[self.collectionBtn, self.listBtn, self.commentBtn]];
}

- (void)configInputView {
    self.inputView.minNumberOfLines = 1;
    self.inputView.maxNumberOfLines = 3;
    self.inputView.returnKeyType = UIReturnKeyDefault;
    self.inputView.font = [UIFont fontWithName:@"HiraKakuProN-W3" size:12];
    self.inputView.textColor = BRGRAYColor;
    self.inputView.internalTextView.contentInset = UIEdgeInsetsMake(9, 0, 9, 0);
    self.inputView.internalTextView.backgroundColor = [UIColor whiteColor];
    [self.inputView.layer setCornerRadius:10];
    [self.inputView.internalTextView.layer setCornerRadius:10];
    self.inputView.delegate = self;
}

-(void)loadData{
    [self showLoading];
    [[PicIndexManager shared] getTagForPicList:self.parm
                                      success:^(id objects){
                                          self.result = objects;
                                          // 刷新表格
                                          [self.collectionView reloadData];
                                          [self.listView reloadData];
                                          [self hideLoading];
                                      }failure:^(NSError *error){
                                          [self hideLoading];
                                      }];
}

-(void)loadCommentData{
    [self showLoading];
    [self.parmComment setObject:self.imgTag.tagId forKey:@"tagId"];
    [[PicIndexManager shared] getTagCommentList:self.parmComment
                                      success:^(id objects){
                                          self.resultComment = objects;
                                          // 刷新表格
                                          [self.commentTableView reloadData];
                                          [self.inputView resignFirstResponder];
                                          [self hideLoading];
                                      }failure:^(NSError *error){
                                          [self hideLoading];
                                      }];
}

-(void)onChange:(id)sender{
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag) {
        case 0:{
            [self.collectionBtn setSelected:YES];
            [self.listBtn setSelected:NO];
            [self.commentBtn setSelected:NO];
        }
            break;
        case 1:{
            [self.collectionBtn setSelected:NO];
            [self.listBtn setSelected:YES];
            [self.commentBtn setSelected:NO];
        }
            break;
        case 2:{
            [self.collectionBtn setSelected:NO];
            [self.listBtn setSelected:NO];
            [self.commentBtn setSelected:YES];
        }
        default:
            break;
    }
}

#pragma mark AKSegmentedControlDelegate

- (void)segmentedViewController:(AKSegmentedControl *)segmentedControl touchedAtIndex:(NSUInteger)index{
    switch (index) {
        case 0:{
            [self.collectionView setHidden:NO];
            [self.listView setHidden:YES];
            [self.commentView setHidden:YES];
            self.selectType = 0;
        }
            break;
        case 1:{
            [self.collectionView setHidden:YES];
            [self.listView setHidden:NO];
            [self.commentView setHidden:YES];
            self.selectType = 0;
        }
            break;
        case 2:{
            [self.collectionView setHidden:YES];
            [self.listView setHidden:YES];
            [self.commentView setHidden:NO];
            self.selectType = 1;
            [self loadCommentData];
        }
            break;
        default:
            break;
    }
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.selectType == 0)
        return self.result.count;
    else
        return self.resultComment.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.selectType == 0)
        return 320;
    else
        return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectType == 0) {
        PicIndexTableCell *cell = [tableView dequeueReusableCellWithIdentifier:[PicIndexTableCell reuseIdentifier]];
        if (cell == nil) {
            cell = [[PicIndexTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[PicIndexTableCell reuseIdentifier]];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.imgShared = self.result[indexPath.row];
        [cell setDelegate:self];
        [cell updateUI];
        return cell;
    }
    else {
        CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentTableViewCell"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.comment = self.resultComment[indexPath.row];
        [cell.floor setText:[NSString stringWithFormat:@"%ld 楼",(long)indexPath.row]];
        [cell updateUI];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectComment = self.resultComment[indexPath.row];
    if ([selectComment.user.objectId isEqual:[AVUser currentUser].objectId]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil
                                      delegate:self
                                      cancelButtonTitle:@"取消"
                                      destructiveButtonTitle:@"删除"
                                      otherButtonTitles:nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        actionSheet.tag = 0;
        [actionSheet showInView:self.view];
    }
    else{
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil
                                      delegate:self
                                      cancelButtonTitle:@"取消"
                                      destructiveButtonTitle:@"举报"
                                      otherButtonTitles:nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        actionSheet.tag = 1;
        [actionSheet showInView:self.view];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        if(actionSheet.tag == 0){
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:selectComment.objectId forKeyedSubscript:@"objectId"];
            [dic setObject:self.imgTag.tagId forKeyedSubscript:@"sharedId"];
            //删除评论
            [[PicIndexManager shared] removeImgTagComment:dic success:^(id objects) {
                [TSMessage showNotificationWithTitle:@"删除成功" type:TSMessageNotificationTypeSuccess];
                [self loadCommentData];
            } failure:^(NSError *error) {
                
            }];
        }
        else if (actionSheet.tag ==1){
            //举报评论
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[NSNumber numberWithInt:4] forKey:@"reportType"];
            [dic setObject:selectComment.objectId forKey:@"commentId"];
            [dic setObject:self.imgTag.tagId forKey:@"tagId"];
            [[PublicManager shared] addReport:dic success:^(id objects) {
                [TSMessage showNotificationWithTitle:@"举报成功" type:TSMessageNotificationTypeSuccess];
            } failure:^(NSError *error) {
                [TSMessage showNotificationWithTitle:@"举报失败" type:TSMessageNotificationTypeError];
            }];
        }
    }
}

- (void)refreshTable:(BRTableView *)tableview{
    [self.parm setObject:@"0" forKey:@"skip"];
    [[PicIndexManager shared] getTagForPicList:self.parm
                                      success:^(id objects){
                                          [self.result removeAllObjects];
                                          self.result = objects;
                                          // 刷新表格
                                          [tableview reloadData];
                                          [tableview headerEndRefreshing];
                                      }failure:^(NSError *error){
                                      }];
}

- (void)loadMoreDataTable:(BRTableView *)tableview{
    [self.parm setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.result.count] forKey:@"skip"];
    [[PicIndexManager shared] getTagForPicList:self.parm
                                      success:^(id objects){
                                          for (NSDictionary *dict in objects) {
                                              [self.result addObject:dict];
                                          }
                                          [tableview reloadData];
                                          [tableview footerEndRefreshing];
                                      }failure:^(NSError *error){
                                          
                                      }];
}

#pragma mark -- UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.result.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"PicTagCollectionViewCell";
    PicTagCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.imgShared = self.result[indexPath.row];
    [cell updateUI];
    return cell;
}

#pragma mark --UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(105,100);
}

#pragma mark --UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell * cell = (UICollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
}

#pragma mark --BRCollectionView Delegate
- (void)refreshCollection:(BRCollectionView *)collectionView{
    [self.parm setObject:@"0" forKey:@"skip"];
    [[PicIndexManager shared] getTagForPicList:self.parm
                                      success:^(id objects){
                                          [self.result removeAllObjects];
                                          self.result = objects;
                                          // 刷新表格
                                          [self.collectionView reloadData];
                                          [self.collectionView headerEndRefreshing];
                                      }failure:^(NSError *error){
                                      }];
}

-(void)loadMoreDataCollection:(BRCollectionView *)collectionView{
    [self.parm setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.result.count] forKey:@"skip"];
    [[PicIndexManager shared] getTagForPicList:self.parm
                                      success:^(id objects){
                                          for (NSDictionary *dict in objects) {
                                              [self.result addObject:dict];
                                          }
                                          [self.collectionView reloadData];
                                          [self.collectionView footerEndRefreshing];
                                      }failure:^(NSError *error){
                                          
                                      }];
}

#pragma mark - PicIndexTableCellDelegate
-(void)selectTag:(ImgTag *)imgTagData{
    TagIndexViewController *tagIndexViewController = [TagIndexViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"TagIndexViewController"];
    tagIndexViewController.imgTag = imgTag;
    [self.navigationController pushViewController:tagIndexViewController animated:YES];
}

- (void)selectUser:(AVUser *)user{
    UserViewController *userViewController = [UserViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserStoryboard"];
    userViewController.user = user;
    userViewController.showTypeUser = 1;
    [self.navigationController pushViewController:userViewController animated:YES];
}

-(void)selectComment:(ImgShared *)imgShared{
    PicDetailViewController *picDetailViewController = [PicDetailViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"PicDetailViewController"];
    picDetailViewController.imgShared = imgShared;
    [self.navigationController pushViewController:picDetailViewController animated:YES];
}

-(void)onLogin{
    IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
    indexViewController.fromType = 1;
    UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:indexViewController];
    [self presentViewController:indexNavigationController animated:YES completion:nil];
}

-(void)selectMore:(ImgShared *)imgShared{
    if([AVUser currentUser]!=nil){
        [self showShareView:imgShared];
    }
    else{
        [self onLogin];
    }
}

-(IBAction)onSendMessage{
    NSString *str = [self.inputView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (!str || [str isEqualToString:@""]) {
        [self.inputView resignFirstResponder];
        return;
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:3];
    [dic setObject:self.imgTag.tagId forKey:@"tagId"];
    [dic setObject:str forKey:@"content"];
    [dic setObject:[AVUser currentUser].objectId forKey:@"userId"];
    [[PicIndexManager shared] addImgTagComment:dic
                                          success:^(id objects){
                                              [self loadCommentData];
                                              [self.inputView setText:@""];
                                              [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"恭喜", nil)
                                                                          subtitle:NSLocalizedString(@"评论提交成功", nil)
                                                                              type:TSMessageNotificationTypeSuccess];
                                              [self.commentTableView reloadData];
                                          }failure:^(NSError *error){
                                              
                                          }];
    
}

#pragma mark - HPGrowingTextViewDelegate

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height {
    float deltaHeight = height - growingTextView.frame.size.height;
    
    [self.inputViewSuperViewConstraintsHeight setConstant:(self.inputViewSuperViewConstraintsHeight.constant+deltaHeight)];
    [growingTextView.superview adjustFrameY:-deltaHeight];
    [self.commentTableView adjustFrameHeight:-deltaHeight];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if([AVUser currentUser]!=nil){
        [self.commentInputView setHidden:NO];
    }
    else{
        [self.commentInputView setHidden:YES];
    }

    [self registerKeyboardNotifications];
    isReply = NO;
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self unregisterKeyboardNotifications];
}

- (void)registerKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willResignActive:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)unregisterKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

#pragma mark - Keyboard

static CGRect viewFrameKeyboard = {{0,0},{0,0}};


- (void)keyboardWillShow:(NSNotification*)notification {
    if (!isReply){
        if([self.inputView.text isEqualToString:@""]){
            //self.inputView.textColor = COLOR_TEXT_BLACK;
        }
    }
    
    NSDictionary* info = [notification userInfo];
    CGRect originkeyboardRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardRect = [self.view convertRect:originkeyboardRect fromView:nil];
    
    self.commentTableView.contentOffset = CGPointMake(0, CGFLOAT_MAX) ;
    [self.inputViewBottomViewConstraints setConstant:keyboardRect.size.height];
}

- (void)keyboardWillHide:(NSNotification*)notification {
    isReply = NO;
    //self.inputView.textColor = COLOR_TEXT_GRAY;
    [self.inputViewBottomViewConstraints setConstant:0];
}

- (void)willResignActive:(NSNotification *)not {
    if(self.inputView.internalTextView.isFirstResponder){
        [UIView animateWithDuration:0.25
                              delay:0
                            options:7
                         animations:^{
                             self.view.frame = viewFrameKeyboard;
                         } completion:NULL];
    }
}

@end
