//
//  PicContentViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-7.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "PicContentViewController.h"
#import "PicIndexManager.h"
#import "PicIndexTableCell.h"
#import "BRTableView.h"
#import "TagIndexViewController.h"
#import "UIViewController+MaryPopin.h"
#import "UserViewController.h"
#import "PicDetailViewController.h"
#import "IndexViewController.h"
#import "PicIndexViewController.h"

@interface PicContentViewController ()<UITableViewDelegate,BRTableViewDelegate,PicIndexTableCellDelegate>

@property(nonatomic,strong) IBOutlet BRTableView *tableView;
@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;

@end

@implementation PicContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.tableView setBRTableViewDelegate:self];
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    self.isLoadModel = NO;
    if (self.isFirstLoad) {
        [self loadData];
    }
}

-(void)loadData{
    if(!self.isLoadModel){
        [self showLoading];
        [self.parm setObject:@"0" forKey:@"skip"];
        if(self.isFav){
            [self.parm setObject:@"isFav" forKey:@"isFav"];
            [[PicIndexManager shared] getPicIndexForUserList:self.parm
                                                     success:^(id objects){
                                                         self.result = objects;
                                                         // 刷新表格
                                                         [self.tableView reloadData];
                                                         self.isLoadModel = YES;
                                                         [self hideLoading];
                                                     }failure:^(NSError *error){
                                                         [self hideLoading];
                                                         [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                                                            subtitle:NSLocalizedString(@"网络连接错误，请您稍后再试", nil)
                                                                                                type:TSMessageNotificationTypeError];
                                                         
                                                     }];
        }
        else{
            if (self.imgGroup) {
                [self.parm setObject:self.imgGroup forKey:@"imgGroup"];
            }
            [[PicIndexManager shared] getPicIndexList:self.parm
                                              success:^(id objects){
                                                  self.result = objects;
                                                  // 刷新表格
                                                  [self.tableView reloadData];
                                                  self.isLoadModel = YES;
                                                  [self hideLoading];
                                              }failure:^(NSError *error){
                                                  [self hideLoading];
                                                  [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                                                     subtitle:NSLocalizedString(@"网络连接错误，请您稍后再试", nil)
                                                                                         type:TSMessageNotificationTypeError];
                                              }];
        }
    }
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.result.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 320;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PicIndexTableCell *cell = [tableView dequeueReusableCellWithIdentifier:[PicIndexTableCell reuseIdentifier]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.imgShared = self.result[indexPath.row];
    cell.delegate = self;
    [cell updateUI];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

- (void)refreshTable:(BRTableView *)tableview{
    [self.parm setObject:@"0" forKey:@"skip"];
    if(self.isFav){
        [[PicIndexManager shared] getPicIndexForUserList:self.parm
                                                 success:^(id objects){
                                                     [self.result removeAllObjects];
                                                     for (NSDictionary *dict in objects) {
                                                         [self.result addObject:dict];
                                                     }
                                                     // 刷新表格
                                                     [self.tableView reloadData];
                                                     [self.tableView headerEndRefreshing];
                                                 }failure:^(NSError *error){
                                                     self.isLoadModel = NO;
                                                     [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                                                        subtitle:NSLocalizedString(@"网络连接错误，请您稍后再试", nil)
                                                                                            type:TSMessageNotificationTypeError];
                                                     [self.tableView reloadData];
                                                     [self.tableView headerEndRefreshing];
                                                 }];
    }
    else{
        [[PicIndexManager shared] getPicIndexList:self.parm
                                          success:^(id objects){
                                              [self.result removeAllObjects];
                                              for (NSDictionary *dict in objects) {
                                                  [self.result addObject:dict];
                                              }
                                              // 刷新表格
                                              [self.tableView reloadData];
                                              [self.tableView headerEndRefreshing];
                                          }failure:^(NSError *error){
                                              self.isLoadModel = NO;
                                              [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                                                 subtitle:NSLocalizedString(@"网络连接错误，请您稍后再试", nil)
                                                                                     type:TSMessageNotificationTypeError];
                                              [self.tableView reloadData];
                                              [self.tableView headerEndRefreshing];
                                          }];
    }
}

- (void)loadMoreDataTable:(BRTableView *)tableview{
    [self.parm setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.result.count] forKey:@"skip"];
    if(self.isFav){
        [[PicIndexManager shared] getPicIndexForUserList:self.parm
                                                 success:^(id objects){
                                                     for (NSDictionary *dict in objects) {
                                                         [self.result addObject:dict];
                                                     }
                                                     // 刷新表格
                                                     [self.tableView reloadData];
                                                     [self.tableView footerEndRefreshing];
                                                 }failure:^(NSError *error){
                                                     [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                                                        subtitle:NSLocalizedString(@"网络连接错误，请您稍后再试", nil)
                                                                                            type:TSMessageNotificationTypeError];
                                                     [self.tableView reloadData];
                                                     [self.tableView footerEndRefreshing];
                                                 }];
    }
    else{
        [[PicIndexManager shared] getPicIndexList:self.parm
                                          success:^(id objects){
                                              for (NSDictionary *dict in objects) {
                                                  [self.result addObject:dict];
                                              }
                                              // 刷新表格
                                              [self.tableView reloadData];
                                              [self.tableView footerEndRefreshing];
                                          }failure:^(NSError *error){
                                              [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                                                 subtitle:NSLocalizedString(@"网络连接错误，请您稍后再试", nil)
                                                                                     type:TSMessageNotificationTypeError];
                                              [self.tableView reloadData];
                                              [self.tableView footerEndRefreshing];
                                          }];
    }
}


#pragma mark - PicIndexTableCellDelegate 
-(void)selectTag:(ImgTag *)imgTag{
    TagIndexViewController *tagIndexViewController = [TagIndexViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"TagIndexViewController"];
    tagIndexViewController.imgTag = imgTag;
    [self.navigationController pushViewController:tagIndexViewController animated:YES];
}

- (void)selectUser:(AVUser *)user{
    UserViewController *userViewController = [UserViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserStoryboard"];
    userViewController.user = user;
    userViewController.showTypeUser = 1;
    [self.navigationController pushViewController:userViewController animated:YES];
}

-(void)selectComment:(ImgShared *)imgShared{
    PicDetailViewController *picDetailViewController = [PicDetailViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"PicDetailViewController"];
    picDetailViewController.imgShared = imgShared;
    [self.navigationController pushViewController:picDetailViewController animated:YES];
}

-(void)onLogin{
    IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
    indexViewController.fromType = 1;
    UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:indexViewController];
    [self presentViewController:indexNavigationController animated:YES completion:nil];
}

-(void)selectMore:(ImgShared *)imgShared{
    if([AVUser currentUser]!=nil){
        [self showShareView:imgShared];
    }
    else{
        [self onLogin];
    }
}

@end
