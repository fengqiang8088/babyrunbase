//
//  TagIndexViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-12-14.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImgTag.h"

@interface TagIndexViewController : BaseViewController

@property (nonatomic,strong) ImgTag *imgTag;

@end
