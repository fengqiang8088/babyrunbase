//
//  PicContentViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-12-7.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//
#import "SCNavTabBarController.h"

@interface PicContentViewController : BaseViewController

@property (nonatomic,strong) NSNumber *imgGroup;
@property (nonatomic,strong) NSString *isFav;
@property (nonatomic,strong) SCNavTabBarController *navTabBarController;

@end
