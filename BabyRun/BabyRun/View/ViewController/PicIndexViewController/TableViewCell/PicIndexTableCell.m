//
//  PicIndexTableCell.m
//  BabyRun
//
//  Created by fengqiang on 14-11-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "PicIndexTableCell.h"
#import "BRTagButton.h"
#import "UIImageView+WebCache.h"
#import "UserViewController.h"
#import "PicIndexManager.h"
#import "PublicManager.h"
#import "IndexViewController.h"
#import "UserManager.h"

@implementation PicIndexTableCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)awakeFromNib {
    [super awakeFromNib];

    self.userHeader.userInteractionEnabled = YES;
    UITapGestureRecognizer *onUserTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onUser)];
    [self.userHeader addGestureRecognizer:onUserTap];
    
    UITapGestureRecognizer *onShowTagTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onShowTag)];
    UITapGestureRecognizer *onShowContentTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onShowTag)];
    [self.contentShowView addGestureRecognizer:onShowTagTap];
    [self.tagShowView addGestureRecognizer:onShowContentTap];
    
    [self.progressBarView setBarBackgroundColor:[UIColor whiteColor]];
    [self.progressBarView setBarFillColor:BRGRAY1Color];
    [self.progressBarView setBarBorderWidth:0];
}

- (void)updateUI{
    //判断相关所属关系
    if([AVUser currentUser] != nil){
        //查询是否是喜欢
        AVQuery *query1 = [AVQuery queryWithClassName:@"imgPraise"];
        [query1 whereKey:@"sharedId" equalTo:self.imgShared.objectId];
        [query1 whereKey:@"userId" equalTo:[AVUser currentUser].objectId];
        [query1 whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
        [query1 getFirstObjectInBackgroundWithBlock:^(AVObject *object, NSError *error) {
            if (object != nil) {
                self.imgShared.isLike = YES;
            }
            else{
                self.imgShared.isLike = NO;
            }
            //设置喜欢按钮
            [self updateLikeBtn];
        }];
    }

    //设置用户发布图片
    [self.userPic sd_setImageWithURL:[NSURL URLWithString:self.imgShared.imgUrl] placeholderImage:[UIImage imageNamed:@"picindex_ph"] options:SDWebImageLowPriority progress:^(NSInteger receivedSize,NSInteger expectedSize){
        [self.progressBarView setHidden:NO];
        double progressValue = (double)receivedSize/(double)expectedSize;
        [self.progressBarView setProgress:progressValue];
    }completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        [self.progressBarView setHidden:YES];
        self.imgShared.image = [self.userPic.image copy];
    }];
    
    //设置用户名称
    [self.userName setText:self.imgShared.user.username];
    
    //设置用户头像    
    [self.userHeader sd_setImageWithURL:[NSURL URLWithString:((AVFile *)self.imgShared.user[@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
    
    //设置用户信息
    NSString *userInfo = [NSString stringWithFormat:@"%@",self.imgShared.user[@"address"]?:@""];
    [self.userInfo setText:userInfo];
    
    //设置用户图片介绍内容
    NSString *userContext = [NSString stringWithFormat:@"%@",self.imgShared.imgText?:@""];
    [self.userContext setText:userContext];
    
    //已登录
    if ([AVUser currentUser] != nil) {
        //判断是不是自己
        if(![[AVUser currentUser].objectId isEqualToString:self.imgShared.user.objectId]){
            //未选择关注界面
            if(!self.isFav){
                [self.updateTime setHidden:YES];
                [self.opBtn setHidden:NO];
                //查询用户之间的所属关系
                AVQuery *queryfe = [AVUser currentUser].followeeQuery;
                [queryfe includeKey:@"followee"];
                [queryfe whereKey:@"followee" equalTo:self.imgShared.user];
                [queryfe getFirstObjectInBackgroundWithBlock:^(AVObject *object, NSError *error) {
                    if (object) {
                        self.imgShared.relation = 1;
                        AVQuery *queryfr = [AVUser currentUser].followerQuery;
                        [queryfr includeKey:@"follower"];
                        [queryfr whereKey:@"follower" equalTo:self.imgShared.user];
                        [queryfr getFirstObjectInBackgroundWithBlock:^(AVObject *object, NSError *error) {
                            if (object) {
                                self.imgShared.relation = 2;
                                [self updateRelationBtn];
                            }
                        }];
                    }
                    else{
                        self.imgShared.relation = 0;
                        [self updateRelationBtn];
                    }
                }];
            }
            //显示发布时间
            else if(self.isFav){
                [self.updateTime setHidden:NO];
                [self.opBtn setHidden:YES];
                [self.updateTime setText:[NSString stringWithFormat:@"%@",[[PublicManager shared] getTimeDiffString:[self.imgShared.updateTime timeIntervalSince1970]]]];
            }
        }
        else{
            [self.updateTime setHidden:NO];
            [self.opBtn setHidden:YES];
            [self.updateTime setText:[NSString stringWithFormat:@"%@",[[PublicManager shared] getTimeDiffString:[self.imgShared.updateTime timeIntervalSince1970]]]];
        }
    }
    //未登录
    else{
        [self.updateTime setHidden:YES];
        [self.opBtn setHidden:NO];
        [self.opBtn setImage:[UIImage imageNamed:@"PicIndexCell_unfav_btn"] forState:UIControlStateNormal];
    }
    
    //设置浏览数
    [self.readCount setText:[NSString stringWithFormat:@"阅读 %@",self.imgShared.readCount]];
    
    //设置评论按钮
    [[PicIndexManager shared] getImgSharedCommentCount:@{@"objectId":self.imgShared.objectId} success:^(id objects) {
        [self.commentBtn setTitle:[NSString stringWithFormat:@"%d",[objects integerValue]] forState:UIControlStateNormal];
    } failure:^(NSError *error) {
        [self.commentBtn setTitle:[NSString stringWithFormat:@"%@",self.imgShared.commentCount?:@"0"] forState:UIControlStateNormal];
    }];
    
    //设置标签信息
    [self.contentShowView setHidden:NO];
    [self.tagShowView setHidden:YES];
}

-(void)updateRelationBtn{
    switch (self.imgShared.relation) {
        case 0:
            [self.opBtn setImage:[UIImage imageNamed:@"PicIndexCell_unfav_btn"] forState:UIControlStateNormal];
            break;
        case 1:
            [self.opBtn setImage:[UIImage imageNamed:@"PicIndexCell_fav_btn"] forState:UIControlStateNormal];
            break;
        case 2:
            [self.opBtn setImage:[UIImage imageNamed:@"fav_too"] forState:UIControlStateNormal];
            break;
            
        default:
            break;
    }
}

-(IBAction)onLike{
    self.likeBtn.userInteractionEnabled = NO;
    if ([AVUser currentUser]!=nil) {
        //添加赞
        if (!self.imgShared.isLike) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:3];
            [dic setObject:self.imgShared.objectId forKey:@"sharedId"];
            [dic setObject:[AVUser currentUser].objectId forKey:@"userId"];
            [[PicIndexManager shared] addImgSharedLike:dic
                                               success:^(id objects){
                                                   if (objects!=nil) {
                                                       self.imgShared.isLike = YES;
                                                       self.imgShared.likeCount = [NSNumber numberWithInt:[self.imgShared.likeCount intValue]+1];
                                                       
                                                       //发送给特定对象通知
                                                       NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                                                       [dic setObject:self.imgShared.user.objectId forKey:@"objectId"];
                                                       [dic setObject:@"赞了你的图片" forKey:@"content"];
                                                       [dic setObject:@"1" forKey:@"statusType"];
                                                       [dic setObject:self.imgShared.objectId forKey:@"targetId"];
                                                       [dic setObject:@"imgShared" forKey:@"targetClass"];
                                                       [dic setObject:@"3" forKey:@"messageType"];
                                                       [dic setObject:self.imgShared.imgUrl forKey:@"targetContent"];
                                                       [dic setObject:self.imgShared.objectId forKey:@"targetId"];
                                                       
                                                       [[UserManager shared] addUserMessage:dic success:^(id objects){
                                                           if ([objects[@"success"] boolValue]) {
                                                           }
                                                       }failure:^(NSError *error){
                                                           
                                                       }];
                                                       
                                                       //发送给全体粉丝对象通知
                                                       NSMutableDictionary *dicAll = [[NSMutableDictionary alloc] init];
                                                       NSString *content = [NSString stringWithFormat:@"赞了 %@ 的图片",self.imgShared.user.username];
                                                       [dicAll setObject:content forKey:@"content"];
                                                       [dicAll setObject:self.imgShared.objectId forKey:@"targetId"];
                                                       [dicAll setObject:@"imgShared" forKey:@"targetClass"];
                                                       [dicAll setObject:@"3" forKey:@"messageType"];
                                                       [dicAll setObject:self.imgShared.imgUrl forKey:@"targetContent"];
                                                       [dicAll setObject:self.imgShared.objectId forKey:@"targetId"];
                                                       
                                                       [[UserManager shared] pushMessageToFans:dicAll success:^(id objects){
                                                           if ([objects[@"success"] boolValue]) {
                                                               
                                                           }
                                                       }failure:^(NSError *error){
                                                           
                                                       }];
                                                   }
                                                   [self updateLikeBtn];
                                                   if ([self.delegate respondsToSelector:@selector(selectLike:)]) {
                                                       [self.delegate selectLike:self.imgShared];
                                                   }
                                               }failure:^(NSError *error){
                                                   
                                               }];
        }
        //取消赞
        else{
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:3];
            [dic setObject:self.imgShared.objectId forKey:@"sharedId"];
            [dic setObject:[AVUser currentUser].objectId forKey:@"userId"];
            [[PicIndexManager shared] removeImgSharedLike:dic
                                                  success:^(id objects){
                                                      self.imgShared.isLike = NO;
                                                      self.imgShared.likeCount = [NSNumber numberWithInt:[self.imgShared.likeCount intValue]-1];
                                                      [self updateLikeBtn];
                                                      if ([self.delegate respondsToSelector:@selector(selectLike:)]) {
                                                          [self.delegate selectLike:self.imgShared];
                                                      }
                                                  }failure:^(NSError *error){
                                                      
                                                  }];
        }
    }
    else{
        [self.delegate onLogin];
    }
    
}

-(void)updateLikeBtn{
    if (self.imgShared.isLike) {
        [self.likeBtn setImage:[UIImage imageNamed:@"PicIndexCell_like_btn"] forState:UIControlStateNormal];
    }
    else{
        [self.likeBtn setImage:[UIImage imageNamed:@"PicIndexCell_unlike_btn"] forState:UIControlStateNormal];
    }
    [self.likeBtn setTitle:[NSString stringWithFormat:@"%@",self.imgShared.likeCount] forState:UIControlStateNormal];
    self.likeBtn.userInteractionEnabled = YES;
}

-(IBAction)onComment{
    [self.delegate selectComment:self.imgShared];
}

-(IBAction)onMore{
    [self.delegate selectMore:self.imgShared];
}

-(void)onShowTag{
    if (self.tagShowView.hidden) {
        for (UIView *view in [self.tagShowView subviews]) {
            [view removeFromSuperview];
        }
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:self.imgShared.objectId forKey:@"sharedId"];
        [[PicIndexManager shared] getPicTagList:dic
                                        success:^(id objects){
                                            for (ImgTag *tag in objects) {
                                                [self addTagButton:tag];
                                            }
                                          }failure:^(NSError *error){
                                          }];
        [self.tagShowView setHidden:NO];
        [self.contentShowView setHidden:YES];
    }
    else {
        [self.tagShowView setHidden:YES];
        [self.contentShowView setHidden:NO];
    }
}

-(void)addTagButton:(ImgTag *)imgTag{
    BRTagButton *tagBtn = [[BRTagButton alloc] initWithFrame:CGRectMake(imgTag.tagPosition.x*SCREEN_WIDTH+15, imgTag.tagPosition.y*SCREEN_WIDTH, 100, 26)];
    [tagBtn setTagText:imgTag.tagText andPosition:imgTag.tagOrientation andGroupId:[imgTag.tagGroup integerValue]];
    [tagBtn setImgTag:imgTag];
    [tagBtn addTarget:self action:@selector(toTagView:) forControlEvents:UIControlEventTouchUpInside];
    [self.tagShowView addSubview:tagBtn];
}

-(void)onUser{
    [self.delegate selectUser:self.imgShared.user];
}

-(void)toTagView:(BRTagButton *)btn{
    [self.delegate selectTag:btn.imgTag];
}

-(IBAction)onFav{
    if ([AVUser currentUser]!=nil) {
        if (self.imgShared.relation == 0){
            //关注
            [[AVUser currentUser] follow:self.imgShared.user.objectId andCallback:^(BOOL succeeded, NSError *error) {
                if(succeeded){
                    //查询用户之间的所属关系
                    AVQuery *queryfr = [AVUser currentUser].followerQuery;
                    [queryfr includeKey:@"follower"];
                    [queryfr whereKey:@"follower" equalTo:self.imgShared.user];
                    if ([queryfr getFirstObject]) {
                        self.imgShared.relation = 2;
                        [self.opBtn setImage:[UIImage imageNamed:@"fav_too"] forState:UIControlStateNormal];
                    }
                    else{
                        [self.opBtn setImage:[UIImage imageNamed:@"PicIndexCell_fav_btn"] forState:UIControlStateNormal];
                        self.imgShared.relation = 1;
                    }
                    [TSMessage showNotificationWithTitle:@"关注成功" type:TSMessageNotificationTypeSuccess];
                    
                    //发送给指定对象通知
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                    [dic setObject:self.imgShared.user.objectId forKey:@"objectId"];
                    [dic setObject:@"关注了你" forKey:@"content"];
                    [dic setObject:@"0" forKey:@"statusType"];
                    [dic setObject:@"0" forKey:@"messageType"];
                    [[UserManager shared] addUserMessage:dic success:^(id objects){
                        if ([objects[@"success"] boolValue]) {
                        }
                    }failure:^(NSError *error){
                        
                    }];
                    
                    //发送给全体粉丝对象通知
                    NSMutableDictionary *dicAll = [[NSMutableDictionary alloc] init];
                    NSString *content = [NSString stringWithFormat:@"关注了 %@",self.imgShared.user.username];
                    [dicAll setObject:content forKey:@"content"];
                    [dicAll setObject:self.imgShared.user.objectId forKey:@"targetId"];
                    [dicAll setObject:@"user" forKey:@"targetClass"];
                    [dicAll setObject:@"0" forKey:@"messageType"];
                    [dicAll setObject:@"" forKey:@"targetContent"];
                    
                    [[UserManager shared] pushMessageToFans:dicAll success:^(id objects){
                        if ([objects[@"success"] boolValue]) {
                            
                        }
                    }failure:^(NSError *error){
                        
                    }];
                }
                else{
                    [TSMessage showNotificationWithTitle:@"关注失败，请稍后再试" type:TSMessageNotificationTypeError];
                }
            }];
        }
        else{
            [[AVUser currentUser] unfollow:self.imgShared.user.objectId andCallback:^(BOOL succeeded, NSError *error) {
                if(succeeded){
                    //查询用户之间的所属关系
                    [self.opBtn setImage:[UIImage imageNamed:@"PicIndexCell_unfav_btn"] forState:UIControlStateNormal];
                    self.imgShared.relation = 0;
                    [TSMessage showNotificationWithTitle:@"取消关注" type:TSMessageNotificationTypeWarning];
                }
                else{
                    [TSMessage showNotificationWithTitle:@"取消关注失败，请稍后再试" type:TSMessageNotificationTypeError];
                }
            }];
        }
    }
    else{
        [self.delegate onLogin];
    }
}

@end
