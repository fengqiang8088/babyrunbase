//
//  PicTagCollectionViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-15.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImgShared.h"

@interface PicTagCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UIImageView *image;
@property (nonatomic,strong) ImgShared *imgShared;

- (void)updateUI;

@end
