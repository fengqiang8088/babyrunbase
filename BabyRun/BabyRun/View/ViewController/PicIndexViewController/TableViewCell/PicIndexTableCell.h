//
//  PicIndexTableCell.h
//  BabyRun
//
//  Created by fengqiang on 14-11-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "ImgShared.h"
#import "ImgTag.h"
#import "TYMProgressBarView.h"

@protocol PicIndexTableCellDelegate <NSObject>

@required
- (void)selectTag:(ImgTag *)imgTag;
- (void)selectUser:(AVUser *)user;
- (void)selectComment:(ImgShared *)imgShared;
- (void)selectMore:(ImgShared *)imgShared;
- (void)onLogin;

@optional
- (void)selectLike:(ImgShared *)imgShared;

@end

@interface PicIndexTableCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *userHeader;
@property (nonatomic,strong) IBOutlet UIImageView *userPic;
@property (nonatomic,strong) IBOutlet UILabel *userName;
@property (nonatomic,strong) IBOutlet UILabel *userInfo;
@property (nonatomic,strong) IBOutlet UILabel *userContext;
@property (nonatomic,strong) IBOutlet UILabel *updateTime;
@property (nonatomic,strong) IBOutlet UIButton *opBtn;
@property (nonatomic,strong) IBOutlet UILabel *readCount;
@property (nonatomic,strong) IBOutlet UIButton *likeBtn;
@property (nonatomic,strong) IBOutlet UIButton *commentBtn;
@property (nonatomic,weak) IBOutlet UIView *contentShowView;
@property (nonatomic,weak) IBOutlet UIView *tagShowView;
@property (nonatomic, strong) IBOutlet TYMProgressBarView *progressBarView;

@property (nonatomic,strong) ImgShared *imgShared;
@property (nonatomic,weak) id<PicIndexTableCellDelegate> delegate;
@property (nonatomic,assign) BOOL isFav;

+ (NSString *)reuseIdentifier;
- (void)updateUI;

@end
