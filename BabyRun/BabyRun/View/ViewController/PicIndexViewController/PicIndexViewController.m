//
//  PicIndexViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-11-27.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "PicIndexViewController.h"
#import "SCNavTabBarController.h"
#import "PicContentViewController.h"
#import "NotificationViewController.h"
#import "PicIndexManager.h"
#import "IndexViewController.h"
#import "UserManager.h"

@interface PicIndexViewController ()<SCNavTabBarControllerDelegate>

@property (nonatomic,strong) SCNavTabBarController *navTabBarController;

@end

@implementation PicIndexViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:NO animated:YES];
    [[UserManager shared] getUserNotReadMessageCount:^(NSInteger number) {
        [self.navTabBarController setNotiMessageCount:number>0?:0];
    } failure:^(NSError *error) {
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initSetup];
    [self initSegmentBar];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSegmentBar) name:BRNOTI_UPDATE_GZ object:nil];
}

-(void)initSegmentBar{
    
    self.navTabBarController = [[SCNavTabBarController alloc] init];
    
    [[PicIndexManager shared] getPicGroupList:nil success:^(id objects){
        //添加动态类别分类数据
        NSMutableArray *segmentArray = [[NSMutableArray alloc] init];
        
        //添加全部界面
        PicContentViewController *childViewController1 = [PicContentViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"PicContentViewController"];
        childViewController1.title = @"全部";
        childViewController1.navTabBarController = self.navTabBarController;
        [segmentArray addObject:childViewController1];
        
        //添加关注界面
        if([AVUser currentUser]!=nil){
            PicContentViewController *childViewController2 = [PicContentViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"PicContentViewController"];
            childViewController2.title = @"关注";
            childViewController2.navTabBarController = self.navTabBarController;
            childViewController2.isFav = @"YES";
            [segmentArray addObject:childViewController2];
        }
        
        for (AVObject *object in objects) {
            PicContentViewController *childViewController = [PicContentViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"PicContentViewController"];
            childViewController.title = object[@"imgGroupName"];
            childViewController.imgGroup = [NSNumber numberWithInteger:[object[@"imgGroup"] integerValue]];
            childViewController.navTabBarController = self.navTabBarController;
            [segmentArray addObject:childViewController];
        }
        if (segmentArray.count>0) {
            ((PicContentViewController *)segmentArray[0]).isFirstLoad = YES;
        }
        
        self.navTabBarController.scNavDelegate = self;
        self.navTabBarController.subViewControllers = segmentArray;
        self.navTabBarController.showArrowButton = YES;
        self.navTabBarController.scrollAnimation = YES;
        [self.navTabBarController addParentController:self];
        
    }failure:^(NSError *error){
            
    }];
}

-(void)updateSegmentBar{
    [self.navTabBarController removeFromParentViewController];
    [self.navTabBarController.view removeFromSuperview];
    [self initSegmentBar];
}

#pragma mark - SCNavTabBarControllerDelegate

-(void)scrollViewEnd:(NSInteger *)index{
    [(PicContentViewController *)self.navTabBarController.subViewControllers[*index] loadData];
}

-(void)arrowBtnClick{
    if([AVUser currentUser]!=nil){
        NotificationViewController *viewController = [NotificationViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"NotificationViewController"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else{
        IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
        indexViewController.fromType = 1;
        UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                       initWithRootViewController:indexViewController];
        [self presentViewController:indexNavigationController animated:YES completion:nil];
    }
}

@end
