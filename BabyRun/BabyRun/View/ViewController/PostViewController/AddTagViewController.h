//
//  AddTagViewController.h
//  BabyRun
//
//  Created by fengqiang on 15-1-3.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "BaseViewController.h"

@interface AddTagViewController : BaseViewController

@property (nonatomic,strong) UIImage *image;
@property (nonatomic,strong) NSString *taskId;

@end
