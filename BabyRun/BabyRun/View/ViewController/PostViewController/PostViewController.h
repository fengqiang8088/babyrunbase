//
//  PostViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-12-1.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ActionBlockData)(id data);

@interface PostViewController : BaseViewController

@property (nonatomic, copy) ActionBlockData finishBlock;

@end
