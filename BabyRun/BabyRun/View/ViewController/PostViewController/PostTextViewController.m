//
//  ImagePostViewController.m
//  BabyRun
//
//  Created by fengqiang on 15-1-3.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "PostTextViewController.h"
#import "IDScrollableTabBar.h"
#import "IDScrollableTabBarDelegate.h"
#import "CommunityIndexManager.h"
#import "PostManager.h"
#import "UIViewController+MaryPopin.h"
#import <CoreLocation/CoreLocation.h>

@interface PostTextViewController () <IDScrollableTabBarDelegate,UITextViewDelegate,CLLocationManagerDelegate>

@property (nonatomic,strong) IBOutlet UIView *contentView;
@property (nonatomic,strong) IBOutlet UIImageView *backgroundView;
@property (nonatomic,strong) IBOutlet UITextView *textView;
@property (nonatomic,strong) IBOutlet UILabel *textCountLabel;
@property (nonatomic,strong) IBOutlet UIScrollView *rootView;

@property (nonatomic,strong) IDScrollableTabBar *scrollableTabBar;
@property (nonatomic,strong) NSNumber *groupId;
@property (nonatomic,strong) NSString *textBackground;

@property (nonatomic, strong) CLLocationManager  *locationManager;

@end

@implementation PostTextViewController

-(void)viewDidAppear:(BOOL)animated{
    [self.rootView setContentSize:CGSizeMake(0, SCREEN_HEIGHT+100)];
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    [[self rdv_tabBarController] setTabBarHidden:NO animated:YES];
    [self updateUI];
    self.groupId = 0;
    
    /*
    self.locationManager = [[CLLocationManager alloc]init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.distanceFilter = 10;
    if (IOS8) {
        [_locationManager requestAlwaysAuthorization];//添加这句
    }
    [_locationManager startUpdatingLocation];
    */
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *currLocation = [locations lastObject];
    //NSLog(@"经度=%f 纬度=%f 高度=%f", currLocation.coordinate.latitude, currLocation.coordinate.longitude, currLocation.altitude);
    [_locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([error code] == kCLErrorDenied)
    {
        //访问被拒绝
    }
    if ([error code] == kCLErrorLocationUnknown) {
        //无法获取位置信息
    }
}

-(void)updateUI{
    
    [[CommunityIndexManager shared] getTextGroupList:nil success:^(id objects){
        NSMutableArray *segmentArray = [[NSMutableArray alloc] init];
        
        for (AVObject *object in objects) {
            IDItem *itemClock = [[IDItem alloc] initWithImage:nil text:object[@"textGroupName"]];
            itemClock.groupId = object[@"textGroup"];
            //itemClock.textBackground = object[@"textGroup"];
            [segmentArray addObject:itemClock];
        }
        
        self.scrollableTabBar = [[IDScrollableTabBar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0) itemWidth:60 items:segmentArray];
        self.scrollableTabBar.delegate = self;
        [self.scrollableTabBar setSelectedItem:0 animated:NO];
        [self.contentView addSubview:self.scrollableTabBar];
        
    }failure:^(NSError *error){
        
    }];
    
    //在弹出的键盘上面加一个view来放置退出键盘的Done按钮
    UIToolbar * topView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
    [topView setBarStyle:UIBarStyleDefault];
    UIBarButtonItem * btnSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc] initWithTitle:@"关闭" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyBoard)];
    NSArray * buttonsArray = [NSArray arrayWithObjects:btnSpace, doneButton, nil];
    [topView setItems:buttonsArray];
    [self.textView setInputAccessoryView:topView];
}

//关闭键盘
-(void) dismissKeyBoard{
    [self.textView resignFirstResponder];
}

-(void)scrollTabBarAction : (NSNumber *)selectedNumber sender:(id)sender{
    IDScrollableTabBarItem *item = [self.scrollableTabBar itemAtIndex:[selectedNumber intValue]];
    //设置用户发布图片
    NSString *textBackground = [NSString stringWithFormat:@"%d",(arc4random()%7)];
    UIImage *backgroundImage = [[UIImage imageNamed:textBackground] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0) resizingMode:UIImageResizingModeTile];
    [self.backgroundView setImage:backgroundImage];
    self.groupId = item.groupId;
    self.textBackground = textBackground;
}

-(IBAction)onPushTextShared{
    [self dismissKeyBoard];
    [self showLoading];
    if (self.textView.text == nil || [self.textView.text isEqualToString:@""]) {
        [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                    subtitle:NSLocalizedString(@"请输入内容", nil)
                                        type:TSMessageNotificationTypeError];
        return;
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:self.groupId forKey:@"groupId"];
    [dic setObject:[NSNumber numberWithInteger:[self.textBackground integerValue]] forKey:@"textBackground"];
    [dic setObject:self.textView.text forKey:@"text"];
    [dic setObject:[AVUser currentUser].objectId forKey:@"userid"];
    
    [[PostManager shared] addTextShared:dic success:^(id object){
        [self hideLoading];
        if(((AVObject *)object).objectId){
            [self dismissViewControllerAnimated:YES completion:nil];
            [self hideLoadProgressView:YES];
        }
        
    }failure:^(NSError *error){
        
    }];
}

-(IBAction)onDiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)textViewDidChange:(UITextView *)textView {
    if (textView.text.length>0) {
        NSInteger number = 500 - [textView.text length];
        [self.textCountLabel setText:[NSString stringWithFormat:@"%ld",(long)number]];
        if (number > 499) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"字符个数不能大于500" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            textView.text = [textView.text substringToIndex:499];
            number = 499;
        }
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"输入内容"]) {
        textView.text = @"";
    }
}

@end
