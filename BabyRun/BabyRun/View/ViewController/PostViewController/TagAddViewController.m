//
//  TagAddViewController.m
//  BabyRun
//
//  Created by fengqiang on 15-1-9.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "TagAddViewController.h"
#import "PostManager.h"
#import "TagTableViewCell.h"
#import "BRTableView.h"

@interface TagAddViewController ()<UITableViewDelegate,UISearchBarDelegate,BRTableViewDelegate>

@property(nonatomic,strong) IBOutlet BRTableView *tableView;
@property(nonatomic,strong) IBOutlet UISearchBar *searchBar;

@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;

@end

@implementation TagAddViewController

- (void)viewDidLoad {
    [self initSetup];
}

- (void)initSetup{
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.tableView setBRTableViewDelegate:self];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.searchBar becomeFirstResponder];
}

-(void)loadData{
    if (self.searchBar.text == nil || self.searchBar.text.length == 0) {
        return;
    }
    [self showLoading];
    [self.parm setObject:@"NO" forKey:@"loadMore"];
    [self.parm setObject:@"0" forKey:@"skip"];
    [self.parm setObject:self.searchBar.text forKey:@"tagName"];
    [self.parm setObject:self.tagGroup forKey:@"tagGroup"];
    [[PostManager shared] findTagList:self.parm
                               success:^(id objects){
                                   self.result = objects;
                                   // 刷新表格
                                   [self.tableView reloadData];
                                   self.isLoadModel = YES;
                                   [self hideLoading];
                               }failure:^(NSError *error){
                                   self.isLoadModel = NO;
                                   [self hideLoading];
                               }];
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.result.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TagTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[TagTableViewCell reuseIdentifier]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.tag = self.result[indexPath.row];
    [cell updateUI];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate selectTag:self.result[indexPath.row]];
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - UISearchBarDelegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self loadData];
    [searchBar resignFirstResponder];
}

-(IBAction)onMiss{
    [self.delegate selectTag:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)refreshTable:(BRTableView *)tableview{
    [self.parm setObject:@"0" forKey:@"skip"];
    [self.parm setObject:@"NO" forKey:@"loadMore"];
    [[PostManager shared] findTagList:self.parm
                              success:^(id objects){
                                  [self.result removeAllObjects];
                                  self.result = objects;
                                  // 刷新表格
                                  [self.tableView reloadData];
                                  [self.tableView headerEndRefreshing];
                              }failure:^(NSError *error){
                                  [self.tableView headerEndRefreshing];
                              }];
}

- (void)loadMoreDataTable:(BRTableView *)tableview{
    [self.parm setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.result.count] forKey:@"skip"];
    [self.parm setObject:@"YES" forKey:@"loadMore"];
    [[PostManager shared] findTagList:self.parm
                              success:^(id objects){
                                  for (NSDictionary *dict in objects) {
                                      [self.result addObject:dict];
                                  }
                                  // 刷新表格
                                  [self.tableView reloadData];
                                  [self.tableView footerEndRefreshing];
                              }failure:^(NSError *error){
                                  [self.tableView footerEndRefreshing];
                              }];
}

@end
