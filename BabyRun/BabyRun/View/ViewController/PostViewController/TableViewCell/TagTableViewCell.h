//
//  TagTableViewCell.h
//  BabyRun
//
//  Created by fengqiang on 15-1-9.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "ImgTag.h"

@interface TagTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *tagIcon;
@property (nonatomic,strong) IBOutlet UILabel *tagName;
@property (nonatomic,strong) IBOutlet NSLayoutConstraint *tagNameLeftConstraint;

@property (nonatomic,strong) ImgTag *tag;

+ (NSString *)reuseIdentifier;
-(void)updateUI;

@end
