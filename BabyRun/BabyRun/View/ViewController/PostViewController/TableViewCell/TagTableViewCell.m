//
//  TagTableViewCell.m
//  BabyRun
//
//  Created by fengqiang on 15-1-9.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "TagTableViewCell.h"

@implementation TagTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)updateUI{
    //设置用户名称
    [self.tagName setText:self.tag.tagText];
    [self.tagNameLeftConstraint setConstant:61];
    
    switch ([self.tag.tagGroup intValue]) {
        case 0:
        {
            [self.tagIcon setImage:[UIImage imageNamed:@"tag_item_person"]];
        }
            break;
        case 1:
        {
            [self.tagIcon setImage:[UIImage imageNamed:@"add_task_icon"]];
        }
            break;
        case 2:
        {
            [self.tagIcon setImage:[UIImage imageNamed:@"tag_item_stuff"]];
        }
            break;
        case 3:
        {
            [self.tagIcon setImage:[UIImage imageNamed:@"tag_item_info"]];
        }
            break;
        case 4:
        {
            [self.tagIcon setImage:[UIImage imageNamed:@"add_local_icon"]];
        }
            break;
        case 999:
        {
            [self.tagNameLeftConstraint setConstant:10];
            [self.tagIcon setImage:[UIImage imageNamed:@""]];
        }
            break;
        case -1:
        {
            [self.tagIcon setImage:[UIImage imageNamed:@"add_tag_icon"]];
        }
            break;
            
        default:
        {
            
        }
            break;
    }
}

@end
