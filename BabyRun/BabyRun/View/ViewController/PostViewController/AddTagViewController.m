//
//  AddTagViewController.m
//  BabyRun
//
//  Created by fengqiang on 15-1-3.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "AddTagViewController.h"
#import "AwesomeMenu.h"
#import "UIViewController+MaryPopin.h"
#import "PostManager.h"
#import "BREditTagButton.h"
#import "TagAddViewController.h"

@interface AddTagViewController () <AwesomeMenuDelegate,UIAlertViewDelegate,TagAddTableDelegate,BREditTagButtonDelegate>

@property (nonatomic,strong) IBOutlet UIImageView *imageView;

@property (nonatomic,strong) IBOutlet UIView *postImageView;
@property (nonatomic,strong) IBOutlet UITextView *postText;

@property(nonatomic,strong) AwesomeMenu *tagMenu;
@property(nonatomic,assign) BOOL showPostView;
@property(nonatomic,strong) NSMutableArray *tagArray;
@property(nonatomic,strong) NSMutableArray *tagGroupArray;

@end

@implementation AddTagViewController

CGPoint touchPoint;
NSInteger groupId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
    [self initTagMenu];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    [[self rdv_tabBarController] setTabBarHidden:NO animated:YES];
    [self.imageView setImage:self.image];
    self.tagArray = [[NSMutableArray alloc] initWithCapacity:0];
    self.tagGroupArray = [[NSMutableArray alloc] initWithCapacity:0];
}

- (void)initTagMenu{
    CGFloat menuWidth = self.imageView.frame.size.width;
    CGFloat menuHeight = self.imageView.frame.size.height;
    
    UIImage *startBg = [UIImage imageNamed:@"tag_start_bg"];
    UIImage *start = [UIImage imageNamed:@"tag_start"];
    
    AwesomeMenuItem *startItem = [[AwesomeMenuItem alloc] initWithImage:startBg
                                                       highlightedImage:startBg
                                                           ContentImage:start
                                                highlightedContentImage:start];
    
    self.tagMenu = [[AwesomeMenu alloc] initWithFrame:(CGRect) {
        { 0, -20 },{ menuWidth, menuHeight }
    } startItem:startItem optionMenus:[self setupTagMenu]];
    self.tagMenu.delegate = self;
    self.tagMenu.endRadius = 80;
    self.tagMenu.nearRadius = 70;
    self.tagMenu.farRadius = 90;
    self.tagMenu.closeRotation = 0;
    self.tagMenu.expandRotation = 0;
    self.tagMenu.rotateAddButton = NO;
    self.tagMenu.hidden = YES;
    
    [self.view addSubview:self.tagMenu];
}

- (void)onImage{
    [self showTagArray:NO];
    if (self.tagMenu.hidden) {
        self.tagMenu.hidden = NO;
        self.tagMenu.alpha = 0.3;
        [UIView animateWithDuration:0.20
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.tagMenu.alpha = 1.0;
                         }
                         completion:^(BOOL finished) {
                             self.tagMenu.expanding = YES;
                         }];
    }
}

- (NSArray *)setupTagMenu {
    
    UIImage *itemBg = [UIImage imageNamed:@"tag_item_bg"];
    UIImage *item1 = [UIImage imageNamed:@"tag_item_person"];
    UIImage *item2 = [UIImage imageNamed:@"tag_item_do"];
    UIImage *item3 = [UIImage imageNamed:@"tag_item_stuff"];
    UIImage *item4 = [UIImage imageNamed:@"tag_item_info"];
    UIImage *item5 = [UIImage imageNamed:@"tag_item_local"];
    UIImage *item6 = [UIImage imageNamed:@"tag_item_other"];
    
    AwesomeMenuItem *menuItem1 = [[AwesomeMenuItem alloc] initWithImage:itemBg
                                                       highlightedImage:itemBg
                                                           ContentImage:item1
                                                highlightedContentImage:nil];
    AwesomeMenuItem *menuItem2 = [[AwesomeMenuItem alloc] initWithImage:itemBg
                                                       highlightedImage:itemBg
                                                           ContentImage:item2
                                                highlightedContentImage:nil];
    AwesomeMenuItem *menuItem3 = [[AwesomeMenuItem alloc] initWithImage:itemBg
                                                       highlightedImage:itemBg
                                                           ContentImage:item3
                                                highlightedContentImage:nil];
    AwesomeMenuItem *menuItem4 = [[AwesomeMenuItem alloc] initWithImage:itemBg
                                                       highlightedImage:itemBg
                                                           ContentImage:item4
                                                highlightedContentImage:nil];
    AwesomeMenuItem *menuItem5 = [[AwesomeMenuItem alloc] initWithImage:itemBg
                                                       highlightedImage:itemBg
                                                           ContentImage:item5
                                                highlightedContentImage:nil];
    AwesomeMenuItem *menuItem6 = [[AwesomeMenuItem alloc] initWithImage:itemBg
                                                       highlightedImage:itemBg
                                                           ContentImage:item6
                                                highlightedContentImage:nil];
    
    return @[ menuItem1, menuItem2, menuItem3, menuItem4, menuItem5, menuItem6 ];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    /*
    if (buttonIndex == 1) {
        NSString *str = [alertView textFieldAtIndex:0].text;
        
        BREditTagButton *tagBtn = [[BREditTagButton alloc] initWithFrame:CGRectMake(touchPoint.x+15, touchPoint.y, 100, 26)];
        [tagBtn setTagText:str andPosition:0 andGroupId:groupId];
        [self.imageView addSubview:tagBtn];
        [self.tagArray addObject:tagBtn];
    }
    [self showTagArray:YES];
    */
}

- (void)awesomeMenu:(AwesomeMenu *)menu didSelectIndex:(NSInteger)idx {
    //NSLog(@"Select the index : %ld", (long)idx);
    groupId = idx;
    
    TagAddViewController *viewController = [TagAddViewController brCreateFor:@"PostStoryboard" andClassIdentifier:@"TagAddViewController"];
    viewController.tagGroup = [NSNumber numberWithInteger:groupId];
    viewController.delegate = self;
    [self presentViewController:viewController animated:NO completion:nil];
    /*
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请输入标签内容" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [[alertView textFieldAtIndex:0] setPlaceholder:@"ceshi"];
    [alertView show];
    */
    
    self.tagMenu.alpha = 1.0;
    [UIView animateWithDuration:0.35
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.tagMenu.alpha = 0.0;
                         //NSLog(@"out animate start");
                     }
                     completion:^(BOOL finished) {
                         //NSLog(@"out animate completion");
                         self.tagMenu.hidden = YES;
                     }];
}

-(void)selectTag:(ImgTag *)imgTag{
    if (imgTag) {
        BREditTagButton *tagBtn = [[BREditTagButton alloc] initWithFrame:CGRectMake(touchPoint.x+15, touchPoint.y, 100, 26)];
        [tagBtn setTagText:imgTag.tagText andPosition:0 andGroupId:[imgTag.tagGroup integerValue]!=-1?:999];
        tagBtn.delegate = self;
        [self.imageView addSubview:tagBtn];
        [self.tagArray addObject:tagBtn];
        [self.tagGroupArray addObject:imgTag];
    }
    [self showTagArray:YES];
}

-(void)longPress:(BREditTagButton *)btn{
    [self.tagArray removeObject:btn];
    [btn removeFromSuperview];
}

- (void)awesomeMenuDidFinishAnimationClose:(AwesomeMenu *)menu {
    //NSLog(@"Menu was closed!");
    
    self.tagMenu.alpha = 1.0;
    [UIView animateWithDuration:0.20
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.tagMenu.alpha = 0.0;
                         //NSLog(@"out animate start");
                     }
                     completion:^(BOOL finished) {
                         //NSLog(@"out animate completion");
                         self.tagMenu.hidden = YES;
                         [self showTagArray:YES];
                     }];
}
- (void)awesomeMenuDidFinishAnimationOpen:(AwesomeMenu *)menu {
    //NSLog(@"Menu is open!");
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (!self.showPostView) {
        UITouch *touch = [[event touchesForView:self.imageView] anyObject];
        CGPoint location = [touch locationInView:self.imageView];
        if (location.x!=0&&location.y!=0) {
            touchPoint = location;
            [self onImage];
        }
    }
}

-(IBAction)onDone{
    self.showPostView = YES;
    [self.postImageView setHidden:NO];
}

-(IBAction)onMissPostView{
    self.showPostView = NO;
    [self.postImageView setHidden:YES];
}

-(IBAction)onPushImage{
    //添加新增的标签类型
    for (ImgTag *tag in self.tagGroupArray) {
        if (tag.tagId == nil) {
            [[PostManager shared] addTag:tag success:^(id objects) {
            } failure:^(NSError *error) {
            }];
        }
    }
    [self dismissViewControllerAnimated:YES completion:^{
        [self showLoadProgressView];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:self.postText.text forKey:@"sharedTitle"];
        [dic setObject:[AVUser currentUser].objectId forKey:@"userId"];
        [dic setObject:self.tagArray forKey:@"tagArray"];
        if (self.taskId) {
            [dic setObject:self.taskId forKey:@"taskId"];
        }
        
        //更改图片大小
        if (self.image.size.width>640) {
            self.image = [self scaleToSize:self.image size:CGSizeMake(640, 640*self.image.size.width/self.image.size.height)];
        }
        //进行图片压缩
        NSData *imageData = UIImageJPEGRepresentation(self.image,0.75);
        AVFile *imageFile = [AVFile fileWithName:@"postUserImgShared.png" data:imageData];
        
        [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
            if (succeeded) {
                [dic setObject:imageFile.url forKey:@"imgUrl"];
                [[PostManager shared] addImgShared:dic success:^(AVObject *object){
                    if (object.objectId) {
                        [self hideLoadProgressView:YES];
                    }
                    else{
                        [self hideLoadProgressView:NO];
                    }
                }failure:^(NSError *error){
                    [self hideLoadProgressView:NO];
                }];
            }
            else{
                
            }
        }progressBlock:^(NSInteger percentDone){
            [self setProgress:percentDone];
        }];
    }];
}

-(IBAction)onClose{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)showTagArray:(BOOL)show{
    if (!show) {
        for (BREditTagButton *btn in self.tagArray) {
            [btn setHidden:YES];
        }
    }
    else{
        for (BREditTagButton *btn in self.tagArray) {
            [btn setHidden:NO];
        }
    }
}

- (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size{
 // 创建一个bitmap的context
 // 并把它设置成为当前正在使用的context
 UIGraphicsBeginImageContext(size);
 // 绘制改变大小的图片
 [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
 // 从当前context中创建一个改变大小后的图片
 UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
 // 使当前的context出堆栈
 UIGraphicsEndImageContext();
 // 返回新的改变大小后的图片
 return scaledImage;
}

@end
