//
//  PostViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-1.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "PostViewController.h"
#import "UIViewController+MaryPopin.h"
#import "PostTextViewController.h"
#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraLibraryViewController.h"
#import "AddTagViewController.h"
#import "PublicManager.h"

@interface PostViewController () <DBCameraViewControllerDelegate,DBCameraContainerDelegate>

@end

@implementation PostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    [[self rdv_tabBarController] setTabBarHidden:NO animated:YES];
}

-(IBAction)onPostPhoto{
    DBCameraViewController *cameraController =
    [DBCameraViewController initWithDelegate:self];
    [cameraController setForceQuadCrop:YES];
    
    DBCameraContainerViewController *container =
    [[DBCameraContainerViewController alloc] initWithDelegate:self];
    [container setCameraViewController:cameraController];
    [container setFullScreenMode];
    
    UINavigationController *nav =
    [[UINavigationController alloc] initWithRootViewController:container];
    [nav setNavigationBarHidden:YES];
    [self presentViewController:nav animated:YES completion:nil];
}

-(IBAction)onPostLibary{
    DBCameraLibraryViewController *libViewController = [[DBCameraLibraryViewController alloc] init];
    [libViewController setDelegate:self];
    [libViewController setContainerDelegate:self];
    libViewController.useCameraSegue = YES;
    
    UINavigationController *nav =
    [[UINavigationController alloc] initWithRootViewController:libViewController];
    [nav setNavigationBarHidden:YES];
    [self presentViewController:nav animated:YES completion:nil];
}

-(IBAction)onPostText{
    [self.presentingPopinViewController dismissCurrentPopinControllerAnimated:YES completion:^{
        PostTextViewController *viewController = [PostTextViewController brCreateFor:@"PostStoryboard" andClassIdentifier:@"PostTextViewController"];
        if (self.finishBlock) {
            self.finishBlock(viewController);
        }
    }];
}

- (IBAction)diss{
    [self.presentingPopinViewController dismissCurrentPopinControllerAnimated:YES completion:^{
    }];
}

- (void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata {
    //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    [cameraViewController restoreFullScreenMode];
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        [self.presentingPopinViewController dismissCurrentPopinControllerAnimated:NO completion:^{
            AddTagViewController *viewController = [AddTagViewController brCreateFor:@"PostStoryboard" andClassIdentifier:@"AddTagViewController"];
            viewController.image = image;
            if (self.finishBlock) {
                self.finishBlock(viewController);
            }
        }];
    }];
}

- (void)dismissCamera:(id)cameraViewController {
    [cameraViewController restoreFullScreenMode];
    [self.presentingPopinViewController dismissCurrentPopinControllerAnimated:YES completion:^{
    }];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)backFromController:(id)fromController{
    [fromController dismissViewControllerAnimated:YES completion:nil];
    [self.presentingPopinViewController dismissCurrentPopinControllerAnimated:YES completion:nil];
}

-(void)switchFromController:(id)fromController toController:(id)controller{
    
}

@end
