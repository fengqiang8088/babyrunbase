//
//  TagAddViewController.h
//  BabyRun
//
//  Created by fengqiang on 15-1-9.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//
#import "ImgTag.h"

@protocol TagAddTableDelegate <NSObject>

@required
- (void)selectTag:(ImgTag *)imgTag;
@end

@interface TagAddViewController : BaseViewController

@property(nonatomic,assign) NSNumber *tagGroup;
@property (nonatomic,weak) id<TagAddTableDelegate> delegate;

@end
