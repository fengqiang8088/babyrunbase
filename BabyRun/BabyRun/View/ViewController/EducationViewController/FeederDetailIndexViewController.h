//
//  FeederDetailIndexViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-12-17.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//
#import "Feeder.h"

@interface FeederDetailIndexViewController : BaseViewController

@property(nonatomic,strong) Feeder *feeder;

@end
