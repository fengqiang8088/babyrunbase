//
//  EducationViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-1.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "EducationViewController.h"
#import "SCNavTabBarController.h"
#import "FeederIndexViewController.h"
#import "ActivityIndexViewController.h"
#import "NotificationViewController.h"
#import "IndexViewController.h"
#import "UserManager.h"

@interface EducationViewController () <SCNavTabBarControllerDelegate>

@property (nonatomic,strong) SCNavTabBarController *navTabBarController;

@end


@implementation EducationViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:NO animated:YES];
    [[UserManager shared] getUserNotReadMessageCount:^(NSInteger number) {
        [self.navTabBarController setNotiMessageCount:number>0?:0];
    } failure:^(NSError *error) {
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initSetup];
    [self initSegmentBar];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)initSegmentBar{
    
    FeederIndexViewController *childViewController1 = [FeederIndexViewController brCreateFor:@"EducationStoryboard" andClassIdentifier:@"FeederIndexViewController"];
    childViewController1.title = @"        奶瓶志     ";
    childViewController1.isFirstLoad = YES;
    
    ActivityIndexViewController *childViewController2 = [ActivityIndexViewController brCreateFor:@"EducationStoryboard" andClassIdentifier:@"ActivityIndexViewController"];
    childViewController2.title = @"     活动秀        ";
    childViewController2.isFirstLoad = YES;
    
    self.navTabBarController = [[SCNavTabBarController alloc] init];
    self.navTabBarController.scNavDelegate = self;
    self.navTabBarController.subViewControllers = @[childViewController1, childViewController2];
    self.navTabBarController.showArrowButton = YES;
    self.navTabBarController.scrollAnimation = YES;
    [self.navTabBarController addParentController:self];
}

-(void)scrollViewEnd:(NSInteger *)index{
    [(BaseViewController *)self.navTabBarController.subViewControllers[*index] loadData];
}

-(void)arrowBtnClick{
    if([AVUser currentUser]!=nil){
        NotificationViewController *viewController = [NotificationViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"NotificationViewController"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else{
        IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
        indexViewController.fromType = 1;
        UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                       initWithRootViewController:indexViewController];
        [self presentViewController:indexNavigationController animated:YES completion:nil];
    }
}

@end
