//
//  ActivityJoinListViewController.h
//  BabyRun
//
//  Created by fengqiang on 15-1-8.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "Activity.h"

@interface ActivityJoinListViewController : BaseViewController

@property (nonatomic,strong) Activity *activity;

@end
