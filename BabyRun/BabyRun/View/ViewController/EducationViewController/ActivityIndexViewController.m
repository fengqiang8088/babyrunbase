//
//  ActivityIndexViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "ActivityIndexViewController.h"

#import "BRTableView.h"
#import "EducationIndexManager.h"
#import "ActivityTableViewCell.h"
#import "ActivityDetailViewController.h"


@interface ActivityIndexViewController ()<UITableViewDelegate,BRTableViewDelegate>

@property(nonatomic,strong) IBOutlet BRTableView *tableView;
@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;

@end

@implementation ActivityIndexViewController

-(void)loadData{
    if (!self.isLoadModel) {
        [self.parm setObject:@"0" forKey:@"skip"];
        [[EducationIndexManager shared] getActivityIndexList:self.parm
                                                     success:^(id objects){
                                                         self.result = objects;
                                                         // 刷新表格
                                                         [self.tableView reloadData];
                                                         [self.tableView headerEndRefreshing];
                                                         self.isLoadModel = YES;
                                                     }failure:^(NSError *error){
                                                         self.isLoadModel = NO;
                                                     }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.tableView setBRTableViewDelegate:self];
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    if (self.isFirstLoad) {
        [self loadData];
    }
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.result.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 108;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ActivityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ActivityTableViewCell reuseIdentifier]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.data = self.result[indexPath.row];
    [cell updateUI];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ActivityDetailViewController *viewController = [ActivityDetailViewController brCreateFor:@"EducationStoryboard" andClassIdentifier:@"ActivityDetailViewController"];
    viewController.activity = self.result[indexPath.row];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)refreshTable:(BRTableView *)tableview{
    [self.parm setObject:@"0" forKey:@"skip"];
    [[EducationIndexManager shared] getActivityIndexList:self.parm
                                               success:^(id objects){
                                                   [self.result removeAllObjects];
                                                   self.result = objects;
                                                   // 刷新表格
                                                   [tableview reloadData];
                                                   [tableview headerEndRefreshing];
                                               }failure:^(NSError *error){
                                                   [tableview headerEndRefreshing];
                                               }];
}

- (void)loadMoreDataTable:(BRTableView *)tableview{
    [self.parm setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.result.count] forKey:@"skip"];
    [[EducationIndexManager shared] getActivityIndexList:self.parm
                                               success:^(id objects){
                                                   for (NSDictionary *dict in objects) {
                                                       [self.result addObject:dict];
                                                   }
                                                   [tableview reloadData];
                                                   [tableview footerEndRefreshing];
                                               }failure:^(NSError *error){
                                                   [tableview footerEndRefreshing];
                                               }];
}

@end
