//
//  ActivityDetailViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-17.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "ActivityDetailViewController.h"
#import "EducationIndexManager.h"
#import "BRCollectionView.h"
#import "EdHeaderCollectionViewCell.h"
#import "EdContentCollectionViewCell.h"
#import "EdSegmentBarCollectionViewCell.h"
#import "EdSmallPicCollectionViewCell.h"
#import "EdBigPicCollectionViewCell.h"
#import "TagIndexViewController.h"
#import "PicDetailViewController.h"
#import "UserViewController.h"
#import "LikeViewController.h"
#import "UIImageView+WebCache.h"
#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraLibraryViewController.h"
#import "AddTagViewController.h"
#import "ActivityJoinListViewController.h"
#import "IndexViewController.h"

@interface ActivityDetailViewController () <UICollectionViewDelegate,BRCollectionViewDelegate,EdSegmentBarCollectionViewCellDelegate,EdBigPicCollectionViewCellDelegate,UIActionSheetDelegate,DBCameraViewControllerDelegate,DBCameraContainerDelegate,EdHeaderCollectionViewCellDelegate>

@property(nonatomic,strong) IBOutlet BRCollectionView *collectionView;
@property(nonatomic,strong) IBOutlet UIImageView *userHeader;
@property(nonatomic,strong) IBOutlet UILabel *userName;
@property(nonatomic,strong) IBOutlet NSLayoutConstraint *userInfoViewHeight;

@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;

@end

@implementation ActivityDetailViewController

@synthesize activity;

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initSetup];
    [self updateUI];
    [self loadData];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.collectionView setBRCollectionViewDelegate:self];
}

-(void)updateUI{
    if ([AVUser currentUser] != nil) {
        [self.userInfoViewHeight setConstant:60];
        AVUser *user = [AVUser currentUser];
        //设置用户名称
        [self.userName setText:user.username];
        
        //设置用户头像
        [self.userHeader sd_setImageWithURL:[NSURL URLWithString:((AVFile *)user[@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
    }
    else{
        [self.userInfoViewHeight setConstant:0];
    }
}

-(void)loadData{
    [self.parm setObject:@"0" forKey:@"skip"];
    [self.parm setObject:self.activity.objectId forKey:@"taskId"];
    [[EducationIndexManager shared] getActivityContentList:self.parm
                                      success:^(id objects){
                                          for (NSDictionary *dict in objects) {
                                              [self.result addObject:dict];
                                          }
                                          // 刷新表格
                                          [self.collectionView reloadData];
                                      }failure:^(NSError *error){
                                          
                                      }];
}

#pragma mark -- UICollectionViewDataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 3+self.result.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
        {
            EdHeaderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[EdHeaderCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
            cell.activity = self.activity;
            cell.delegate = self;
            [cell updateUI];
            return cell;
        }
            break;
        case 1:
        {
            EdContentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[EdContentCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
            cell.activity = self.activity;
            [cell updateUI];
            return cell;
        }
            break;
        case 2:
        {
            EdSegmentBarCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[EdSegmentBarCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
            cell.delegate = self;
            return cell;
        }
            break;
        default:
        {
            switch (self.showTypeCollection) {
                case 0:
                {
                    EdSmallPicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[EdSmallPicCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
                    cell.imgShared = self.result[indexPath.row-3];
                    [cell updateUI];
                    return cell;
                }
                    break;
                case 1:
                {
                    EdBigPicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[EdBigPicCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
                    cell.imgShared = self.result[indexPath.row-3];
                    cell.delegate = self;
                    [cell updateUI];
                    return cell;
                }
                    break;
                default:
                {
                    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
                    return cell;
                }
                    break;
            }
            
        }
            break;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewFlowLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
        {
            return CGSizeMake(SCREEN_WIDTH, 145);
        }
            break;
        case 1:
        {
            return [EdContentCollectionViewCell sizeWithData:self.activity];
        }
            break;
        case 2:
        {
            return CGSizeMake(SCREEN_WIDTH, 45);
        }
            break;
        default:
        {
            switch (self.showTypeCollection) {
                case 0:{
                    /*
                    CGFloat cellSpacing = collectionViewLayout.minimumInteritemSpacing;
                    UIEdgeInsets insets = collectionViewLayout.sectionInset;
                    CGFloat width = SCREEN_WIDTH / 3 - insets.left - cellSpacing / 3;
                     */
                    return CGSizeMake(105,100);
                }
                    break;
                    
                default:
                    return CGSizeMake(SCREEN_WIDTH,320);
                    break;
            }
        }
            break;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell * cell = (UICollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    switch (indexPath.row) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            if([AVUser currentUser]!=nil){
                LikeViewController *viewController = [LikeViewController brCreateFor:@"PublicStoryboard" andClassIdentifier:@"LikeViewController"];
                viewController.objectClass = @"taskPraise";
                viewController.objectKey = @"taskId";
                viewController.objectValue = self.activity.objectId;
                viewController.object = self.activity;
                viewController.likeType = 4;
                [self.navigationController pushViewController:viewController animated:YES];
            }
            else{
                [self onLogin];
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark --BRCollectionView Delegate

-(void)loadMoreDataCollection:(BRCollectionView *)collectionView{
    [self.parm setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.result.count] forKey:@"skip"];
    [[EducationIndexManager shared] getActivityContentList:self.parm
                                      success:^(id objects){
                                          for (NSDictionary *dict in objects) {
                                              [self.result addObject:dict];
                                          }
                                          [self.collectionView reloadData];
                                          [self.collectionView footerEndRefreshing];
                                      }failure:^(NSError *error){
                                          [self.collectionView footerEndRefreshing];
                                      }];
}

-(void)refreshCollection:(BRCollectionView *)collectionView{
    [self.parm setObject:@"0" forKey:@"skip"];
    [[EducationIndexManager shared] getActivityContentList:self.parm
                                                   success:^(id objects){
                                                       [self.result removeAllObjects];
                                                       self.result = objects;
                                                       [self.collectionView reloadData];
                                                       [self.collectionView headerEndRefreshing];
                                                   }failure:^(NSError *error){
                                                       [self.collectionView headerEndRefreshing];
                                                   }];
}

#pragma mark --EdSegmentBarCollectionViewCellDelegate

-(void)onChangeType:(int)type{
    self.showTypeCollection = type;
    [self.collectionView reloadData];
}

#pragma mark - EdBigPicCollectionViewCellDelegate

-(void)selectTag:(ImgTag *)imgTag{
    TagIndexViewController *tagIndexViewController = [TagIndexViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"TagIndexViewController"];
    tagIndexViewController.imgTag = imgTag;
    [self.navigationController pushViewController:tagIndexViewController animated:YES];
}

- (void)selectUser:(AVUser *)user{
    UserViewController *userViewController = [UserViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserStoryboard"];
    userViewController.user = user;
    userViewController.showTypeUser = 1;
    [self.navigationController pushViewController:userViewController animated:YES];
}

-(void)selectComment:(ImgShared *)imgShared{
    PicDetailViewController *picDetailViewController = [PicDetailViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"PicDetailViewController"];
    picDetailViewController.imgShared = imgShared;
    [self.navigationController pushViewController:picDetailViewController animated:YES];
}

-(void)onLogin{
    IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
    indexViewController.fromType = 1;
    UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:indexViewController];
    [self presentViewController:indexNavigationController animated:YES completion:nil];
}

-(void)selectMore:(ImgShared *)imgShared{
    if([AVUser currentUser]!=nil){
        [self showShareView:imgShared];
    }
    else{
        [self onLogin];
    }
}

-(IBAction)onJoin{
    UIActionSheet *headerSheet = [[UIActionSheet alloc] initWithTitle:@"请选择" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册选择", nil];
    headerSheet.tag = 9;
    [headerSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(actionSheet.tag == 9){
        switch (buttonIndex) {
            case 0:
            {
                DBCameraViewController *cameraController =
                [DBCameraViewController initWithDelegate:self];
                [cameraController setForceQuadCrop:YES];
                
                DBCameraContainerViewController *container =
                [[DBCameraContainerViewController alloc] initWithDelegate:self];
                [container setCameraViewController:cameraController];
                [container setFullScreenMode];
                
                UINavigationController *nav =
                [[UINavigationController alloc] initWithRootViewController:container];
                [nav setNavigationBarHidden:YES];
                [self presentViewController:nav animated:YES completion:nil];
            }
                break;
            case 1:
            {
                DBCameraLibraryViewController *libViewController = [[DBCameraLibraryViewController alloc] init];
                [libViewController setDelegate:self];
                [libViewController setContainerDelegate:self];
                
                UINavigationController *nav =
                [[UINavigationController alloc] initWithRootViewController:libViewController];
                [nav setNavigationBarHidden:YES];
                [self presentViewController:nav animated:YES completion:nil];
            }
                break;
            default:
                break;
        }
    }
}

- (void)camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata {
    //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    [cameraViewController restoreFullScreenMode];
    AddTagViewController *viewController = [AddTagViewController brCreateFor:@"PostStoryboard" andClassIdentifier:@"AddTagViewController"];
    viewController.image = image;
    viewController.taskId = self.activity.objectId;
    [cameraViewController dismissViewControllerAnimated:YES completion:^{
        [self presentViewController:viewController animated:YES completion:nil];
    }];
}

- (void)dismissCamera:(id)cameraViewController {
    [cameraViewController restoreFullScreenMode];
    [cameraViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)backFromController:(id)fromController{
    [fromController dismissViewControllerAnimated:YES completion:nil];
}

-(void)switchFromController:(id)fromController toController:(id)controller{
    
}

-(IBAction)onJoinToUser{
    if ([AVUser currentUser]!=nil) {
        ActivityJoinListViewController *viewController = [ActivityJoinListViewController brCreateFor:@"EducationStoryboard" andClassIdentifier:@"ActivityJoinListViewController"];
        viewController.activity = self.activity;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else{
        [self onLogin];
    }
}

-(IBAction)selectActivityMore{
    if([AVUser currentUser]!=nil){
        [self showShareView:self.activity];
    }
    else{
        [self onLogin];
    }
}

@end

