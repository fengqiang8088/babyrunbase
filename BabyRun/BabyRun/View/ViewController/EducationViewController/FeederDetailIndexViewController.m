//
//  FeederDetailIndexViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-17.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "FeederDetailIndexViewController.h"

#import "SCNavTabBarController.h"
#import "FeederDetailViewController.h"
#import "EducationIndexManager.h"
#import "IndexViewController.h"
#import "FeederDetail.h"

@interface FeederDetailIndexViewController () <SCNavTabBarControllerDelegate,FeederDetailDelegate>

@property (nonatomic,strong) IBOutlet UIPageControl *pageControl;

@property (nonatomic,strong) SCNavTabBarController *navTabBarController;
@property (nonatomic,strong) NSMutableArray *feederDetailList;

@end


@implementation FeederDetailIndexViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initSetup];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    [self loadData];
}

- (void)loadData{
    self.feederDetailList = [[NSMutableArray alloc] init];
    [[EducationIndexManager shared] getFeederDetailListById:self.feeder.objectId
                                                    success:^(id objects){
                                                        for (NSDictionary *dict in objects) {
                                                            [self.feederDetailList addObject:dict];
                                                        }
                                                        [self initSegmentBar];
                                                        [self initRootViewController];
                                                        self.isLoadModel = YES;
                                                    }failure:^(NSError *error){
                                                        self.isLoadModel = NO;
                                                    }];
}

-(void)initSegmentBar{
    NSMutableArray *childViewControllers = [[NSMutableArray alloc] init];
    for(int i=0; i<self.feederDetailList.count; i++){
        FeederDetailViewController *childViewController = [FeederDetailViewController brCreateFor:@"EducationStoryboard" andClassIdentifier:@"FeederDetailViewController"];
        childViewController.title = @"";
        childViewController.feederDetail = self.feederDetailList[i];
        childViewController.isFirstLoad = YES;
        childViewController.delegate = self;
        [childViewControllers addObject:childViewController];
    }
    
    self.navTabBarController = [[SCNavTabBarController alloc] init];
    self.navTabBarController.scNavDelegate = self;
    self.navTabBarController.subViewControllers = childViewControllers;
    self.navTabBarController.showArrowButton = NO;
    self.navTabBarController.scrollAnimation = YES;
    [self.navTabBarController addParentController:self];
    [self.navTabBarController setNavTabBarHidden];

}

-(void)arrowBtnClick{
}

-(void)initRootViewController{
    [self setupBackBtn];
    [self.pageControl setNumberOfPages:self.feederDetailList.count];
}

-(void)scrollViewEnd:(NSInteger *)index{
    [(BaseViewController *)self.navTabBarController.subViewControllers[*index] loadData];
    [self.pageControl setCurrentPage:*index];
}

-(void)selectMore:(FeederDetail *)feederDetail{
    if([AVUser currentUser]!=nil){
        [self showShareView:feederDetail];
    }
    else{
        [self onLogin];
    }
}

-(void)onLogin{
    IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
    indexViewController.fromType = 1;
    UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:indexViewController];
    [self presentViewController:indexNavigationController animated:YES completion:nil];
}

@end
