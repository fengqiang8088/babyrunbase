//
//  FeederDetailViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-12-17.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeederDetail.h"

@protocol FeederDetailDelegate <NSObject>

@required
- (void)selectMore:(FeederDetail *)feederDetail;
@end

@interface FeederDetailViewController : BaseViewController

@property(nonatomic,strong) FeederDetail *feederDetail;
@property (nonatomic,weak) id<FeederDetailDelegate> delegate;

@end
