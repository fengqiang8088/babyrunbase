//
//  FeederIndexViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "FeederIndexViewController.h"

#import "BRTableView.h"
#import "EducationIndexManager.h"
#import "FeederTableViewCell.h"
#import "FeederDetailIndexViewController.h"


@interface FeederIndexViewController ()<UITableViewDelegate,UIScrollViewDelegate>

@property(nonatomic,strong) IBOutlet BRTableView *tableView;
@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;
@property(nonatomic,assign) CGPoint lastContentOffset;

@end

@implementation FeederIndexViewController

-(void)loadData{
    if (!self.isLoadModel) {
        [self.parm setObject:@"0" forKey:@"skip"];
        [[EducationIndexManager shared] getFeederIndexList:self.parm
                                                   success:^(id objects){
                                                       self.result = objects;
                                                       // 刷新表格
                                                       [self.tableView reloadData];
                                                       self.isLoadModel = YES;
                                                   }failure:^(NSError *error){
                                                       self.isLoadModel = NO;
                                                   }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    if (self.isFirstLoad) {
        [self loadData];
    }
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.result.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FeederTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[FeederTableViewCell reuseIdentifier]];
    cell.data = self.result[indexPath.row];
    [cell updateUI];
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    DiceTableViewCellScrollDirection scrollDirection = ScrollDirectionNone;
    
    if (self.lastContentOffset.x > scrollView.contentOffset.x)
    {
        scrollDirection = ScrollDirectionRight;
    }
    else if (self.lastContentOffset.x < scrollView.contentOffset.x)
    {
        scrollDirection = ScrollDirectionLeft;
    }
    else if (self.lastContentOffset.y > scrollView.contentOffset.y)
    {
        scrollDirection = ScrollDirectionDown;
        //        NSLog(@"DOWN");
    }
    else if (self.lastContentOffset.y < scrollView.contentOffset.y)
    {
        scrollDirection = ScrollDirectionUp;
        //        NSLog(@"UP");
    }
    else
    {
        scrollDirection = ScrollDirectionCrazy;
    }
    
    self.lastContentOffset = scrollView.contentOffset;
    
    id notificationObject = [NSNumber numberWithInteger:scrollDirection];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:DiceTableViewCellDirectionNotification object:notificationObject];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    FeederDetailIndexViewController *feederDetailIndexViewController = [FeederDetailIndexViewController brCreateFor:@"EducationStoryboard" andClassIdentifier:@"FeederDetailIndexViewController"];
    feederDetailIndexViewController.feeder = self.result[indexPath.row];
    [self.navigationController pushViewController:feederDetailIndexViewController animated:YES];
}

@end
