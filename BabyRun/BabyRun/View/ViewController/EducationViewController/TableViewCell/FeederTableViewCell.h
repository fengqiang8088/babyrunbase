//
//  FeederTableViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "Feeder.h"

typedef enum DiceTableViewCellScrollDirection : NSInteger {
    ScrollDirectionNone = 0,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} DiceTableViewCellScrollDirection;

#define DiceTableViewCellDirectionNotification @"DiceTableViewCellDirectionNotification"
#define DiceTableViewMaximumOffset 9.0f
#define DiceTableViewOffsetFactor 0.25f

@interface FeederTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *feederImage;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *constraintVerticalCenter;
@property (nonatomic,strong) Feeder *data;

+ (NSString *)reuseIdentifier;
- (void)updateUI;

@end
