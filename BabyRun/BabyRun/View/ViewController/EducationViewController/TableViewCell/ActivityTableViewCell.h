//
//  ActivityTableViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "Activity.h"

@interface ActivityTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *activityImage;
@property (nonatomic,strong) IBOutlet UILabel *joinCount;

@property (nonatomic,strong) Activity *data;

+ (NSString *)reuseIdentifier;
- (void)updateUI;

@end
