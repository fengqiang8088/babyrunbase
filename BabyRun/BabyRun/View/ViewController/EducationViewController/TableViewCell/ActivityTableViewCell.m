//
//  ActivityTableViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "ActivityTableViewCell.h"

@implementation ActivityTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)updateUI{
    
    //设置活动图片    
    [self.data.image getDataInBackgroundWithBlock:^(NSData *data , NSError *error){
        [self.activityImage setImage:[UIImage imageWithData:data]];
    }progressBlock:^(NSInteger percentDone){

    }];
    
    //设置参加人数
    [self.joinCount setText:[NSString stringWithFormat:@"%@ 人次参加",[self.data.joinCount stringValue]]];
}

@end
