//
//  ActivityJoinTableViewCell.m
//  BabyRun
//
//  Created by fengqiang on 15-1-8.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "ActivityJoinTableViewCell.h"

#import "UIImageView+WebCache.h"
#import "UserManager.h"

@implementation ActivityJoinTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)updateUI{
    
    //设置用户名称
    [self.userName setText:self.data.user.username];
    
    //设置用户头像
    [self.userHeader sd_setImageWithURL:[NSURL URLWithString:((AVFile *)self.data.user[@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
    
    //设置用户信息
    NSString *userInfo = [NSString stringWithFormat:@"%@",self.data.user[@"address"]?:@""];
    [self.userInfo setText:userInfo];
    
    switch (self.data.relation) {
        case 0:
            [self.likeBtn setTitle:@"邀请" forState:UIControlStateNormal];
            [self.likeBtn setTitleColor:BRPurpleColor forState:UIControlStateNormal];
            break;
        case 1:
            [self.likeBtn setTitle:@"已邀请" forState:UIControlStateNormal];
            [self.likeBtn setTitleColor:BRBLUEColor forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

-(IBAction)onJoinBtn{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:self.data.user.objectId forKey:@"inviteId"];
    [dic setObject:self.activity.objectId forKey:@"taskId"];
    
    if (self.data.relation == 0) {
        [[UserManager shared] addInviteToUser:dic success:^(id objects) {
            AVObject *object = objects;
            if (object.objectId) {
                //发送给指定对象通知
                NSMutableDictionary *dic1 = [[NSMutableDictionary alloc] init];
                [dic1 setObject:self.data.user.objectId forKey:@"objectId"];
                [dic1 setObject:@"邀请你参加活动" forKey:@"content"];
                [dic1 setObject:@"0" forKey:@"statusType"];
                [dic1 setObject:@"6" forKey:@"messageType"];
                [[UserManager shared] addUserMessage:dic1 success:^(id objects){
                    if ([objects[@"success"] boolValue]) {
                        [self.likeBtn setTitle:@"已邀请" forState:UIControlStateNormal];
                        [self.likeBtn setTitleColor:BRBLUEColor forState:UIControlStateNormal];
                        self.data.relation = 1;
                    }
                }failure:^(NSError *error){
                    
                }];
            }
        } failure:^(NSError *error) {
            
        }];
    }
}

@end
