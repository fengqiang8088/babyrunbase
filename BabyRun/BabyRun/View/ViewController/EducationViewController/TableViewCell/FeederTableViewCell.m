//
//  FeederTableViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-16.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "FeederTableViewCell.h"

@implementation FeederTableViewCell

- (void)awakeFromNib {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDiceTableViewCellDirectionNotification:) name:DiceTableViewCellDirectionNotification object:nil];
    
}

- (void)dealloc;{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)handleDiceTableViewCellDirectionNotification:(NSNotification *)notification;{
    id notificationObject = notification.object;
    
    DiceTableViewCellScrollDirection scrollDirection = [(NSNumber *)notificationObject integerValue];
    
    CGFloat constant = self.constraintVerticalCenter.constant;
    
    switch (scrollDirection) {
        case ScrollDirectionUp:
        {
            constant -= DiceTableViewOffsetFactor;
            if (-1 * DiceTableViewMaximumOffset > constant)
            {
                constant = -1 * DiceTableViewMaximumOffset;
            }
        }
            break;
        case ScrollDirectionDown:
        {
            constant += DiceTableViewOffsetFactor;
            if (DiceTableViewMaximumOffset < constant)
            {
                constant = DiceTableViewMaximumOffset;
            }
        }
        default:
        {
            //            Do Nothing
        }
            break;
    }
    
    self.constraintVerticalCenter.constant = constant;
    
}

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)updateUI{
    
    //设置用户发布图片    
    [self.data.image getDataInBackgroundWithBlock:^(NSData *data , NSError *error){
        [self.feederImage setImage:[UIImage imageWithData:data]];
    }progressBlock:^(NSInteger percentDone){

    }];
}

@end
