//
//  ActivityJoinTableViewCell.h
//  BabyRun
//
//  Created by fengqiang on 15-1-8.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "ViewUser.h"
#import "Activity.h"

@interface ActivityJoinTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *userHeader;
@property (nonatomic,strong) IBOutlet UILabel *userName;
@property (nonatomic,strong) IBOutlet UILabel *userInfo;
@property (nonatomic,strong) IBOutlet UIButton *likeBtn;

@property (nonatomic,strong) ViewUser *data;
@property (nonatomic,strong) Activity *activity;

+ (NSString *)reuseIdentifier;
- (void)updateUI;

@end
