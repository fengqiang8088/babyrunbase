//
//  ActivityDetailViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-12-17.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "Activity.h"

@interface ActivityDetailViewController : BaseViewController

@property (nonatomic,strong) Activity *activity;
@property (nonatomic,assign) int showTypeCollection;  //0:small，1:big

@end
