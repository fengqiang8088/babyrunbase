//
//  FeederDetailViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-17.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "FeederDetailViewController.h"
#import "EducationIndexManager.h"
#import "UIImageView+WebCache.h"
#import "TYMProgressBarView.h"
#import "LikeViewController.h"
#import "IndexViewController.h"

@interface FeederDetailViewController ()

@property (nonatomic,strong) IBOutlet UIScrollView *rootView;
@property (nonatomic,strong) IBOutlet UIImageView *imageView;
@property (nonatomic,strong) IBOutlet UIWebView *content;
@property (nonatomic,strong) IBOutlet UILabel *readCount;
@property (nonatomic,strong) IBOutlet UIButton *likeBtn;
@property (nonatomic,strong) IBOutlet TYMProgressBarView *progressBarView;
@property (nonatomic,strong) IBOutlet UIView *likeView;
@property (nonatomic,strong) IBOutlet UILabel *likeCount;

//喜欢的人
@property(nonatomic,strong) NSArray *likeUser;

@end

@implementation FeederDetailViewController

-(void)viewDidAppear:(BOOL)animated{
    [self.rootView setContentSize:CGSizeMake(0, SCREEN_HEIGHT+341)];
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    [self.progressBarView setBarBackgroundColor:[UIColor whiteColor]];
    [self.progressBarView setBarFillColor:BRGRAY1Color];
    [self.progressBarView setBarBorderWidth:0];
    
    [self loadData];
}

-(void)loadData{
    [self updateUI];
}

-(void)updateUI{
    //设置用户发布图片
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:self.feederDetail.image.url] placeholderImage:[UIImage imageNamed:@"picindex_ph"] options:SDWebImageLowPriority progress:^(NSInteger receivedSize,NSInteger expectedSize){
        [self.progressBarView setHidden:NO];
        double progressValue = (double)receivedSize/(double)expectedSize;
        [self.progressBarView setProgress:progressValue];
    }completed:^(UIImage *image,NSError *error,SDImageCacheType cacheType,NSURL *imageURL){
        [self.progressBarView setHidden:YES];
    }];
    
    //设置浏览数
    [self.readCount setText:[NSString stringWithFormat:@"阅读 %@",self.feederDetail.readCount]];
    
    //设置喜欢按钮
    [self updateLikeBtn];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:self.feederDetail.objectId forKey:@"articleId"];
    [[EducationIndexManager shared] getFeederLikeIndex:dic success:^(id objects) {
        self.likeUser = objects[@"likeList"];
        for (UIView *view in self.likeView.subviews) {
            [view removeFromSuperview];
        }
        for (int i=0;i<self.likeUser.count;i++) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10+45*i, 10, 40, 40)];
            [imageView.layer setCornerRadius:20];
            [imageView.layer setMasksToBounds:YES];
            //设置用户头像
            [imageView sd_setImageWithURL:[NSURL URLWithString:((AVFile *)self.likeUser[i][@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
            [self.likeView addSubview:imageView];
        }
        
        [self.likeCount setText:[NSString stringWithFormat:@"%@ 人赞过",objects[@"likeCount"]]];
    } failure:^(NSError *error) {
    
    }];
    
    [self.content loadHTMLString:self.feederDetail.content baseURL:[NSURL fileURLWithPath: [[NSBundle mainBundle]  bundlePath]]];
}

-(IBAction)onLikeList{
    if ([AVUser currentUser]!=nil) {
        LikeViewController *viewController = [LikeViewController brCreateFor:@"PublicStoryboard" andClassIdentifier:@"LikeViewController"];
        viewController.objectClass = @"articlePraise";
        viewController.objectKey = @"articleId";
        viewController.objectValue = self.feederDetail.objectId;
        viewController.object = self.feederDetail;
        viewController.likeType = 3;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else{
        [self onLogin];
    }
}

-(void)updateLikeBtn{
    if (self.feederDetail.isLike) {
        [self.likeBtn setImage:[UIImage imageNamed:@"PicIndexCell_like_btn"] forState:UIControlStateNormal];
    }
    else{
        [self.likeBtn setImage:[UIImage imageNamed:@"PicIndexCell_unlike_btn"] forState:UIControlStateNormal];
    }
    [self.likeBtn setTitle:[NSString stringWithFormat:@"%@",self.feederDetail.likeCount] forState:UIControlStateNormal];
}

-(IBAction)onLike{
    if ([AVUser currentUser]!=nil) {
        //添加赞
        if (!self.feederDetail.isLike) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:1];
            [dic setObject:self.feederDetail.objectId forKey:@"articleId"];
            [[EducationIndexManager shared] addFeederLike:dic
                                                  success:^(id objects){
                                                      if (objects!=nil) {
                                                          self.feederDetail.isLike = YES;
                                                          self.feederDetail.likeCount = [NSNumber numberWithInt:[self.feederDetail.likeCount intValue]+1];
                                                      }
                                                      [self updateLikeBtn];
                                                  }failure:^(NSError *error){
                                                      
                                                  }];
        }
        //取消赞
        else{
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:1];
            [dic setObject:self.feederDetail.objectId forKey:@"articleId"];
            [[EducationIndexManager shared] removeFeederLike:dic
                                                     success:^(id objects){
                                                         self.feederDetail.isLike = NO;
                                                         self.feederDetail.likeCount = [NSNumber numberWithInt:[self.feederDetail.likeCount intValue]-1];
                                                         [self updateLikeBtn];
                                                     }failure:^(NSError *error){
                                                         
                                                     }];
        }
    }
    else{
        [self onLogin];
    }
}

-(void)onLogin{
    IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
    indexViewController.fromType = 1;
    UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:indexViewController];
    [self presentViewController:indexNavigationController animated:YES completion:nil];
}

-(IBAction)onMore{
    [self.delegate selectMore:self.feederDetail];
}

@end
