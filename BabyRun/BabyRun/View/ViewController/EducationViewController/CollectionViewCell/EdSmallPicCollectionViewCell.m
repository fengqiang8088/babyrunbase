//
//  EdSmallPicCollectionViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "EdSmallPicCollectionViewCell.h"
#import "UIImageView+WebCache.h"

@implementation EdSmallPicCollectionViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)updateUI{
    //设置用户发布图片
    [self.image sd_setImageWithURL:[NSURL URLWithString:self.imgShared.imgUrl] placeholderImage:[UIImage imageNamed:@"picindex_ph"]];
}

@end
