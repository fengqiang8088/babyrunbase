//
//  EdSegmentBarCollectionViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "EdSegmentBarCollectionViewCell.h"

@implementation EdSegmentBarCollectionViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

-(IBAction)onChangeType:(UIButton *)btn{
    [self.picSmallBtn setSelected:NO];
    [self.picBigBtn setSelected:NO];
    [btn setSelected:YES];
    [self.delegate onChangeType:(int)btn.tag];
}

@end
