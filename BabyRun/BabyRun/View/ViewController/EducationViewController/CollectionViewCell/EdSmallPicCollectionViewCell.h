//
//  EdSmallPicCollectionViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "ImgShared.h"

@interface EdSmallPicCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UIImageView *image;
@property (nonatomic,strong) ImgShared *imgShared;

+ (NSString *)reuseIdentifier;
- (void)updateUI;

@end
