//
//  EdBigPicCollectionViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "ImgShared.h"
#import "ImgTag.h"
#import "TYMProgressBarView.h"

@protocol EdBigPicCollectionViewCellDelegate <NSObject>

@required
- (void)selectTag:(ImgTag *)imgTag;
- (void)selectUser:(AVUser *)user;
- (void)selectComment:(ImgShared *)imgShared;
- (void)selectMore:(ImgShared *)imgShared;
- (void)onLogin;
@end

@interface EdBigPicCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UIImageView *userHeader;
@property (nonatomic,strong) IBOutlet UIImageView *userPic;
@property (nonatomic,strong) IBOutlet UILabel *userName;
@property (nonatomic,strong) IBOutlet UILabel *userInfo;
@property (nonatomic,strong) IBOutlet UILabel *userContext;
@property (nonatomic,strong) IBOutlet UILabel *updateTime;
@property (nonatomic,strong) IBOutlet UIButton *opBtn;
@property (nonatomic,strong) IBOutlet UILabel *readCount;
@property (nonatomic,strong) IBOutlet UIButton *likeBtn;
@property (nonatomic,strong) IBOutlet UIButton *commentBtn;
@property (nonatomic,weak) IBOutlet UIView *contentShowView;
@property (nonatomic,weak) IBOutlet UIView *tagShowView;
@property (nonatomic, strong) IBOutlet TYMProgressBarView *progressBarView;

@property (nonatomic,strong) ImgShared *imgShared;
@property (nonatomic,weak) id<EdBigPicCollectionViewCellDelegate> delegate;
@property (nonatomic,assign) BOOL isFav;

+ (NSString *)reuseIdentifier;
- (void)updateUI;

@end
