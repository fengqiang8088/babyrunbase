//
//  EdHeaderCollectionViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "EdHeaderCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "EducationIndexManager.h"

@implementation EdHeaderCollectionViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)updateUI{
    //设置用户发布图片
    [self.image sd_setImageWithURL:[NSURL URLWithString:((AVFile *)self.activity.image).url] placeholderImage:[UIImage imageNamed:@"picindex_ph"]];
    
    //设置浏览数
    [self.readLabel setText:[NSString stringWithFormat:@"参与 %@ 人",self.activity.joinCount]];
    
    //设置喜欢按钮
    [self updateLikeBtn];
}

-(void)updateLikeBtn{
    if (self.activity.isLike) {
        [self.likeBtn setImage:[UIImage imageNamed:@"PicIndexCell_like_btn"] forState:UIControlStateNormal];
    }
    else{
        [self.likeBtn setImage:[UIImage imageNamed:@"PicIndexCell_unlike_btn"] forState:UIControlStateNormal];
    }
    [self.likeBtn setTitle:[NSString stringWithFormat:@"%@",self.activity.likeCount] forState:UIControlStateNormal];
}

-(IBAction)onLike{
    if ([AVUser currentUser]!=nil) {
        //添加赞
        if (!self.activity.isLike) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:1];
            [dic setObject:self.activity.objectId forKey:@"taskId"];
            [[EducationIndexManager shared] addActivityLike:dic
                                                    success:^(id objects){
                                                        if (objects!=nil) {
                                                            self.activity.isLike = YES;
                                                            self.activity.likeCount = [NSNumber numberWithInt:[self.activity.likeCount intValue]+1];
                                                        }
                                                        [self updateLikeBtn];
                                                    }failure:^(NSError *error){
                                                        
                                                    }];
        }
        //取消赞
        else{
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:1];
            [dic setObject:self.activity.objectId forKey:@"taskId"];
            [[EducationIndexManager shared] removeActivityLike:dic
                                                       success:^(id objects){
                                                           self.activity.isLike = NO;
                                                           self.activity.likeCount = [NSNumber numberWithInt:[self.activity.likeCount intValue]-1];
                                                           [self updateLikeBtn];
                                                       }failure:^(NSError *error){
                                                           
                                                       }];
        }
    }
    else{
        [self.delegate onLogin];
    }
}

@end
