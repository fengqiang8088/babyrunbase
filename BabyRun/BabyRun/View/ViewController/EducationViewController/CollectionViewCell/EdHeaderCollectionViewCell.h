//
//  EdHeaderCollectionViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "Activity.h"

@protocol EdHeaderCollectionViewCellDelegate <NSObject>

@required
- (void)onLogin;
@end

@interface EdHeaderCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong) IBOutlet UIImageView *image;
@property(nonatomic,strong) IBOutlet UILabel *readLabel;
@property(nonatomic,strong) IBOutlet UIButton *likeBtn;
@property(nonatomic,strong) IBOutlet UIButton *opBtn;

@property (nonatomic,strong) Activity *activity;
@property (nonatomic,weak) id<EdHeaderCollectionViewCellDelegate> delegate;

+ (NSString *)reuseIdentifier;
- (void)updateUI;

@end
