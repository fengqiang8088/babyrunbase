//
//  EdContentCollectionViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "EdContentCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "EducationIndexManager.h"

@implementation EdContentCollectionViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)updateUI{
    //更新活动内容
    [self.contentlabel setText:self.activity.content];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:self.activity.objectId forKey:@"taskId"];
    [[EducationIndexManager shared] getActivityLikeIndex:dic success:^(id objects) {
        self.likeUser = objects[@"likeList"];
        for (UIView *view in self.userView.subviews) {
            [view removeFromSuperview];
        }
        for (int i=0;i<self.likeUser.count;i++) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(45*i, 0, 40, 40)];
            [imageView.layer setCornerRadius:20];
            [imageView.layer setMasksToBounds:YES];
            //设置用户头像
            [imageView sd_setImageWithURL:[NSURL URLWithString:((AVFile *)self.likeUser[i][@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
            [self.userView addSubview:imageView];
        }
        [self.likelabel setText:[NSString stringWithFormat:@"%@ 人赞过",objects[@"likeCount"]]];
    } failure:^(NSError *error) {
        
    }];
}

+ (CGSize)sizeWithData:(id)data{
    Activity *activity = data;
    //计算文字高度
    CGFloat textSize = [activity.content boundingRectWithSize:CGSizeMake(SCREEN_WIDTH-20,CGFLOAT_MAX)
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:[UIFont fontWithName:@"HiraKakuProN-W3" size:12]}
                                                     context:nil].size.height;
    return CGSizeMake(SCREEN_WIDTH, 75+textSize);
}

@end
