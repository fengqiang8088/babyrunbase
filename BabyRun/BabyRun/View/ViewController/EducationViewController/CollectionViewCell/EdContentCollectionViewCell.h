//
//  EdContentCollectionViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "Activity.h"

@interface EdContentCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UIView *userView;
@property (nonatomic,strong) IBOutlet UILabel *likelabel;
@property (nonatomic,strong) IBOutlet UILabel *contentlabel;

@property (nonatomic,strong) Activity *activity;
@property(nonatomic,strong) NSArray *likeUser;

+ (NSString *)reuseIdentifier;
+ (CGSize)sizeWithData:(id)data;
- (void)updateUI;

@end
