//
//  EdSegmentBarCollectionViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-29.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

@protocol EdSegmentBarCollectionViewCellDelegate <NSObject>

@required
- (void)onChangeType:(int)type;
@end

@interface EdSegmentBarCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong) IBOutlet UIButton *picSmallBtn;
@property(nonatomic,strong) IBOutlet UIButton *picBigBtn;

@property(nonatomic,weak) id<EdSegmentBarCollectionViewCellDelegate> delegate;

+ (NSString *)reuseIdentifier;

@end
