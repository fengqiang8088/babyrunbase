//
//  ActivityJoinListViewController.m
//  BabyRun
//
//  Created by fengqiang on 15-1-8.
//  Copyright (c) 2015年 fengqiang. All rights reserved.
//

#import "ActivityJoinListViewController.h"

#import "UserManager.h"
#import "ActivityJoinTableViewCell.h"
#import "BRTableView.h"

@interface ActivityJoinListViewController ()<UITableViewDelegate,BRTableViewDelegate>

@property(nonatomic,strong) IBOutlet BRTableView *tableView;

@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;

@end

@implementation ActivityJoinListViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.tableView setBRTableViewDelegate:self];
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    [self loadData];
}

-(void)loadData{
    [self.parm setObject:self.activity.objectId forKey:@"taskId"];
    [[UserManager shared] getFansForTaskList:self.parm
                                     success:^(id objects){
                                         self.result = objects;
                                         // 刷新表格
                                         [self.tableView reloadData];
                                         self.isLoadModel = YES;
                                     }failure:^(NSError *error){
                                         self.isLoadModel = NO;
                                     }];
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.result.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ActivityJoinTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ActivityJoinTableViewCell reuseIdentifier]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.data = self.result[indexPath.row];
    cell.activity = self.activity;
    [cell updateUI];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)refreshTable:(BRTableView *)tableview{
    [self.parm setObject:@"0" forKey:@"skip"];
    [[UserManager shared] getFansForTaskList:self.parm
                                      success:^(id objects){
                                          [self.result removeAllObjects];
                                          self.result = objects;
                                          // 刷新表格
                                          [tableview reloadData];
                                          [tableview headerEndRefreshing];
                                      }failure:^(NSError *error){
                                          [tableview headerEndRefreshing];
                                      }];
}

- (void)loadMoreDataTable:(BRTableView *)tableview{
    [self.parm setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.result.count] forKey:@"skip"];
    [[UserManager shared] getFansForTaskList:self.parm
                                      success:^(id objects){
                                          for (NSDictionary *dict in objects) {
                                              [self.result addObject:dict];
                                          }
                                          [tableview reloadData];
                                          [tableview footerEndRefreshing];
                                      }failure:^(NSError *error){
                                          
                                      }];
}

@end
