//
//  LikeIndexTableViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-27.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikeIndexTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIView *userView;
@property (nonatomic,strong) IBOutlet UILabel *label;

@property(nonatomic,strong) NSArray *likeUser;

+ (NSString *)reuseIdentifier;
- (void)updateUI;

@end
