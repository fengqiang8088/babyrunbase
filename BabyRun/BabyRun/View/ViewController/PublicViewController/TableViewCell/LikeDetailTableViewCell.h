//
//  LikeDetailTableViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-27.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "ViewUser.h"

@interface LikeDetailTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *userHeader;
@property (nonatomic,strong) IBOutlet UILabel *userName;
@property (nonatomic,strong) IBOutlet UILabel *userInfo;
@property (nonatomic,strong) IBOutlet UIButton *likeBtn;

@property (nonatomic,strong) ViewUser *data;

+ (NSString *)reuseIdentifier;
- (void)updateUI;

@end
