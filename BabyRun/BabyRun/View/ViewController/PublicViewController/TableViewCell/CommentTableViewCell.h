//
//  CommentTableViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-27.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "Comment.h"
#import "HBVLinkedTextView.h"

@interface CommentTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *userHeader;
@property (nonatomic,strong) IBOutlet HBVLinkedTextView *linkedTextView;
@property (nonatomic,strong) IBOutlet UILabel *floor;

@property(nonatomic,strong) Comment *comment;
@property(nonatomic,assign) int type;   //0:图片评论

+ (NSString *)reuseIdentifier;
+ (CGSize)sizeWithData:(id)data;
- (void)updateUI;

@end
