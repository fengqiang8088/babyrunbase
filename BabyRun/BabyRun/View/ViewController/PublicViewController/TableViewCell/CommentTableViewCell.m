//
//  CommentTableViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-27.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "CommentTableViewCell.h"
#import "PublicManager.h"
#import "UIImageView+WebCache.h"

@implementation CommentTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)awakeFromNib {
    // Initialization code
    [self.linkedTextView setUserInteractionEnabled:NO];
}

- (void)updateUI{
        
    //设置用户头像
    [self.userHeader sd_setImageWithURL:[NSURL URLWithString:((AVFile *)self.comment.user[@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
    
    //计算回复时间
    NSString *upTime = [[PublicManager shared] returnUploadTime:self.comment.updateTime];
    
    //将用户名特殊显示
    NSString *userName = self.comment.user.username;
    //设置评论内容
    self.linkedTextView.text = [NSString stringWithFormat:@"%@ : %@  %@",userName,self.comment.content,upTime];
    //设置不同颜色
    [self.linkedTextView linkString:userName
                   defaultAttributes:@{NSForegroundColorAttributeName:BRPurpleColor}
               highlightedAttributes:nil
                          tapHandler:nil];
    
    [self.linkedTextView linkString:upTime
                  defaultAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"HiraKakuProN-W3" size:10],
                                      NSForegroundColorAttributeName:BRBlackColor}
              highlightedAttributes:nil
                         tapHandler:nil];
    
}

+ (CGSize)sizeWithData:(id)data{
    Comment *comment = data;
    //计算文字高度
    CGFloat textSize = [comment.content boundingRectWithSize:CGSizeMake(SCREEN_WIDTH-65,CGFLOAT_MAX)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{NSFontAttributeName:[UIFont fontWithName:@"HiraKakuProN-W3" size:12]}
                                          context:nil].size.height;
    return CGSizeMake(0, 100+textSize);
}

@end
