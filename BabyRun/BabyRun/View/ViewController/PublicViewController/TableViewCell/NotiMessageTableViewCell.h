//
//  NotiMessageTableViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-28.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "NotiMessage.h"

@interface NotiMessageTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *userHeader;
@property (nonatomic,strong) IBOutlet UILabel *linkedTextView;
@property (nonatomic,strong) IBOutlet UILabel *updateLabel;
@property (nonatomic,strong) IBOutlet UIButton *favBtn;
@property (nonatomic,strong) IBOutlet UIImageView *imgShared;
@property (nonatomic,strong) IBOutlet UILabel *textShared;

@property (nonatomic,strong) NotiMessage *message;

+ (NSString *)reuseIdentifier;
+ (CGSize)sizeWithData:(id)data;
- (void)updateUI;

@end
