//
//  LikeIndexTableViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-27.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "LikeIndexTableViewCell.h"
#import "UIImageView+WebCache.h"

@implementation LikeIndexTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)updateUI{
    for (UIView *view in self.userView.subviews) {
        [view removeFromSuperview];
    }
    for (int i=0;i<self.likeUser.count;i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10+45*i, 10, 40, 40)];
        [imageView.layer setCornerRadius:20];
        [imageView.layer setMasksToBounds:YES];
        //设置用户头像
        [imageView sd_setImageWithURL:[NSURL URLWithString:((AVFile *)self.likeUser[i][@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
        [self.userView addSubview:imageView];
    }
    
    [self.label setText:[NSString stringWithFormat:@"%lu 人赞过",(unsigned long)self.likeUser.count]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
