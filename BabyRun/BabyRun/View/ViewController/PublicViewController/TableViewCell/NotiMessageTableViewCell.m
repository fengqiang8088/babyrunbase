//
//  NotiMessageTableViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-28.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "NotiMessageTableViewCell.h"
#import "PublicManager.h"
#import "UIImageView+WebCache.h"
#import "UserManager.h"

@implementation NotiMessageTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)updateUI{
    
    //设置用户头像
    [self.userHeader sd_setImageWithURL:[NSURL URLWithString:((AVFile *)self.message.user[@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
    
    //计算回复时间
    [self.linkedTextView setText:[NSString stringWithFormat:@"%@ : %@",self.message.user.username,self.message.content]];
    [self.updateLabel setText:[[PublicManager shared] returnUploadTime:self.message.updateTime]];

    switch (self.message.type) {
        case 0:
        {
            [self.favBtn setHidden:NO];
            [self.imgShared setHidden:YES];
            [self.textShared setHidden:YES];
            switch (self.message.relation) {
                case 0:
                    [self.favBtn setImage:[UIImage imageNamed:@"fav_no"] forState:UIControlStateNormal];
                    break;
                case 1:
                    [self.favBtn setImage:[UIImage imageNamed:@"fav_yes"] forState:UIControlStateNormal];
                    break;
                case 2:
                    [self.favBtn setImage:[UIImage imageNamed:@"fav_too"] forState:UIControlStateNormal];
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            [self.favBtn setHidden:YES];
            [self.imgShared setHidden:NO];
            
            AVFile * file = [AVFile fileWithURL:self.message.targetContent];
            [file getThumbnail:YES width:45 height:45 withBlock:^(UIImage * image, NSError *error) {
                [self.imgShared setImage:image];
            }];
            [self.textShared setHidden:YES];
        }
            break;
        case 2:
        {
            [self.favBtn setHidden:YES];
            [self.imgShared setHidden:YES];
            [self.textShared setHidden:NO];
            [self.textShared setText:self.message.targetContent];
        }
        case 3:
        {
            [self.favBtn setHidden:YES];
            [self.imgShared setHidden:NO];
            [self.textShared setHidden:YES];
            AVFile * file = [AVFile fileWithURL:self.message.targetContent];
            [file getThumbnail:YES width:45 height:45 withBlock:^(UIImage * image, NSError *error) {
                [self.imgShared setImage:image];
            }];
        }
            break;
        case 4:
        {
            [self.favBtn setHidden:YES];
            [self.imgShared setHidden:YES];
            [self.textShared setHidden:NO];
            [self.textShared setText:self.message.targetContent];
        }
            break;
        case 5:
        {
            [self.favBtn setHidden:YES];
            [self.imgShared setHidden:YES];
            [self.textShared setHidden:YES];
        }
            break;
            
        default:
            break;
    }
    
}

+ (CGSize)sizeWithData:(id)data{
    NotiMessage *message = data;
    //计算文字高度
    CGFloat textSize = [message.content boundingRectWithSize:CGSizeMake(SCREEN_WIDTH-122,CGFLOAT_MAX)
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:[UIFont fontWithName:@"HiraKakuProN-W3" size:12]}
                                                     context:nil].size.height;
    return CGSizeMake(0, 60+textSize);
}

-(IBAction)onFav{
    if (self.message.relation == 0){
        //关注
        [[AVUser currentUser] follow:self.message.user.objectId andCallback:^(BOOL succeeded, NSError *error) {
            if(succeeded){
                //查询用户之间的所属关系
                AVQuery *queryfr = [AVUser currentUser].followerQuery;
                [queryfr includeKey:@"follower"];
                [queryfr whereKey:@"follower" equalTo:self.message.user];
                if ([queryfr getFirstObject]) {
                    self.message.relation = 2;
                    [self.favBtn setImage:[UIImage imageNamed:@"fav_too"] forState:UIControlStateNormal];
                }
                else{
                    [self.favBtn setImage:[UIImage imageNamed:@"fav_yes"] forState:UIControlStateNormal];
                    self.message.relation = 1;
                }
                [TSMessage showNotificationWithTitle:@"关注成功" type:TSMessageNotificationTypeSuccess];
                
                //发送给指定对象通知
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                [dic setObject:self.message.user.objectId forKey:@"objectId"];
                [dic setObject:@"关注了你" forKey:@"content"];
                [dic setObject:@"0" forKey:@"statusType"];
                [dic setObject:@"0" forKey:@"messageType"];
                [[UserManager shared] addUserMessage:dic success:^(id objects){
                    if ([objects[@"success"] boolValue]) {
                    }
                }failure:^(NSError *error){
                    
                }];
                
                //发送给全体粉丝对象通知
                NSMutableDictionary *dicAll = [[NSMutableDictionary alloc] init];
                NSString *content = [NSString stringWithFormat:@"关注了 %@",self.message.user.username];
                [dicAll setObject:content forKey:@"content"];
                [dicAll setObject:self.message.user.objectId forKey:@"targetId"];
                [dicAll setObject:@"user" forKey:@"targetClass"];
                [dicAll setObject:@"0" forKey:@"messageType"];
                [dicAll setObject:@"" forKey:@"targetContent"];
                
                [[UserManager shared] pushMessageToFans:dicAll success:^(id objects){
                    if ([objects[@"success"] boolValue]) {
                        
                    }
                }failure:^(NSError *error){
                    
                }];
            }
            else{
                [TSMessage showNotificationWithTitle:@"关注失败，请稍后再试" type:TSMessageNotificationTypeError];
            }
        }];
    }
    else{
        [[AVUser currentUser] unfollow:self.message.user.objectId andCallback:^(BOOL succeeded, NSError *error) {
            if(succeeded){
                //查询用户之间的所属关系
                [self.favBtn setImage:[UIImage imageNamed:@"fav_no"] forState:UIControlStateNormal];
                self.message.relation = 0;
                [TSMessage showNotificationWithTitle:@"取消关注" type:TSMessageNotificationTypeWarning];
            }
            else{
                [TSMessage showNotificationWithTitle:@"取消关注失败，请稍后再试" type:TSMessageNotificationTypeError];
            }
        }];
    }
}

@end
