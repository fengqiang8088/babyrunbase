//
//  LikeViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-25.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "LikeViewController.h"

#import "PublicManager.h"
#import "LikeDetailTableViewCell.h"
#import "BRTableView.h"
#import "UIImageView+WebCache.h"
#import "PicIndexManager.h"
#import "UserManager.h"
#import "CommunityIndexManager.h"
#import "EducationIndexManager.h"

@interface LikeViewController ()<UITableViewDelegate,BRTableViewDelegate>

@property(nonatomic,strong) IBOutlet UILabel *titleLabel;
@property(nonatomic,strong) IBOutlet BRTableView *tableView;
@property(nonatomic,strong) IBOutlet UIImageView *userHeader;
@property(nonatomic,strong) IBOutlet UILabel *userName;
@property(nonatomic,strong) IBOutlet UIButton *likeBtn;
@property(nonatomic,strong) IBOutlet UILabel *countLabel;

@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;
@property(nonatomic,assign) BOOL isLike;

@end

@implementation LikeViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.tableView setBRTableViewDelegate:self];
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    [self loadData];
    [self updateUI];
}

-(void)loadData{
    [self.parm setObject:@"0" forKey:@"skip"];
    [self.parm setObject:self.objectClass forKey:@"objectClass"];
    [self.parm setObject:self.objectKey forKey:@"objectKey"];
    [self.parm setObject:self.objectValue forKey:@"objectValue"];
    [[PublicManager shared] getLikeAllList:self.parm
                                      success:^(id objects){
                                          if ([objects[@"isLiked"] isEqualToString:@"YES"]) {
                                              self.isLike = YES;
                                          }
                                          else{
                                              self.isLike = NO;
                                          }
                                          [self updateLikeBtn];
                                          NSNumber *count = objects[@"likeCount"];
                                          [self.countLabel setText:[NSString stringWithFormat:@"%@人已赞",count]];
                                          self.result =  objects[@"userList"];                                          // 刷新表格
                                          [self.tableView reloadData];
                                          [self.tableView headerEndRefreshing];
                                          self.likeBtn.userInteractionEnabled = YES;
                                      }failure:^(NSError *error){
                                          self.likeBtn.userInteractionEnabled = YES;
                                      }];
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.result.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LikeDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[LikeDetailTableViewCell reuseIdentifier]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.data = self.result[indexPath.row];
    [cell updateUI];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)refreshTable:(BRTableView *)tableview{
    [self.parm setObject:@"0" forKey:@"skip"];
    [[PublicManager shared] getLikeAllList:self.parm
                                      success:^(id objects){
                                          [self.result removeAllObjects];
                                          self.result = objects;
                                          // 刷新表格
                                          [tableview reloadData];
                                          [tableview headerEndRefreshing];
                                      }failure:^(NSError *error){
                                          [tableview headerEndRefreshing];
                                      }];
}

- (void)loadMoreDataTable:(BRTableView *)tableview{
    [self.parm setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.result.count] forKey:@"skip"];
    [[PublicManager shared] getLikeAllList:self.parm
                                      success:^(id objects){
                                          for (NSDictionary *dict in objects) {
                                              [self.result addObject:dict];
                                          }
                                          [tableview reloadData];
                                          [tableview footerEndRefreshing];
                                      }failure:^(NSError *error){
                                          [tableview footerEndRefreshing];
                                      }];
}

- (void)updateLikeBtn{
    if (self.isLike) {
        [self.likeBtn setImage:[UIImage imageNamed:@"like_btn"] forState:UIControlStateNormal];
    }
    else{
        [self.likeBtn setImage:[UIImage imageNamed:@"unlike_btn"] forState:UIControlStateNormal];
    }
}

- (void)updateUI{
    //设置用户名称
    [self.userName setText:[AVUser currentUser].username];
    
    //设置用户头像
    [self.userHeader sd_setImageWithURL:[NSURL URLWithString:((AVFile *)[AVUser currentUser][@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
    self.likeBtn.userInteractionEnabled = YES;
}

-(IBAction)onLike{
    self.likeBtn.userInteractionEnabled = NO;
    if (self.likeType == 1) {
        ImgShared *imgShared = (ImgShared *)self.object;
        //添加赞
        if (!self.isLike) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:3];
            [dic setObject:imgShared.objectId forKey:@"sharedId"];
            [dic setObject:[AVUser currentUser].objectId forKey:@"userId"];
            [[PicIndexManager shared] addImgSharedLike:dic
                                               success:^(id objects){
                                                   if (objects!=nil) {
                                                       self.isLike = YES;
                                
                                                       //发送给特定对象通知
                                                       NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                                                       [dic setObject:imgShared.user.objectId forKey:@"objectId"];
                                                       [dic setObject:@"赞了你的图片" forKey:@"content"];
                                                       [dic setObject:@"1" forKey:@"statusType"];
                                                       [dic setObject:imgShared.objectId forKey:@"targetId"];
                                                       [dic setObject:@"imgShared" forKey:@"targetClass"];
                                                       [dic setObject:@"3" forKey:@"messageType"];
                                                       [dic setObject:imgShared.imgUrl forKey:@"targetContent"];
                                                       [dic setObject:imgShared.objectId forKey:@"targetId"];
                                                       
                                                       [[UserManager shared] addUserMessage:dic success:^(id objects){
                                                           if ([objects[@"success"] boolValue]) {
                                                           }
                                                       }failure:^(NSError *error){
                                                           
                                                       }];
                                                       
                                                       //发送给全体粉丝对象通知
                                                       NSMutableDictionary *dicAll = [[NSMutableDictionary alloc] init];
                                                       NSString *content = [NSString stringWithFormat:@"赞了 %@ 的图片",imgShared.user.username];
                                                       [dicAll setObject:content forKey:@"content"];
                                                       [dicAll setObject:imgShared.objectId forKey:@"targetId"];
                                                       [dicAll setObject:@"imgShared" forKey:@"targetClass"];
                                                       [dicAll setObject:@"3" forKey:@"messageType"];
                                                       [dicAll setObject:imgShared.imgUrl forKey:@"targetContent"];
                                                       [dicAll setObject:imgShared.objectId forKey:@"targetId"];
                                                       
                                                       [[UserManager shared] pushMessageToFans:dicAll success:^(id objects){
                                                           if ([objects[@"success"] boolValue]) {
                                                               [self loadData];
                                                           }
                                                       }failure:^(NSError *error){
                                                           
                                                       }];
                                                   }
                                               }failure:^(NSError *error){
                                                   
                                               }];
        }
        //取消赞
        else{
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:3];
            [dic setObject:imgShared.objectId forKey:@"sharedId"];
            [dic setObject:[AVUser currentUser].objectId forKey:@"userId"];
            [[PicIndexManager shared] removeImgSharedLike:dic
                                                  success:^(id objects){
                                                      self.isLike = YES;
                                                      [self loadData];
                                                  }failure:^(NSError *error){
                                                      
                                                  }];
        }
    }
    else if (self.likeType == 2){
        TextShared *textShared = (TextShared *)self.object;
        //添加赞
        if (!self.isLike) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:3];
            [dic setObject:textShared.objectId forKey:@"sharedId"];
            [dic setObject:[AVUser currentUser].objectId forKey:@"userId"];
            [[CommunityIndexManager shared] addTextSharedLike:dic
                                                      success:^(id objects){
                                                          if (objects!=nil) {
                                                              self.isLike = YES;
                                                              //发送给特定对象通知
                                                              NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                                                              [dic setObject:textShared.user.objectId forKey:@"objectId"];
                                                              [dic setObject:@"赞了你的文字帖" forKey:@"content"];
                                                              [dic setObject:@"1" forKey:@"statusType"];
                                                              [dic setObject:textShared.objectId forKey:@"targetId"];
                                                              [dic setObject:@"textShared" forKey:@"targetClass"];
                                                              [dic setObject:@"4" forKey:@"messageType"];
                                                              [dic setObject:textShared.sharedTitle forKey:@"targetContent"];
                                                              [dic setObject:textShared.objectId forKey:@"targetId"];
                                                              
                                                              [[UserManager shared] addUserMessage:dic success:^(id objects){
                                                                  if ([objects[@"success"] boolValue]) {
                                                                      
                                                                  }
                                                              }failure:^(NSError *error){
                                                                  
                                                              }];
                                                              
                                                              //发送给全体粉丝对象通知
                                                              NSMutableDictionary *dicAll = [[NSMutableDictionary alloc] init];
                                                              NSString *content = [NSString stringWithFormat:@"赞了 %@ 的文字帖",textShared.user.username];
                                                              [dicAll setObject:content forKey:@"content"];
                                                              [dicAll setObject:textShared.objectId forKey:@"targetId"];
                                                              [dicAll setObject:@"textShared" forKey:@"targetClass"];
                                                              [dicAll setObject:@"4" forKey:@"messageType"];
                                                              [dicAll setObject:textShared.sharedTitle forKey:@"targetContent"];
                                                              [dicAll setObject:textShared.objectId forKey:@"targetId"];
                                                              
                                                              [[UserManager shared] pushMessageToFans:dicAll success:^(id objects){
                                                                  if ([objects[@"success"] boolValue]) {
                                                                      
                                                                  }
                                                              }failure:^(NSError *error){
                                                                  
                                                              }];
                                                              [self loadData];
                                                              
                                                          }
                                                      }failure:^(NSError *error){
                                                          
                                                      }];
        }
        //取消赞
        else{
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:3];
            [dic setObject:textShared.objectId forKey:@"sharedId"];
            [dic setObject:[AVUser currentUser].objectId forKey:@"userId"];
            [[CommunityIndexManager shared] removeTextSharedLike:dic
                                                         success:^(id objects){
                                                             self.isLike = NO;
                                                             [self loadData];
                                                         }failure:^(NSError *error){
                                                             NSLog(@"error");
                                                         }];
        }
    }
    else if (self.likeType == 3){
        FeederDetail *feederDetail = (FeederDetail *)self.object;
        //添加赞
        if (!self.isLike) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:1];
            [dic setObject:feederDetail.objectId forKey:@"articleId"];
            [[EducationIndexManager shared] addFeederLike:dic
                                                  success:^(id objects){
                                                      if (objects!=nil) {
                                                          self.isLike = YES;
                                                      }
                                                      [self loadData];
                                                  }failure:^(NSError *error){
                                                      
                                                  }];
        }
        //取消赞
        else{
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:1];
            [dic setObject:feederDetail.objectId forKey:@"articleId"];
            [[EducationIndexManager shared] removeFeederLike:dic
                                                     success:^(id objects){
                                                         self.isLike = NO;
                                                         [self loadData];
                                                     }failure:^(NSError *error){
                                                         
                                                     }];
        }
    }
    else if (self.likeType == 4){
        Activity *activity = (Activity *)self.object;
        //添加赞
        if (!self.isLike) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:1];
            [dic setObject:activity.objectId forKey:@"taskId"];
            [[EducationIndexManager shared] addActivityLike:dic
                                                    success:^(id objects){
                                                        if (objects!=nil) {
                                                            self.isLike = YES;
                                                        }
                                                        [self loadData];
                                                    }failure:^(NSError *error){
                                                        
                                                    }];
        }
        //取消赞
        else{
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:1];
            [dic setObject:activity.objectId forKey:@"taskId"];
            [[EducationIndexManager shared] removeActivityLike:dic
                                                       success:^(id objects){
                                                           self.isLike = NO;
                                                           [self loadData];
                                                       }failure:^(NSError *error){
                                                           
                                                       }];
        }
    }
}

@end
