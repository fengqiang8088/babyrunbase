//
//  LikeViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-12-25.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

//赞列表页面
#import "ImgShared.h"
#import "TextShared.h"
#import "FeederDetail.h"
#import "Activity.h"

@interface LikeViewController : BaseViewController

@property(nonatomic,strong) NSString *objectClass;
@property(nonatomic,strong) NSString *objectKey;
@property(nonatomic,strong) NSString *objectValue;
@property(nonatomic,assign) NSInteger likeType; //1:imgShared,2:textShared,3:feeder,4:activity
@property(nonatomic,strong) id object;

@end
