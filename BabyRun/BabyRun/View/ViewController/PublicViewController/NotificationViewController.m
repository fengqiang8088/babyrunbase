//
//  NotificationViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-25.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "NotificationViewController.h"

#import "UserManager.h"
#import "BRTableView.h"
#import "UserViewController.h"
#import "NotiMessageTableViewCell.h"
#import "PicDetailViewController.h"
#import "CommunityDetailViewController.h"
#import "PicIndexManager.h"
#import "CommunityIndexManager.h"

@interface NotificationViewController ()<UITableViewDelegate,BRTableViewDelegate>

@property(nonatomic,strong) IBOutlet BRTableView *tableView;
@property(nonatomic,strong) NSMutableArray *resultMessage;
@property(nonatomic,strong) NSMutableArray *resultLike;
@property(nonatomic,strong) NSMutableArray *resultDynamic;
@property(nonatomic,strong) NSMutableDictionary *parmMessage;
@property(nonatomic,strong) NSMutableDictionary *parmLike;
@property(nonatomic,strong) NSMutableDictionary *parmDynamic;
@property(nonatomic,assign) int showType; //0:message,1:like,2:dynamic

@property(nonatomic,strong) IBOutlet UIButton *messageBtn;
@property(nonatomic,strong) IBOutlet UIButton *likeBtn;
@property(nonatomic,strong) IBOutlet UIButton *dynamicBtn;

@property(nonatomic,strong) IBOutlet UIImageView *messageBtnCount;
@property(nonatomic,strong) IBOutlet UIImageView *likeBtnCount;

@end

@implementation NotificationViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    [AVStatus getUnreadStatusesCountWithType:@"BabyRunMessage" andCallback:^(NSInteger number1, NSError *error) {
        if (!error) {
            if (number1 > 0) {
                [self.messageBtnCount setHidden:NO];
            }
        } else {
            [self.messageBtnCount setHidden:YES];
        }
    }];
    
    [AVStatus getUnreadStatusesCountWithType:@"BabyRunLike" andCallback:^(NSInteger number2, NSError *error) {
        if (!error) {
            if (number2 > 0) {
                [self.likeBtnCount setHidden:NO];
            }
        } else {
            [self.likeBtnCount setHidden:YES];
        }
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initSetup];
}

- (void)initSetup{
    self.parmMessage = [[NSMutableDictionary alloc] init];
    self.parmLike = [[NSMutableDictionary alloc] init];
    self.parmDynamic = [[NSMutableDictionary alloc] init];
    self.resultMessage = [[NSMutableArray alloc] init];
    self.resultLike = [[NSMutableArray alloc] init];
    self.resultDynamic = [[NSMutableArray alloc] init];
    
    [self.navigationController setNavigationBarHidden:YES];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    [self.tableView setBRTableViewDelegate:self];
    [self.tableView setFooterHidden:YES];
    
    [self.messageBtn setSelected:YES];
    [self loadDataMessage];
}

-(void)loadDataMessage{
    [self showLoading];
    self.showType = 0;
    [self.parmMessage setObject:@"0" forKey:@"skip"];
    [[UserManager shared] getUserMessageList:self.parmMessage
                                        success:^(id objects){
                                            [self.resultMessage removeAllObjects];
                                            self.resultMessage = objects;
                                            // 刷新表格
                                            [self.tableView reloadData];
                                            [self.messageBtnCount setHidden:YES];
                                            [self hideLoading];
                                        }failure:^(NSError *error){
                                            [self hideLoading];
                                        }];
    
}

-(void)loadDataLike{
    [self showLoading];
    self.showType = 1;
    [self.parmLike setObject:@"0" forKey:@"skip"];
    [[UserManager shared] getUserLikeList:self.parmLike
                                     success:^(id objects){
                                         [self.resultLike removeAllObjects];
                                         self.resultLike = objects;
                                         // 刷新表格
                                         [self.tableView reloadData];
                                         [self.likeBtnCount setHidden:YES];
                                         [self hideLoading];
                                     }failure:^(NSError *error){
                                         [self hideLoading];
                                     }];
    
}

-(void)loadDataDynamic{
    [self showLoading];
    self.showType = 2;
    [self.parmDynamic setObject:@"0" forKey:@"skip"];
    [[UserManager shared] getUserDynamicList:self.parmDynamic
                                     success:^(id objects){
                                         [self.resultDynamic removeAllObjects];
                                         self.resultDynamic = objects;
                                         // 刷新表格
                                         [self.tableView reloadData];
                                         [self hideLoading];
                                     }failure:^(NSError *error){
                                         [self hideLoading];
                                     }];
    
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (self.showType) {
        case 0:
            return self.resultMessage.count;
            break;
        case 1:
            return self.resultLike.count;
            break;
        case 2:
            return self.resultDynamic.count;
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (self.showType) {
        case 0:
            return [NotiMessageTableViewCell sizeWithData:self.resultMessage[indexPath.row]].height;
            break;
        case 1:
            return [NotiMessageTableViewCell sizeWithData:self.resultLike[indexPath.row]].height;
            break;
        case 2:
            return [NotiMessageTableViewCell sizeWithData:self.resultDynamic[indexPath.row]].height;
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (self.showType) {
        case 0:
        {
            NotiMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NotiMessageTableViewCell reuseIdentifier]];
            cell.message = self.resultMessage[indexPath.row];
            [cell updateUI];
            return cell;
        }
            break;
        case 1:
        {
            NotiMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NotiMessageTableViewCell reuseIdentifier]];
            cell.message = self.resultLike[indexPath.row];
            [cell updateUI];
            return cell;
        }
            break;
        case 2:
        {
            NotiMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NotiMessageTableViewCell reuseIdentifier]];
            cell.message = self.resultDynamic[indexPath.row];
            [cell updateUI];
            return cell;
        }
            break;
            
        default:
            return nil;
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NotiMessage *message = self.resultMessage[indexPath.row];
    //0:关注用户,1:评论图片,2:评论文字,3:赞图片,4:赞文字,5:系统消息
    if(message.type == 0){
        UserViewController *userViewController = [UserViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserStoryboard"];
        userViewController.user = message.user;
        userViewController.showTypeUser = 1;
        [self.navigationController pushViewController:userViewController animated:YES];
    }
    else if(message.type == 1 || message.type == 3){
        PicDetailViewController *picDetailViewController = [PicDetailViewController brCreateFor:@"PicIndexStoryboard" andClassIdentifier:@"PicDetailViewController"];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:message.targetId forKey:@"objectId"];
        [[PicIndexManager shared] getPicIndexList:dic success:^(id objects) {
            picDetailViewController.imgShared = objects[0];
            [self.navigationController pushViewController:picDetailViewController animated:YES];
        } failure:^(NSError *error) {
            
        }];
    }
    else if(message.type == 2 || message.type == 4){
        CommunityDetailViewController *communityDetailViewController = [CommunityDetailViewController brCreateFor:@"CommunityStoryboard" andClassIdentifier:@"CommunityDetailViewController"];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:message.targetId forKey:@"objectId"];
        [[CommunityIndexManager shared] getCommunityIndexList:dic success:^(id objects) {
            communityDetailViewController.textShared = objects[0];
            [self.navigationController pushViewController:communityDetailViewController animated:YES];
        } failure:^(NSError *error) {
        }];
    }
}

-(IBAction)onChange:(id)sender{
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag) {
        case 0:{
            [self.messageBtn setSelected:YES];
            [self.likeBtn setSelected:NO];
            [self.dynamicBtn setSelected:NO];
            [self loadDataMessage];
        }
            break;
        case 1:{
            [self.messageBtn setSelected:NO];
            [self.likeBtn setSelected:YES];
            [self.dynamicBtn setSelected:NO];
            [self loadDataLike];
        }
            break;
        case 2:{
            [self.messageBtn setSelected:NO];
            [self.likeBtn setSelected:NO];
            [self.dynamicBtn setSelected:YES];
            [self loadDataDynamic];
        }
        default:
            break;
    }
}

- (void)refreshTable:(BRTableView *)tableview{
    switch (self.showType) {
        case 0:{
            [self.parmMessage setObject:@"0" forKey:@"skip"];
            [[UserManager shared] getUserMessageList:self.parmMessage
                                             success:^(id objects){
                                                 [self.resultMessage removeAllObjects];
                                                 self.resultMessage = objects;
                                                 // 刷新表格
                                                 [self.tableView reloadData];
                                                 [self.tableView headerEndRefreshing];
                                             }failure:^(NSError *error){
                                                 [self.tableView headerEndRefreshing];
                                             }];
        }
            break;
        case 1:{
            [self.parmLike setObject:@"0" forKey:@"skip"];
            [[UserManager shared] getUserLikeList:self.parmLike
                                          success:^(id objects){
                                              [self.resultLike removeAllObjects];
                                              self.resultLike = objects;
                                              // 刷新表格
                                              [self.tableView reloadData];
                                              [self.tableView headerEndRefreshing];
                                          }failure:^(NSError *error){
                                              [self.tableView headerEndRefreshing];
                                          }];
        }
            break;
        case 2:{
            [self.parmDynamic setObject:@"0" forKey:@"skip"];
            [[UserManager shared] getUserDynamicList:self.parmDynamic
                                             success:^(id objects){
                                                 [self.resultDynamic removeAllObjects];
                                                 self.resultDynamic = objects;
                                                 // 刷新表格
                                                 [self.tableView reloadData];
                                                 [self.tableView headerEndRefreshing];
                                             }failure:^(NSError *error){
                                                 [self.tableView headerEndRefreshing];
                                             }];
        }
        default:
            break;
    }
}
- (void)loadMoreDataTable:(BRTableView *)tableview{
    switch (self.showType) {
        case 0:{
            [self.parmMessage setObject:self.resultMessage.lastObject forKey:@"maxid"];
            [[UserManager shared] getUserMessageList:self.parmMessage
                                             success:^(id objects){
                                                 for (NSDictionary *dict in objects) {
                                                     [self.resultMessage addObject:dict];
                                                 }
                                                 // 刷新表格
                                                 [self.tableView reloadData];
                                                 [self.tableView footerEndRefreshing];
                                             }failure:^(NSError *error){
                                                 [self.tableView footerEndRefreshing];
                                             }];
        }
            break;
        case 1:{
            [self.parmLike setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.resultLike.count] forKey:@"skip"];
            [[UserManager shared] getUserLikeList:self.parmLike
                                          success:^(id objects){
                                              for (NSDictionary *dict in objects) {
                                                  [self.resultLike addObject:dict];
                                              }
                                              // 刷新表格
                                              [self.tableView reloadData];
                                              [self.tableView footerEndRefreshing];
                                          }failure:^(NSError *error){
                                              [self.tableView footerEndRefreshing];
                                          }];
        }
            break;
        case 2:{
            [self.parmDynamic setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.resultDynamic.count] forKey:@"skip"];
            [[UserManager shared] getUserDynamicList:self.parmDynamic
                                             success:^(id objects){
                                                 for (NSDictionary *dict in objects) {
                                                     [self.resultDynamic addObject:dict];
                                                 }
                                                 // 刷新表格
                                                 [self.tableView reloadData];
                                                 [self.tableView footerEndRefreshing];
                                             }failure:^(NSError *error){
                                                 [self.tableView footerEndRefreshing];
                                             }];
        }
        default:
            break;
    }
}

@end
