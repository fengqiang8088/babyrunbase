//
//  CommunityViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-1.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "CommunityViewController.h"
#import "SCNavTabBarController.h"
#import "CommunityContentViewController.h"
#import "CommunityIndexManager.h"
#import "NotificationViewController.h"
#import "IndexViewController.h"
#import "UserManager.h"

@interface CommunityViewController () <SCNavTabBarControllerDelegate>

@property (nonatomic,strong) SCNavTabBarController *navTabBarController;

@end

@implementation CommunityViewController

- (void)viewWillAppear:(BOOL)animated{
    [[self rdv_tabBarController] setTabBarHidden:NO animated:YES];
    [[UserManager shared] getUserNotReadMessageCount:^(NSInteger number) {
        [self.navTabBarController setNotiMessageCount:number>0?:0];
    } failure:^(NSError *error) {
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initSetup];
    [self initSegmentBar];
}

- (void)initSetup{
    [self.navigationController setNavigationBarHidden:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSegmentBar) name:BRNOTI_UPDATE_GZ object:nil];
}

-(void)initSegmentBar{
    self.navTabBarController = [[SCNavTabBarController alloc] init];
    [[CommunityIndexManager shared] getTextGroupList:nil success:^(id objects){
        NSMutableArray *segmentArray = [[NSMutableArray alloc] init];
        
        //添加全部界面
        CommunityContentViewController *childViewController1 = [CommunityContentViewController brCreateFor:@"CommunityStoryboard" andClassIdentifier:@"CommunityContentViewController"];
        childViewController1.title = @"全部";
        childViewController1.navTabBarController = self.navTabBarController;
        [segmentArray addObject:childViewController1];
        
        if([AVUser currentUser]!=nil){
            //添加关注界面
            CommunityContentViewController *childViewController2 = [CommunityContentViewController brCreateFor:@"CommunityStoryboard" andClassIdentifier:@"CommunityContentViewController"];
            childViewController2.title = @"关注";
            childViewController2.navTabBarController = self.navTabBarController;
            childViewController2.isFav = @"YES";
            [segmentArray addObject:childViewController2];
        }
        
        for (AVObject *object in objects) {
            CommunityContentViewController *childViewController = [CommunityContentViewController brCreateFor:@"CommunityStoryboard" andClassIdentifier:@"CommunityContentViewController"];
            childViewController.title = object[@"textGroupName"];
            childViewController.navTabBarController = self.navTabBarController;
            childViewController.textGroup = object[@"textGroup"];
            [segmentArray addObject:childViewController];
        }
        if (segmentArray.count>0) {
            ((CommunityContentViewController *)segmentArray[0]).isFirstLoad = YES;
        }
        
        self.navTabBarController.scNavDelegate = self;
        self.navTabBarController.subViewControllers = segmentArray;
        self.navTabBarController.showArrowButton = YES;
        self.navTabBarController.scrollAnimation = YES;
        [self.navTabBarController addParentController:self];
    }failure:^(NSError *error){
        
    }];
}

-(void)scrollViewEnd:(NSInteger *)index{
    [(CommunityContentViewController *)self.navTabBarController.subViewControllers[*index] loadData];
}

-(void)arrowBtnClick{
    if([AVUser currentUser]!=nil){
        NotificationViewController *viewController = [NotificationViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"NotificationViewController"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else{
        IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
        indexViewController.fromType = 1;
        UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                       initWithRootViewController:indexViewController];
        [self presentViewController:indexNavigationController animated:YES completion:nil];
    }
}

-(void)updateSegmentBar{
    [self.navTabBarController removeFromParentViewController];
    [self.navTabBarController.view removeFromSuperview];
    [self initSegmentBar];
}

@end
