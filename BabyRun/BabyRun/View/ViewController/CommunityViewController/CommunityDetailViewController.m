//
//  CommunityDetailViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-25.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "CommunityDetailViewController.h"

#import "CommunityIndexManager.h"
#import "CommunityTableViewCell.h"
#import "TextShared.h"
#import "CommentTableViewCell.h"
#import "LikeIndexTableViewCell.h"
#import "BRTableView.h"
#import "TagIndexViewController.h"
#import "UserViewController.h"
#import "LikeViewController.h"
#import "HPGrowingTextView.h"
#import "UIView+BTPosition.h"
#import "IndexViewController.h"
#import "UserManager.h"
#import "PublicManager.h"


@interface CommunityDetailViewController ()<UITableViewDelegate,CommunityTableViewCellDelegate,HPGrowingTextViewDelegate,UIActionSheetDelegate>

@property(nonatomic,strong) IBOutlet UITableView *tableView;
@property(nonatomic,strong) IBOutlet HPGrowingTextView *inputView;
@property(nonatomic,strong) IBOutlet NSLayoutConstraint *inputViewSuperViewConstraintsHeight;
@property(nonatomic,strong) IBOutlet NSLayoutConstraint *inputViewBottomViewConstraints;
@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableArray *resultLike;
@property(nonatomic,strong) NSMutableDictionary *parm;
@property(nonatomic,strong) IBOutlet UIView *commentView;


@end

@implementation CommunityDetailViewController

bool isReply;           //记录是否是回复
Comment *selectComment;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initSetup];
}

- (void)initSetup{
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    self.resultLike = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    
    [self configInputView];
    [self loadData];
}

- (void)configInputView {
    self.inputView.minNumberOfLines = 1;
    self.inputView.maxNumberOfLines = 3;
    self.inputView.returnKeyType = UIReturnKeyDefault;
    self.inputView.font = [UIFont fontWithName:@"HiraKakuProN-W3" size:12];
    self.inputView.textColor = BRGRAYColor;
    self.inputView.internalTextView.contentInset = UIEdgeInsetsMake(9, 0, 9, 0);
    self.inputView.internalTextView.backgroundColor = [UIColor whiteColor];
    [self.inputView.layer setCornerRadius:10];
    [self.inputView.internalTextView.layer setCornerRadius:10];
    self.inputView.delegate = self;
}

-(void)loadData{
    [self.parm setObject:@"0" forKey:@"skip"];
    [self.parm setObject:self.textShared.objectId forKey:@"objectId"];
    
    [[CommunityIndexManager shared] getCommunityCommentList:self.parm
                                        success:^(id objects){
                                            self.result = objects;
                                            // 刷新表格
                                            [self.tableView reloadData];
                                            [self.inputView resignFirstResponder];
                                        }failure:^(NSError *error){
                                            
                                        }];
    
    [[CommunityIndexManager shared] getCommunityLikeIndex:self.parm
                                      success:^(id objects){
                                          self.resultLike = objects;
                                          // 刷新表格
                                          [self.tableView reloadData];
                                          
                                      }failure:^(NSError *error){
                                          
                                      }];
    
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.result.count+2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            return 320;
            break;
        case 1:
            return 60;
            break;
        default:
            return [CommentTableViewCell sizeWithData:self.result[indexPath.row-2]].height;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
        {
            CommunityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[CommunityTableViewCell reuseIdentifier]];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.textShared = self.textShared;
            cell.delegate = self;
            [cell updateUI];
            return cell;
        }
            break;
        case 1:
        {
            LikeIndexTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[LikeIndexTableViewCell reuseIdentifier]];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.likeUser = self.resultLike;
            [cell updateUI];
            return cell;
        }
            break;
            
        default:
        {
            CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[CommentTableViewCell reuseIdentifier]];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.comment = self.result[indexPath.row-2];
            [cell.floor setText:[NSString stringWithFormat:@"%d 楼",(int)indexPath.row-1]];
            [cell updateUI];
            return cell;
            
        }
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        if([AVUser currentUser]!=nil){
            LikeViewController *viewController = [LikeViewController brCreateFor:@"PublicStoryboard" andClassIdentifier:@"LikeViewController"];
            viewController.objectClass = @"textPraise";
            viewController.objectKey = @"sharedId";
            viewController.objectValue = self.textShared.objectId;
            viewController.object = self.textShared;
            viewController.likeType = 2;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else{
            [self onLogin];
        }
    }
    else if(indexPath.row > 1){
        selectComment = self.result[indexPath.row-2];
        if ([selectComment.user.objectId isEqual:[AVUser currentUser].objectId]) {
            UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                          initWithTitle:nil
                                          delegate:self
                                          cancelButtonTitle:@"取消"
                                          destructiveButtonTitle:@"删除"
                                          otherButtonTitles:nil];
            actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
            actionSheet.tag = 0;
            [actionSheet showInView:self.view];
        }
        else{
            UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                          initWithTitle:nil
                                          delegate:self
                                          cancelButtonTitle:@"取消"
                                          destructiveButtonTitle:@"举报"
                                          otherButtonTitles:nil];
            actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
            actionSheet.tag = 1;
            [actionSheet showInView:self.view];
        }
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        if(actionSheet.tag == 0){
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:selectComment.objectId forKey:@"objectId"];
            [dic setObject:self.textShared.objectId forKey:@"sharedId"];
            //删除评论
            [[CommunityIndexManager shared] removeTextSharedComment:dic success:^(id objects) {
                [TSMessage showNotificationWithTitle:@"删除成功" type:TSMessageNotificationTypeSuccess];
                [self loadData];
            } failure:^(NSError *error) {
                
            }];
        }
        else if (actionSheet.tag ==1){
            //举报评论
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[NSNumber numberWithInt:3] forKey:@"reportType"];
            [dic setObject:selectComment.objectId forKey:@"commentId"];
            [dic setObject:self.textShared.objectId forKey:@"sharedId"];
            [[PublicManager shared] addReport:dic success:^(id objects) {
                [TSMessage showNotificationWithTitle:@"举报成功" type:TSMessageNotificationTypeSuccess];
            } failure:^(NSError *error) {
                [TSMessage showNotificationWithTitle:@"举报失败" type:TSMessageNotificationTypeError];
            }];
        }
    }
}

#pragma mark - CommunityTableCellDelegate

- (void)selectUser:(AVUser *)user{
    UserViewController *userViewController = [UserViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserStoryboard"];
    userViewController.user = user;
    userViewController.showTypeUser = 1;
    [self.navigationController pushViewController:userViewController animated:YES];
}

- (void)selectComment:(TextShared *)textShared{
    
}

-(void)selectMore:(TextShared *)textShared{
    if([AVUser currentUser]!=nil){
        [self showShareView:textShared];
    }
    else{
        [self onLogin];
    }
}

-(void)onLogin{
    IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
    indexViewController.fromType = 1;
    UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:indexViewController];
    [self presentViewController:indexNavigationController animated:YES completion:nil];
}

-(IBAction)onSendMessage{
    NSString *str = [self.inputView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (!str || [str isEqualToString:@""]) {
        [self.inputView resignFirstResponder];
        return;
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:3];
    [dic setObject:self.textShared.objectId forKey:@"sharedId"];
    [dic setObject:str forKey:@"content"];
    [dic setObject:[AVUser currentUser].objectId forKey:@"userId"];
    [[CommunityIndexManager shared] addTextSharedComment:dic
                                          success:^(id objects){
                                              
                                              //发送给指定对象通知
                                              NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                                              [dic setObject:self.textShared.user.objectId forKey:@"objectId"];
                                              [dic setObject:@"评论了我的文字" forKey:@"content"];
                                              [dic setObject:@"0" forKey:@"statusType"];
                                              [dic setObject:@"2" forKey:@"messageType"];
                                              [dic setObject:self.textShared.sharedTitle forKey:@"targetContent"];
                                              [dic setObject:self.textShared.objectId forKey:@"targetId"];
                                              [[UserManager shared] addUserMessage:dic success:^(id objects){
                                                  if ([objects[@"success"] boolValue]) {
                                                  }
                                              }failure:^(NSError *error){
                                                  
                                              }];
                                              
                                              //发送给全体粉丝对象通知
                                              NSMutableDictionary *dicAll = [[NSMutableDictionary alloc] init];
                                              NSString *content = [NSString stringWithFormat:@"评论了 %@ 的文字",self.textShared.user.username];
                                              [dicAll setObject:content forKey:@"content"];
                                              [dicAll setObject:self.textShared.user.objectId forKey:@"targetId"];
                                              [dicAll setObject:@"user" forKey:@"targetClass"];
                                              [dicAll setObject:@"2" forKey:@"messageType"];
                                              [dicAll setObject:self.textShared.sharedTitle forKey:@"targetContent"];
                                              [dicAll setObject:self.textShared.objectId forKey:@"targetId"];
                                              
                                              [[UserManager shared] pushMessageToFans:dicAll success:^(id objects){
                                                  if ([objects[@"success"] boolValue]) {
                                                      
                                                  }
                                              }failure:^(NSError *error){
                                                  
                                              }];
                                              
                                              [self loadData];
                                              [self.inputView setText:@""];
                                          }failure:^(NSError *error){
                                              
                                          }];
    
}

#pragma mark - HPGrowingTextViewDelegate

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height {
    float deltaHeight = height - growingTextView.frame.size.height;
    
    [self.inputViewSuperViewConstraintsHeight setConstant:(self.inputViewSuperViewConstraintsHeight.constant+deltaHeight)];
    [growingTextView.superview adjustFrameY:-deltaHeight];
    [self.tableView adjustFrameHeight:-deltaHeight];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    [self registerKeyboardNotifications];
    isReply = NO;
    if([AVUser currentUser]!=nil){
        [self.commentView setHidden:NO];
    }
    else{
        [self.commentView setHidden:YES];
    }

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self unregisterKeyboardNotifications];
}

- (void)registerKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willResignActive:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)unregisterKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

#pragma mark - Keyboard

static CGRect viewFrameKeyboard = {{0,0},{0,0}};


- (void)keyboardWillShow:(NSNotification*)notification {
    if (!isReply){
        if([self.inputView.text isEqualToString:@""]){
            //self.inputView.textColor = COLOR_TEXT_BLACK;
        }
    }
    
    NSDictionary* info = [notification userInfo];
    CGRect originkeyboardRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardRect = [self.view convertRect:originkeyboardRect fromView:nil];
    
    self.tableView.contentOffset = CGPointMake(0, CGFLOAT_MAX) ;
    [self.inputViewBottomViewConstraints setConstant:keyboardRect.size.height];
}

- (void)keyboardWillHide:(NSNotification*)notification {
    isReply = NO;
    //self.inputView.textColor = COLOR_TEXT_GRAY;
    [self.inputViewBottomViewConstraints setConstant:0];
}

- (void)willResignActive:(NSNotification *)not {
    if(self.inputView.internalTextView.isFirstResponder){
        [UIView animateWithDuration:0.25
                              delay:0
                            options:7
                         animations:^{
                             self.view.frame = viewFrameKeyboard;
                         } completion:NULL];
    }
}

-(void)selectLike:(TextShared *)textShared{
    [[CommunityIndexManager shared] getCommunityLikeIndex:self.parm
                                                  success:^(id objects){
                                                      self.resultLike = objects;
                                                      // 刷新表格
                                                      [self.tableView reloadData];
                                                      
                                                  }failure:^(NSError *error){
                                                      
                                                  }];
}

@end
