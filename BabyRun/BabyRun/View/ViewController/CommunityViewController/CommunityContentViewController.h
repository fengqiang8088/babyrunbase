//
//  CommunityContentViewController.h
//  BabyRun
//
//  Created by fengqiang on 14-12-15.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "SCNavTabBarController.h"

@interface CommunityContentViewController : BaseViewController

@property (nonatomic,strong) NSString *textGroup;
@property (nonatomic,strong) NSString *isFav;
@property (nonatomic,strong) SCNavTabBarController *navTabBarController;

@end
