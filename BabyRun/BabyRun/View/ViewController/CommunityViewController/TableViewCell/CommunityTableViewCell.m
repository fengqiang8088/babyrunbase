//
//  CommunityTableViewCell.m
//  BabyRun
//
//  Created by fengqiang on 14-12-15.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "CommunityTableViewCell.h"
#import "CommunityIndexManager.h"
#import "UIImageView+WebCache.h"
#import "UserManager.h"

@implementation CommunityTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.userHeader.userInteractionEnabled = YES;
    UITapGestureRecognizer *onUserTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onUser)];
    [self.userHeader addGestureRecognizer:onUserTap];
}

- (void)updateUI{
    //判断相关所属关系
    if([AVUser currentUser] != nil){
        //查询是否是喜欢
        AVQuery *query1 = [AVQuery queryWithClassName:@"textPraise"];
        [query1 whereKey:@"sharedId" equalTo:self.textShared.objectId];
        [query1 whereKey:@"userId" equalTo:[AVUser currentUser].objectId];
        [query1 whereKey:@"deleteFlag" equalTo:[NSNumber numberWithInt:0]];
        [query1 getFirstObjectInBackgroundWithBlock:^(AVObject *object, NSError *error) {
            if (object != nil) {
                self.textShared.isLike = YES;
            }
            else{
                self.textShared.isLike = NO;
            }
            //设置喜欢按钮
            [self updateLikeBtn];
        }];
    }
    
    //设置用户发布图片
    UIImage *backgroundImage = [[UIImage imageNamed:self.textShared.textBackground] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0) resizingMode:UIImageResizingModeTile];
    [self.cellBackgroundView setImage:backgroundImage];
    
    //设置用户名称
    [self.userName setText:self.textShared.user.username];
    
    //设置用户头像
    [self.userHeader sd_setImageWithURL:[NSURL URLWithString:((AVFile *)self.textShared.user[@"usericon"]).url] placeholderImage:[UIImage imageNamed:@"userHeader_ph"]];
    
    //设置用户信息
    [self.userInfo setText:self.textShared.user[@"address"]?:@""];
    
    //设置用户图片介绍内容
    //[self.userShineInfo setText:self.textShared.sharedTitle?:@""];
    //[self.userShineInfo shine];
    [self.userContext setText:self.textShared.sharedTitle?:@""];
    
    //设置文字贴类别
    [self.groupName setText:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"textGroup_%d",self.textShared.sharedGroup]]];
    
    //设置浏览数
    [self.readCount setText:[NSString stringWithFormat:@"阅读 %@",self.textShared.readCount?:@"0"]];
    
    //设置评论按钮
    [[CommunityIndexManager shared] getTextSharedCommentCount:@{@"objectId":self.textShared.objectId} success:^(id objects) {
        [self.commentBtn setTitle:[NSString stringWithFormat:@"%d",[objects integerValue]] forState:UIControlStateNormal];
    } failure:^(NSError *error) {
        [self.commentBtn setTitle:[NSString stringWithFormat:@"%@",self.textShared.commentCount?:@"0"] forState:UIControlStateNormal];
    }];
}

-(IBAction)onLike{
    self.likeBtn.userInteractionEnabled = NO;
    if ([AVUser currentUser]!=nil) {
        //添加赞
        if (!self.textShared.isLike) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:3];
            [dic setObject:self.textShared.objectId forKey:@"sharedId"];
            [dic setObject:[AVUser currentUser].objectId forKey:@"userId"];
            [[CommunityIndexManager shared] addTextSharedLike:dic
                                                      success:^(id objects){
                                                          if (objects!=nil) {
                                                              self.textShared.isLike = YES;
                                                              self.textShared.likeCount = [NSNumber numberWithInt:[self.textShared.likeCount intValue]+1];
                                                              
                                                              //发送给特定对象通知
                                                              NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                                                              [dic setObject:self.textShared.user.objectId forKey:@"objectId"];
                                                              [dic setObject:@"赞了你的文字帖" forKey:@"content"];
                                                              [dic setObject:@"1" forKey:@"statusType"];
                                                              [dic setObject:self.textShared.objectId forKey:@"targetId"];
                                                              [dic setObject:@"textShared" forKey:@"targetClass"];
                                                              [dic setObject:@"4" forKey:@"messageType"];
                                                              [dic setObject:self.textShared.sharedTitle forKey:@"targetContent"];
                                                              [dic setObject:self.textShared.objectId forKey:@"targetId"];
                                                              
                                                              [[UserManager shared] addUserMessage:dic success:^(id objects){
                                                                  if ([objects[@"success"] boolValue]) {
                                                                      
                                                                  }
                                                              }failure:^(NSError *error){
                                                                  
                                                              }];
                                                              
                                                              //发送给全体粉丝对象通知
                                                              NSMutableDictionary *dicAll = [[NSMutableDictionary alloc] init];
                                                              NSString *content = [NSString stringWithFormat:@"赞了 %@ 的文字帖",self.textShared.user.username];
                                                              [dicAll setObject:content forKey:@"content"];
                                                              [dicAll setObject:self.textShared.objectId forKey:@"targetId"];
                                                              [dicAll setObject:@"textShared" forKey:@"targetClass"];
                                                              [dicAll setObject:@"4" forKey:@"messageType"];
                                                              [dicAll setObject:self.textShared.sharedTitle forKey:@"targetContent"];
                                                              [dicAll setObject:self.textShared.objectId forKey:@"targetId"];
                                                              
                                                              [[UserManager shared] pushMessageToFans:dicAll success:^(id objects){
                                                                  if ([objects[@"success"] boolValue]) {
                                                                      
                                                                  }
                                                              }failure:^(NSError *error){
                                                                  
                                                              }];
                                                              [self updateLikeBtn];
                                                              if ([self.delegate respondsToSelector:@selector(selectLike:)]) {
                                                                  [self.delegate selectLike:self.textShared];
                                                              }
                                                          }
                                                      }failure:^(NSError *error){
                                                          
                                                      }];
        }
        //取消赞
        else{
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:3];
            [dic setObject:self.textShared.objectId forKey:@"sharedId"];
            [dic setObject:[AVUser currentUser].objectId forKey:@"userId"];
            [[CommunityIndexManager shared] removeTextSharedLike:dic
                                                         success:^(id objects){
                                                             self.textShared.isLike = NO;
                                                             self.textShared.likeCount = [NSNumber numberWithInt:[self.textShared.likeCount intValue]-1];
                                                             [self updateLikeBtn];
                                                             if ([self.delegate respondsToSelector:@selector(selectLike:)]) {
                                                                 [self.delegate selectLike:self.textShared];
                                                             }
                                                         }failure:^(NSError *error){
                                                             NSLog(@"error");
                                                         }];
        }
    }
    else{
        [self.delegate onLogin];
    }
}

-(void)updateLikeBtn{
    if (self.textShared.isLike) {
        [self.likeBtn setImage:[UIImage imageNamed:@"PicIndexCell_like_btn"] forState:UIControlStateNormal];
    }
    else{
        [self.likeBtn setImage:[UIImage imageNamed:@"PicIndexCell_unlike_btn"] forState:UIControlStateNormal];
    }
    [self.likeBtn setTitle:[NSString stringWithFormat:@"%@",self.textShared.likeCount?:@"0"] forState:UIControlStateNormal];
    self.likeBtn.userInteractionEnabled = YES;
}

-(IBAction)onComment{
    [self.delegate selectComment:self.textShared];
}

-(IBAction)onMore{
    [self.delegate selectMore:self.textShared];
}

-(void)onUser{
    [self.delegate selectUser:self.textShared.user];
}

@end
