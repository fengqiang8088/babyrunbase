//
//  CommunityTableViewCell.h
//  BabyRun
//
//  Created by fengqiang on 14-12-15.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "TextShared.h"

@protocol CommunityTableViewCellDelegate <NSObject>

@required
- (void)selectUser:(AVUser *)user;
- (void)selectComment:(TextShared *)textShared;
- (void)selectMore:(TextShared *)textShared;
- (void)onLogin;

@optional
- (void)selectLike:(TextShared *)textShared;

@end

@interface CommunityTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *userHeader;
@property (nonatomic,strong) IBOutlet UIImageView *cellBackgroundView;
@property (nonatomic,strong) IBOutlet UILabel *userName;
@property (nonatomic,strong) IBOutlet UILabel *userInfo;
@property (nonatomic,strong) IBOutlet UILabel *userContext;
@property (nonatomic,strong) IBOutlet UILabel *groupName;
@property (nonatomic,strong) IBOutlet UILabel *readCount;
@property (nonatomic,strong) IBOutlet UIButton *likeBtn;
@property (nonatomic,strong) IBOutlet UIButton *commentBtn;

@property (nonatomic,strong) TextShared *textShared;
@property (nonatomic,weak) id<CommunityTableViewCellDelegate> delegate;

+ (NSString *)reuseIdentifier;
- (void)updateUI;

@end
