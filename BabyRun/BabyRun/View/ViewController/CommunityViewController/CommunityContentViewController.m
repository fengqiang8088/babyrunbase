//
//  CommunityContentViewController.m
//  BabyRun
//
//  Created by fengqiang on 14-12-15.
//  Copyright (c) 2014年 fengqiang. All rights reserved.
//

#import "CommunityContentViewController.h"

#import "BRTableView.h"
#import "CommunityIndexManager.h"
#import "CommunityTableViewCell.h"
#import "UserViewController.h"
#import "CommunityDetailViewController.h"
#import "IndexViewController.h"


@interface CommunityContentViewController ()<UITableViewDelegate,BRTableViewDelegate,CommunityTableViewCellDelegate>

@property(nonatomic,strong) IBOutlet BRTableView *tableView;
@property(nonatomic,strong) NSMutableArray *result;
@property(nonatomic,strong) NSMutableDictionary *parm;

@end

@implementation CommunityContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSetup];
}

- (void)initSetup{
    [self.tableView setBRTableViewDelegate:self];
    self.parm = [[NSMutableDictionary alloc] init];
    self.result = [[NSMutableArray alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    self.isLoadModel = NO;
    if (self.isFirstLoad) {
        [self loadData];
    }
}

-(void)loadData{
    if(!self.isLoadModel){
        [self showLoading];
        [self.parm setObject:@"0" forKey:@"skip"];
        if(self.isFav){
            [self.parm setObject:@"isFav" forKey:@"isFav"];
            [[CommunityIndexManager shared] getCommunityIndexForUserList:self.parm
                                                                 success:^(id objects){
                                                                     [self.result removeAllObjects];
                                                                     self.result = objects;
                                                                     // 刷新表格
                                                                     [self.tableView reloadData];
                                                                     self.isLoadModel = YES;
                                                                     [self hideLoading];
                                                                 }failure:^(NSError *error){
                                                                     [self hideLoading];
                                                                     [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                                                                        subtitle:NSLocalizedString(@"网络连接错误，请您稍后再试", nil)
                                                                                                            type:TSMessageNotificationTypeError];
                                                                     [self.tableView reloadData];
                                                                 }];
        }
        else{
            if (self.textGroup) {
                [self.parm setObject:self.textGroup forKey:@"textGroup"];
            }
            [[CommunityIndexManager shared] getCommunityIndexList:self.parm
                                                          success:^(id objects){
                                                              [self.result removeAllObjects];
                                                              self.result = objects;
                                                              // 刷新表格
                                                              [self.tableView reloadData];
                                                              self.isLoadModel = YES;
                                                              [self hideLoading];
                                                          }failure:^(NSError *error){
                                                              self.isLoadModel = NO;
                                                              [self hideLoading];
                                                              [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                                                                 subtitle:NSLocalizedString(@"网络连接错误，请您稍后再试", nil)
                                                                                                     type:TSMessageNotificationTypeError];
                                                              [self.tableView reloadData];
                                                          }];
        }
    }
}

#pragma mark  -  table delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.result.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 320;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommunityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[CommunityTableViewCell reuseIdentifier]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.textShared = self.result[indexPath.row];
    cell.delegate = self;
    [cell updateUI];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

- (void)refreshTable:(BRTableView *)tableview{
    [self.parm setObject:@"0" forKey:@"skip"];
    if(self.isFav){
        [[CommunityIndexManager shared] getCommunityIndexForUserList:self.parm
                                                             success:^(id objects){
                                                                 [self.result removeAllObjects];
                                                                 self.result = objects;
                                                                 // 刷新表格
                                                                 [self.tableView reloadData];
                                                                 [self.tableView headerEndRefreshing];
                                                             }failure:^(NSError *error){
                                                                 [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                                                                    subtitle:NSLocalizedString(@"网络连接错误，请您稍后再试", nil)
                                                                                                        type:TSMessageNotificationTypeError];
                                                                 [self.tableView reloadData];
                                                                 [self.tableView headerEndRefreshing];
                                                             }];
    }
    else{
        [[CommunityIndexManager shared] getCommunityIndexList:self.parm
                                                      success:^(id objects){
                                                          [self.result removeAllObjects];
                                                          self.result = objects;
                                                          // 刷新表格
                                                          [self.tableView reloadData];
                                                          [self.tableView headerEndRefreshing];
                                                      }failure:^(NSError *error){
                                                          self.isLoadModel = NO;
                                                          [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                                                             subtitle:NSLocalizedString(@"网络连接错误，请您稍后再试", nil)
                                                                                                 type:TSMessageNotificationTypeError];
                                                          [self.tableView reloadData];
                                                          [self.tableView headerEndRefreshing];
                                                      }];
    }
}

- (void)loadMoreDataTable:(BRTableView *)tableview{
    [self.parm setObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.result.count] forKey:@"skip"];
    if(self.isFav){
        [[CommunityIndexManager shared] getCommunityIndexForUserList:self.parm
                                                             success:^(id objects){
                                                                 for (NSDictionary *dict in objects) {
                                                                     [self.result addObject:dict];
                                                                 }
                                                                 // 刷新表格
                                                                 [self.tableView reloadData];
                                                                 [self.tableView footerEndRefreshing];
                                                             }failure:^(NSError *error){
                                                                 [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                                                                    subtitle:NSLocalizedString(@"网络连接错误，请您稍后再试", nil)
                                                                                                        type:TSMessageNotificationTypeError];
                                                                 [self.tableView reloadData];
                                                                 [self.tableView footerEndRefreshing];
                                                             }];
    }
    else{
        [[CommunityIndexManager shared] getCommunityIndexList:self.parm
                                                      success:^(id objects){
                                                          for (NSDictionary *dict in objects) {
                                                              [self.result addObject:dict];
                                                          }
                                                          // 刷新表格
                                                          [self.tableView reloadData];
                                                          [self.tableView footerEndRefreshing];
                                                      }failure:^(NSError *error){
                                                          self.isLoadModel = NO;
                                                          [TSMessage showNotificationInViewController:self title:NSLocalizedString(@"出现错误", nil)
                                                                                             subtitle:NSLocalizedString(@"网络连接错误，请您稍后再试", nil)
                                                                                                 type:TSMessageNotificationTypeError];
                                                          [self.tableView reloadData];
                                                          [self.tableView footerEndRefreshing];
                                                      }];
    }
}

#pragma mark - CommunityTableViewCellDelegate

- (void)selectUser:(AVUser *)user{
    UserViewController *userViewController = [UserViewController brCreateFor:@"UserStoryboard" andClassIdentifier:@"UserStoryboard"];
    userViewController.user = user;
    userViewController.showTypeUser = 1;
    [self.navigationController pushViewController:userViewController animated:YES];
}

-(void)selectComment:(TextShared *)textShared{
    CommunityDetailViewController *communityDetailViewController = [CommunityDetailViewController brCreateFor:@"CommunityStoryboard" andClassIdentifier:@"CommunityDetailViewController"];
    communityDetailViewController.textShared = textShared;
    [self.navigationController pushViewController:communityDetailViewController animated:YES];
}

-(void)onLogin{
    IndexViewController *indexViewController = [IndexViewController brCreateFor:@"LoginAndRegister" andClassIdentifier:@"IndexStoryboard"];
    indexViewController.fromType = 1;
    UIViewController *indexNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:indexViewController];
    [self presentViewController:indexNavigationController animated:YES completion:nil];
}

-(void)selectMore:(TextShared *)textShared{
    if([AVUser currentUser]!=nil){
        [self showShareView:textShared];
    }
    else{
        [self onLogin];
    }
}

@end
